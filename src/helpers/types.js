import { isNumber, isString } from 'lodash';
import { isValidElement } from 'react';

export const isStringNumberOrNode = (value) =>
  isString(value) || isNumber(value) || isValidElement(value);
