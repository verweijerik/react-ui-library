import React from 'react';
import PropTypes from 'prop-types';
import { Toaster } from '../components/toaster/toaster';

import '../style/reset/reset.less';
import '../style/global.less';

const outerContainerStyle = {
  margin: '50px',
};

export const Container = ({
  style,
  children,
  margin,
  deprecatedMessage,
  warningMessage,
}) => {
  return (
    <div style={margin ? outerContainerStyle : {}}>
      {deprecatedMessage ? (
        <div
          style={{
            border: '1px solid red',
            color: 'red',
            padding: '10px',
            marginBottom: '20px',
          }}
        >
          {deprecatedMessage}
        </div>
      ) : null}
      {warningMessage ? (
        <div
          style={{
            border: '1px solid orange',
            color: 'orange',
            padding: '10px',
            marginBottom: '20px',
          }}
        >
          {warningMessage}
        </div>
      ) : null}
      <div style={style}>{children}</div>
      <Toaster />
    </div>
  );
};

Container.defaultProps = {
  style: {},
  margin: true,
  deprecatedMessage: null,
};

Container.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.node.isRequired,
  margin: PropTypes.bool,
  deprecatedMessage: PropTypes.string,
};
