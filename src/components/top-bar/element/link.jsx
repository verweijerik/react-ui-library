import React from 'react';
import cx from 'classnames';
import styles from '../top-bar.module.less';

export const Link = ({ label, url, icon, onClick, active, disabled }) => {
  return (
    <a
      className={cx(styles.link, active ? styles.active : '')}
      href={url}
      onClick={onClick}
      disabled={disabled}
    >
      {icon}
      <span className={styles.label}>{label}</span>
    </a>
  );
};
