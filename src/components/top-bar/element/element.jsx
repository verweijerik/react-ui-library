import React from 'react';
import cx from 'classnames';
import { Button } from '../../button/button';
import { Menu } from '../../menu/menu';
import { Link } from './link';
import styles from '../top-bar.module.less';

export const Element = ({ element }) => {
  return (() => {
    switch (element.type) {
      case 'Link': {
        return (
          <div className={styles.item}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <Link
              label={element.label}
              url={element.url}
              icon={element.icon}
              onClick={element.onClick}
              active={element.active}
              disabled={element.disabled}
            />
          </div>
        );
      }
      case 'Button': {
        return (
          <div className={cx(styles.item, styles.button)}>
            <Button
              label={element.label}
              colored={element.colored}
              pill
              disabled={element.disabled}
              onClick={element.onClick}
              icon={element.icon}
            />
          </div>
        );
      }
      case 'Menu': {
        return (
          <div className={cx(styles.item, styles.menu)}>
            <Menu
              height="100%"
              menu={{
                trigger: 'Component',
                fullHeightTrigger: true,
                anchor: 'BOTTOM_LEFT',
                component: (
                  // eslint-disable-next-line jsx-a11y/anchor-is-valid
                  <Link
                    label={element.label}
                    icon={element.icon}
                    onClick={element.onClick}
                  />
                ),
                sections: element.sections,
              }}
            />
          </div>
        );
      }
      case 'Component': {
        return <div className={styles.item}>{element.component}</div>;
      }
      default:
        return null;
    }
  })();
};
