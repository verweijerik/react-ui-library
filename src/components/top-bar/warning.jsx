import React from 'react';
import styles from './top-bar.module.less';

export const Warning = ({ warning }) => {
  return <div className={styles.alert}>{warning}</div>;
};
