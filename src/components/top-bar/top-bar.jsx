import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Warning } from './warning';
import { Title } from './title';
import { Element } from './element/element';
import styles from './top-bar.module.less';

export const TopBar = ({
  title,
  content,
  contentRight,
  warning,
  height,
  //private:
  fixedPosition,
}) => {
  return (
    <div>
      {warning && <Warning warning={warning} />}
      <div
        className={cx(styles.topbar, fixedPosition ? styles.fixed : '')}
        style={{ height }}
      >
        <div className={styles.left}>
          {title && (
            <Title
              label={title.label}
              logo={title.logo}
              url={title.url}
              version={title.version}
              onClick={title.onClick}
            />
          )}
          {content.map((element, i) => {
            if (element) {
              return <Element key={i} element={element} />;
            }
            return null;
          })}
        </div>
        <div className={styles.right}>
          {contentRight.map((element, i) => {
            if (element) {
              return <Element key={i} element={element} />;
            }
            return null;
          })}
        </div>
      </div>
    </div>
  );
};

TopBar.defaultProps = {
  title: null,
  content: [],
  contentRight: [],
  warning: '',
  height: null,
  //private props:
  fixedPosition: true,
};

TopBar.propTypes = {
  title: PropTypes.shape({
    label: PropTypes.string,
    logo: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    url: PropTypes.string,
    onClick: PropTypes.func,
    version: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  content: PropTypes.array,
  contentRight: PropTypes.array,
  warning: PropTypes.string,
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  //private props:
  fixedPosition: PropTypes.bool,
};
