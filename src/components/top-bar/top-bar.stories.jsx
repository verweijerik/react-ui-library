import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  FaSignOutAlt,
  FaUser,
  FaReply,
  FaDog,
  FaPhone,
  FaEnvelope,
  FaBell,
} from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Button, Menu, TopBar, Icon, Badge } from '../../..';
import LogoOliasoft from '../../images/logo@2x.png';
import LogoPNG from '../../images/logo.png';
import LogoSVG from '../../images/logo.svg';

const style = {
  height: '1000%',
  width: '100%',
};

storiesOf('Navigation/TopBar', module)
  .add('default', () => (
    <Container style={style}>
      <TopBar />
    </Container>
  ))
  .add('with title', () => (
    <Container style={style}>
      <TopBar
        title={{
          label: 'Alpacas Unlimited',
          onClick: () => {},
        }}
      />
    </Container>
  ))
  .add('with link title', () => (
    <Container style={style}>
      <TopBar
        title={{
          label: 'Alpacas Unlimited',
          url: 'https://google.com',
        }}
      />
    </Container>
  ))
  .add('with logo', () => (
    <Container style={style}>
      <TopBar
        title={{
          logo: <img src={LogoSVG} alt="logo" />,
        }}
      />
    </Container>
  ))
  .add('with logo and title', () => (
    <Container style={style}>
      <TopBar
        title={{
          logo: <img src={LogoSVG} alt="logo" />,
          label: 'Alpacas Unlimited',
          url: 'https://google.com',
        }}
      />
    </Container>
  ))
  .add('with buttons', () => (
    <Container style={style}>
      <TopBar
        content={[
          {
            type: 'Button',
            label: 'Save',
            colored: true,
            onClick: () => {},
          },
          {
            type: 'Button',
            label: 'Back',
            icon: <FaReply />,
            onClick: () => {},
          },
        ]}
      />
    </Container>
  ))
  .add('with links', () => (
    <Container style={style}>
      <TopBar
        content={[
          {
            type: 'Link',
            label: 'Project',
            onClick: () => {},
            active: true,
          },
          {
            type: 'Link',
            label: 'External',
            url: 'https://google.com',
          },
        ]}
      />
    </Container>
  ))
  .add('with link menus', () => (
    <Container style={style}>
      <TopBar
        content={[
          {
            type: 'Menu',
            label: 'Dropdown',
            onClick: () => {},
            sections: [
              {
                type: 'Option',
                label: 'Alpacas',
                onClick: () => {},
              },
              {
                type: 'Option',
                label: 'Llamas',
                onClick: () => {},
              },
            ],
          },
        ]}
      />
    </Container>
  ))
  .add('with right-hand content', () => (
    <Container style={style}>
      <TopBar
        contentRight={[
          {
            type: 'Link',
            label: 'Project',
            onClick: () => {},
          },
          {
            type: 'Button',
            label: 'Back',
            icon: <FaReply />,
            onClick: () => {},
          },
        ]}
      />
    </Container>
  ))
  .add('with different logo types', () => (
    <Container style={style}>
      <div style={{ marginBottom: '40px' }}>
        <h4>Oliasoft Logo:</h4>
        <TopBar
          title={{
            logo: <img src={LogoOliasoft} alt="logo" />,
          }}
          fixedPosition={false}
        />
      </div>
      <div style={{ marginBottom: '40px' }}>
        <h4>SVG Logo:</h4>
        <TopBar
          title={{
            logo: <img src={LogoSVG} alt="logo" />,
          }}
          fixedPosition={false}
        />
      </div>
      <div style={{ marginBottom: '40px' }}>
        <h4>PNG Logo:</h4>
        <TopBar
          title={{
            logo: <img src={LogoPNG} alt="logo" />,
          }}
          fixedPosition={false}
        />
      </div>
      <div style={{ marginBottom: '40px' }}>
        <h4>React-icon Logo:</h4>
        <TopBar
          title={{
            logo: <FaDog color="#ff6600" size={40} />,
          }}
          fixedPosition={false}
        />
      </div>
    </Container>
  ))
  .add('with version', () => (
    <Container style={style}>
      <div style={{ marginBottom: '40px' }}>
        <h4>Oliasoft Logo:</h4>
        <TopBar
          title={{
            logo: <img src={LogoOliasoft} alt="logo" />,
            version: 'V1.2.3',
          }}
          fixedPosition={false}
        />
      </div>
      <div style={{ marginBottom: '40px' }}>
        <h4>SVG Logo:</h4>
        <TopBar
          title={{
            logo: <img src={LogoSVG} alt="logo" />,
            version: 'V1.2.3',
          }}
          fixedPosition={false}
        />
      </div>
      <div style={{ marginBottom: '40px' }}>
        <h4>PNG Logo:</h4>
        <TopBar
          title={{
            logo: <img src={LogoPNG} alt="logo" />,
            version: 'V1.2.3',
          }}
          fixedPosition={false}
        />
      </div>
      <div style={{ marginBottom: '40px' }}>
        <h4>React-icon Logo:</h4>
        <TopBar
          title={{
            logo: <FaDog color="#ff6600" size={40} />,
            version: 'V1.2.3',
          }}
          fixedPosition={false}
        />
      </div>
    </Container>
  ))
  .add('with custom components', () => (
    <Container style={style}>
      <TopBar
        content={[
          {
            type: 'Component',
            component: (
              <Menu
                menu={{
                  trigger: 'Component',
                  fullHeightTrigger: true,
                  anchor: 'BOTTOM_RIGHT',
                  component: (
                    <Button label="TU" colored round onClick={() => {}} />
                  ),
                  sections: [
                    {
                      type: 'Heading',
                      label: 'Test User',
                    },
                    {
                      type: 'Option',
                      label: 'Profile',
                      icon: <FaUser color="#ff6600" />,
                      onClick: () => {},
                    },
                    { type: 'Divider' },
                    {
                      type: 'Option',
                      label: 'Sign out',
                      icon: <FaSignOutAlt color="#ff6600" />,
                      onClick: () => {},
                    },
                  ],
                }}
              />
            ),
          },
        ]}
        contentRight={[
          {
            type: 'Component',
            component: (
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  color: '#7d7b7a',
                  marginRight: '20px',
                }}
              >
                <span
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'right',
                    marginBottom: '5px',
                  }}
                >
                  <FaPhone color="#7d7b7a" size="18px" />
                  <span style={{ paddingLeft: '5px' }}>+44 123 456 7890</span>
                </span>
                <span
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'right',
                  }}
                >
                  <FaEnvelope color="#7d7b7a" size="18px" />
                  <a
                    href="mailto:contact@alpacas.llamas"
                    style={{ paddingLeft: '5px', color: '#7d7b7a' }}
                  >
                    contact@alpacas.llamas
                  </a>
                </span>
              </div>
            ),
          },
        ]}
      />
    </Container>
  ))
  .add('with warning', () => (
    <Container style={style}>
      <TopBar warning="Application update available. Please refresh your browser." />
    </Container>
  ))
  .add('kitchen sink', () => (
    <Container style={style}>
      <TopBar
        warning="Application update available. Please refresh your browser."
        title={{
          logo: <img src={LogoPNG} alt="logo" />,
          label: 'Alpacas Unlimited',
          url: 'https://google.com',
          version: 'V1.2.3',
        }}
        content={[
          {
            type: 'Link',
            label: 'Project',
            disabled: true,
            onClick: () => {},
          },
          {
            type: 'Link',
            label: 'Users',
            icon: <FaUser />,
          },
          {
            type: 'Menu',
            label: 'Dropdown',
            onClick: () => {},
            sections: [
              {
                type: 'Option',
                label: 'Alpacas',
                onClick: () => {},
              },
              {
                type: 'Option',
                label: 'Llamas',
                onClick: () => {},
              },
            ],
          },
          {
            type: 'Link',
            label:
              'Long Project Name Goes Here Lorem Ipsum Dolor Est Compendum',
            onClick: () => {},
            active: true,
          },
          {
            type: 'Button',
            label: 'Save',
            disabled: true,
            colored: true,
            onClick: () => {},
          },
        ]}
        contentRight={[
          {
            type: 'Button',
            label: 'Back',
            icon: <FaReply />,
            onClick: () => {},
          },
          {
            type: 'Component',
            component: (
              <Badge title="3">
                <Icon size={24} icon={<FaBell />} clickable />
              </Badge>
            ),
          },
          {
            type: 'Component',
            component: (
              <Menu
                menu={{
                  trigger: 'Component',
                  fullHeightTrigger: true,
                  anchor: 'BOTTOM_RIGHT',
                  component: (
                    <Button label="TU" colored round onClick={() => {}} />
                  ),
                  sections: [
                    {
                      type: 'Heading',
                      label: 'Test User',
                    },
                    {
                      type: 'Option',
                      label: 'Profile',
                      icon: <FaUser color="#ff6600" />,
                      onClick: () => {},
                    },
                    { type: 'Divider' },
                    {
                      type: 'Option',
                      label: 'Sign out',
                      icon: <FaSignOutAlt color="#ff6600" />,
                      onClick: () => {},
                    },
                  ],
                }}
              />
            ),
          },
        ]}
      />
    </Container>
  ))
  .add('too wide', () => (
    <Container style={style}>
      <TopBar
        title={{
          logo: <img src={LogoPNG} alt="logo" />,
          label: 'Alpacas Unlimited',
          url: 'https://google.com',
          version: 'V1.2.3',
        }}
        content={Array(8)
          .fill(0)
          .map(() => ({
            type: 'Link',
            label: Math.random().toString(36).substring(7),
            onClick: () => {},
          }))}
        contentRight={Array(8)
          .fill(0)
          .map(() => ({
            type: 'Button',
            label: Math.random().toString(36).substring(7),
            onClick: () => {},
          }))}
      />
    </Container>
  ))
  .add('with height', () => (
    <Container style={style}>
      <TopBar
        height="100px"
        title={{
          logo: <img src={LogoPNG} alt="logo" />,
          label: 'Alpacas Unlimited',
          url: 'https://google.com',
          version: 'V1.2.3',
        }}
        content={[
          {
            type: 'Link',
            label: 'Project',
            onClick: () => {},
          },
          {
            type: 'Link',
            label: 'External',
            url: 'https://google.com',
          },
          {
            type: 'Link',
            label: 'Users',
            icon: <FaUser />,
          },
          {
            type: 'Menu',
            label: 'Dropdown',
            onClick: () => {},
            sections: [
              {
                type: 'Option',
                label: 'Alpacas',
                onClick: () => {},
              },
              {
                type: 'Option',
                label: 'Llamas',
                onClick: () => {},
              },
            ],
          },
          {
            type: 'Button',
            label: 'Save',
            colored: true,
            onClick: () => {},
          },
        ]}
        contentRight={[
          {
            type: 'Button',
            label: 'Back',
            icon: <FaReply />,
            onClick: () => {},
          },
          {
            type: 'Component',
            component: (
              <Badge title="3">
                <Icon size={24} icon={<FaBell />} clickable />
              </Badge>
            ),
          },
          {
            type: 'Component',
            component: (
              <Menu
                menu={{
                  trigger: 'Component',
                  fullHeightTrigger: true,
                  anchor: 'BOTTOM_RIGHT',
                  component: (
                    <Button label="TU" colored round onClick={() => {}} />
                  ),
                  sections: [
                    {
                      type: 'Heading',
                      label: 'Test User',
                    },
                    {
                      type: 'Option',
                      label: 'Profile',
                      icon: <FaUser color="#ff6600" />,
                      onClick: () => {},
                    },
                    { type: 'Divider' },
                    {
                      type: 'Option',
                      label: 'Sign out',
                      icon: <FaSignOutAlt color="#ff6600" />,
                      onClick: () => {},
                    },
                  ],
                }}
              />
            ),
          },
        ]}
      />
    </Container>
  ));
