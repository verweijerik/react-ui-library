import React from 'react';
import cx from 'classnames';
import styles from './top-bar.module.less';

export const Title = ({ label, logo, url, version, onClick }) => {
  const content = (
    <div className={styles.title}>
      {logo && logo}
      {label && <span className={styles.label}>{label}</span>}
      {version && <div className={styles.version}>{version}</div>}
    </div>
  );
  return (
    <div className={cx(styles.item, styles.brand)}>
      {url ? (
        <a href={url} onClick={onClick}>
          {content}
        </a>
      ) : (
        <div onClick={onClick}>{content}</div>
      )}
    </div>
  );
};
