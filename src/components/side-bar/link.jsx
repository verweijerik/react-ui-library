import React, { memo } from 'react';
import cx from 'classnames';
import isEqual from 'react-fast-compare';
import styles from './side-bar.module.less';
import { Tooltip } from '../tooltip/tooltip';
import { Icon } from '../icon/icon';

const TooltipIcon = memo(
  ({ isOpen, label, icon }) => {
    return (
      <>
        <span className={styles.icon}>
          <Tooltip
            text={label}
            enabled={!isOpen}
            anchor="RIGHT_CENTER"
            possibleAnchors={['RIGHT_CENTER']}
            triggerOffset={30}
            fontSize={16}
            padding="10px 16px"
            display="block"
          >
            <Icon icon={icon} size={30} color="white" />
          </Tooltip>
        </span>
        <span className={styles.label}>{label}</span>
      </>
    );
  },
  /*
    Only re-render if props have changed
    Re-rendering tooltips when not necessary causes 'ghost tooltip' issues,
    because re-renders cause mouse events to get swallowed, which prevents
    tooltips closing when the main thread is blocked
    We use a deep equality check because the the label can theoretically be JSX
   */
  (prevProps, nextProps) => isEqual(prevProps, nextProps),
);

export const Link = ({ isOpen, items, sectionIndex, onClick }) => {
  return (
    <div className={styles.list}>
      {items.map((link, i) => {
        const key = `${sectionIndex}_${i}`;
        return (
          <a
            href={link.value}
            className={cx(
              styles.item,
              link.isActive ? styles.active : '',
              link.isExperimental ? styles.experimental : '',
            )}
            key={key}
            onClick={(evt) =>
              onClick(evt, link.value, link.label, link.onClick)
            }
          >
            <TooltipIcon
              key={key}
              label={link.label}
              icon={link.icon}
              isOpen={isOpen}
            />
          </a>
        );
      })}
    </div>
  );
};
