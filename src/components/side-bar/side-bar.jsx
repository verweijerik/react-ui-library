import React, { memo, useState } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import isEqual from 'react-fast-compare';
import { FaArrowLeft } from 'react-icons/fa';
import { Drawer } from '../drawer/drawer';
import { Sections } from './sections';
import styles from './side-bar.module.less';
import { Button } from '../button/button';

export const SideBar = memo(
  ({ options, startOpen, onShiftClickToggleOpen, top }) => {
    const [isOpen, setIsOpen] = useState(startOpen || false);

    const onClick = (evt, value, label, clickHandler) => {
      evt.preventDefault();
      if (typeof clickHandler === 'function') {
        evt.target.value = value;
        evt.target.label = label;
        clickHandler(evt);
      }
      setIsOpen(false);
    };

    const visible = options.sections.length > 0;
    return (
      <Drawer
        fixed
        open={isOpen}
        top={top}
        background="#1c1c1c"
        closedWidth={70}
        enableButton={visible}
        buttonPosition="bottom"
        buttonAnimate
        button={
          visible && (
            <Button
              onClick={(evt) => {
                if (evt.shiftKey) {
                  onShiftClickToggleOpen(evt);
                }
                setIsOpen(!isOpen);
              }}
              colored
              round
              icon={<FaArrowLeft />}
            />
          )
        }
      >
        <div className={cx(styles.sidebar, !isOpen ? styles.collapsed : '')}>
          {visible && (
            <div className={styles.inner}>
              <h4 className={styles.title}>{options.title}</h4>
              <Sections
                isOpen={isOpen}
                sections={options.sections}
                onClick={onClick}
              />
            </div>
          )}
        </div>
      </Drawer>
    );
  },
  (prevProps, nextProps) => isEqual(prevProps, nextProps),
);

SideBar.defaultProps = {
  startOpen: false,
  onShiftClickToggleOpen: () => {},
  top: undefined,
};

SideBar.propTypes = {
  options: PropTypes.shape({
    title: PropTypes.string.isRequired,
    sections: PropTypes.arrayOf(
      PropTypes.shape({
        heading: PropTypes.string.isRequired,
        items: PropTypes.arrayOf(
          PropTypes.shape({
            label: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired,
            icon: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
              .isRequired,
            onClick: PropTypes.func.isRequired,
            isActive: PropTypes.bool,
            isExperimental: PropTypes.bool,
          }),
        ).isRequired,
      }),
    ).isRequired,
  }).isRequired,
  startOpen: PropTypes.bool,
  onShiftClickToggleOpen: PropTypes.func,
  top: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
