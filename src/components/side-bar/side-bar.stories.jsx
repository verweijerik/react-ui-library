import React from 'react';
import cx from 'classnames';
import { storiesOf } from '@storybook/react';
import { FaAngleDoubleRight } from 'react-icons/fa';
import svgIcon from '../../images/icons/icons8-junction.svg';
import pngIcon from '../../images/icons/icon-share.png';
import { SideBar } from './side-bar';
import { Container } from '../../helpers/container';
import styles from './container.module.less';

const style = {
  height: '500px',
  width: '500px',
};

const onClick = (evt) => {
  const { value, label } = evt.target;
  console.log(value);
  console.log(label);
};

storiesOf('Navigation/SideBar', module)
  .add('default', () => (
    <Container style={style}>
      <SideBar
        options={{
          title: 'Menu Title Goes Here Lorem Ipsum Dolor Est',
          sections: [
            {
              heading: 'First Heading',
              items: [
                {
                  label: 'Section A',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
                {
                  label: 'Section B',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                  isActive: true,
                },
                {
                  label: 'Section C',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
              ],
            },
            {
              heading: 'Second Heading',
              items: [
                {
                  label: 'Section 1',
                  value: '/path/to/something',
                  onClick,
                  icon: pngIcon,
                },
                {
                  label: 'Section 2',
                  value: '/path/to/something',
                  onClick,
                  icon: <FaAngleDoubleRight />,
                },
              ],
            },
          ],
        }}
      />
    </Container>
  ))
  .add('open', () => (
    <Container style={style}>
      <SideBar
        options={{
          title: 'Menu Title Goes Here Lorem Ipsum Dolor Est',
          sections: [
            {
              heading: 'First Heading',
              items: [
                {
                  label: 'Section A',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
                {
                  label: 'Section B',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                  isActive: true,
                },
                {
                  label: 'Section C',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
              ],
            },
            {
              heading: 'Second Heading',
              items: [
                {
                  label: 'Section 1',
                  value: '/path/to/something',
                  onClick,
                  icon: pngIcon,
                },
                {
                  label: 'Section 2',
                  value: '/path/to/something',
                  onClick,
                  icon: <FaAngleDoubleRight />,
                },
              ],
            },
          ],
        }}
        startOpen
      />
    </Container>
  ))
  .add('with hidden options', () => (
    <Container style={style}>
      <SideBar
        options={{
          title: 'Menu With Hidden Options',
          sections: [
            {
              heading: 'First Heading',
              items: [
                {
                  label: 'Section A',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
                {
                  label: 'Section B',
                  value: '/path/to/something',
                  onClick,
                  icon: pngIcon,
                  isExperimental: true,
                  isActive: true,
                },
                {
                  label: 'Section C',
                  value: '/path/to/something',
                  onClick,
                  icon: <FaAngleDoubleRight />,
                  isExperimental: true,
                },
                {
                  label: 'Section D',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                  isExperimental: true,
                },
              ],
            },
          ],
        }}
        startOpen
      />
    </Container>
  ))
  .add('with content', () => (
    <Container style={style}>
      <SideBar
        options={{
          title: 'Menu Title Goes Here Lorem Ipsum Dolor Est',
          sections: [
            {
              heading: 'First Heading',
              items: [
                {
                  label: 'Section A',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
                {
                  label: 'Section B',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                  isActive: true,
                },
                {
                  label: 'Section C',
                  value: '',
                  onClick,
                  icon: svgIcon,
                },
              ],
            },
            {
              heading: 'Second Heading',
              items: [
                {
                  label: 'Section 1',
                  value: '/path/to/something',
                  onClick,
                  icon: pngIcon,
                },
                {
                  label: 'Section 2',
                  value: '/path/to/something',
                  onClick,
                  icon: <FaAngleDoubleRight />,
                },
              ],
            },
          ],
        }}
        startOpen
      />
      <div className={cx(styles.oContent, styles.oContentWithPadding)}>
        Example text showing that the sidebar appears on top of the content (no
        resizing of content when sidebar opens or closes.
      </div>
    </Container>
  ))
  .add('custom top', () => (
    <Container style={style}>
      <SideBar
        top="200px"
        options={{
          title: 'Menu Title Goes Here Lorem Ipsum Dolor Est',
          sections: [
            {
              heading: 'First Heading',
              items: [
                {
                  label: 'Section A',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
                {
                  label: 'Section B',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                  isActive: true,
                },
                {
                  label: 'Section C',
                  value: '/path/to/something',
                  onClick,
                  icon: svgIcon,
                },
              ],
            },
            {
              heading: 'Second Heading',
              items: [
                {
                  label: 'Section 1',
                  value: '/path/to/something',
                  onClick,
                  icon: pngIcon,
                },
                {
                  label: 'Section 2',
                  value: '/path/to/something',
                  onClick,
                  icon: <FaAngleDoubleRight />,
                },
              ],
            },
          ],
        }}
      />
    </Container>
  ))
  .add('content with no sidebar', () => (
    <Container style={style}>
      <div
        className={cx(
          styles.oContent,
          styles.oContentWithPadding,
          styles.oContentNoSideBar,
        )}
      >
        Example text showing that when there is no sidebar, the content takes up
        the full page width (no gap where the sidebar normally is).
      </div>
    </Container>
  ))
  .add('empty', () => (
    <Container style={style}>
      <SideBar options={{ title: '', sections: [] }} />
    </Container>
  ));
