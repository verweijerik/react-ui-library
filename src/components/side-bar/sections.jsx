import React from 'react';
import { Link } from './link';
import styles from './side-bar.module.less';

export const Sections = ({ isOpen, sections, onClick }) => {
  return (
    <>
      {sections.map((section, i) => (
        <React.Fragment key={i}>
          <h5 className={styles.subtitle}>{section.heading.toUpperCase()}</h5>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <Link
            isOpen={isOpen}
            items={section.items}
            sectionindex={i}
            onClick={onClick}
          />
        </React.Fragment>
      ))}
    </>
  );
};
