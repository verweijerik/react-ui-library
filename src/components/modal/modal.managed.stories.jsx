import React, { useReducer } from 'react';
import { storiesOf } from '@storybook/react';
import { useKeyboardEvent } from '../../hooks';
import { Container } from '../../helpers/container';
import { Modal } from './modal';
import { Button } from '../button/button';

const style = {
  height: '500px',
  width: '200px',
};

const initialState = {
  visible: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'OPEN': {
      return {
        ...state,
        visible: true,
      };
    }
    case 'CLOSE': {
      return {
        ...state,
        visible: false,
      };
    }
    default:
      return state;
  }
};

const ManagedModal = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  useKeyboardEvent('Escape', () => dispatch({ type: 'CLOSE' }));
  return (
    <>
      <Button label="Open modal" onClick={() => dispatch({ type: 'OPEN' })} />
      <Modal visible={state.visible} fullScreen>
        <div>
          <h1>Are you sure?</h1>
          <Button
            label="Execute"
            colored
            onClick={() => dispatch({ type: 'CLOSE' })}
          />
          <Button label="Cancel" onClick={() => dispatch({ type: 'CLOSE' })} />
        </div>
      </Modal>
    </>
  );
};

storiesOf('Modals/Modal', module).add('managed', () => (
  <Container style={style}>
    <ManagedModal />
  </Container>
));
