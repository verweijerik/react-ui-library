import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Modal } from './modal';
import { Button } from '../button/button';
import { Dialog } from '../dialog/dialog';
import { Message } from '../message/message';

const style = {
  height: '500px',
  width: '200px',
};

//A basic modal can have any React / JSX component as content
const simple = <div>Example modal content</div>;

const withMessage = (
  <Message
    message={{
      visible: true,
      heading: 'Error heading',
      label: 'An error has happened',
      details: 'Bad input data',
      icon: true,
      type: 'Error',
      withDismiss: true,
      onClose: () => {},
    }}
  />
);

const withDialog = (
  <Dialog
    dialog={{
      heading: 'Commit transaction?',
      content: <>This will complete the transaction</>,
      footer: (
        <>
          <Button label="Okay" inverted colored="green" onClick={() => {}} />
          <Button label="Cancel" inverted colored="red" onClick={() => {}} />
        </>
      ),
      onClose: () => {},
    }}
  />
);

const withDialogWithLongContentAndScrollBar = (
  <Dialog
    dialog={{
      scroll: true,
      heading: 'Long Content With Scrollbar',
      content: (
        <div>
          {'Example modal content goes here lorem ipsum. '.repeat(1000)}
        </div>
      ),
      footer: (
        <>
          <Button label="Okay" inverted colored="green" onClick={() => {}} />
          <Button label="Cancel" inverted colored="red" onClick={() => {}} />
        </>
      ),
      onClose: () => {},
    }}
  />
);

//Using the Message component is optional. You can have a fully custom modal
const customModal = (
  <div>
    <h1>Are you sure?</h1>
    <Button label="Okay" inverted colored="green" onClick={() => {}} />
    <Button label="Cancel" inverted colored="red" onClick={() => {}} />
  </div>
);

storiesOf('Modals/Modal', module)
  .add('simple', () => (
    <Container style={style}>
      <Modal visible>{simple}</Modal>
    </Container>
  ))
  .add('centered', () => (
    <Container style={style}>
      <Modal visible centered>
        {simple}
      </Modal>
    </Container>
  ))
  .add('with message', () => (
    <Container style={style}>
      <Modal visible>{withMessage}</Modal>
    </Container>
  ))
  .add('with centered message', () => (
    <Container style={style}>
      <Modal visible centered>
        {withMessage}
      </Modal>
    </Container>
  ))
  .add('with dialog', () => (
    <Container style={style}>
      <Modal visible>{withDialog}</Modal>
    </Container>
  ))
  .add('with centered dialog', () => (
    <Container style={style}>
      <Modal visible centered>
        {withDialog}
      </Modal>
    </Container>
  ))
  .add('with long dialog with internal scrollbar', () => (
    <Container style={style}>
      <Modal visible>{withDialogWithLongContentAndScrollBar}</Modal>
    </Container>
  ))
  .add('with custom modal', () => (
    <Container style={style}>
      <Modal visible centered>
        {customModal}
      </Modal>
    </Container>
  ));
