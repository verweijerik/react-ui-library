import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Portal } from '../portal/portal';
import styles from './modal.module.less';

const Wrapper = ({ children }) => (
  <div className={cx(styles.wrapper)}>{children}</div>
);

const Content = ({ children, width, centered }) => {
  return (
    <div
      className={cx(styles.contentContainer, centered ? styles.centered : '')}
      style={{ maxWidth: width }}
    >
      {children}
    </div>
  );
};

export const Modal = ({ children, visible, centered, width }) => {
  return (
    <>
      <Portal id="modalContainer">
        {visible ? (
          <Wrapper>
            <Content width={width} centered={centered}>
              {children}
            </Content>
          </Wrapper>
        ) : null}
      </Portal>
      <div id="modalContainer" />
    </>
  );
};

Modal.defaultProps = {
  visible: false,
  centered: false,
  width: '100%',
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  visible: PropTypes.bool,
  centered: PropTypes.bool,
  width: PropTypes.string,
};
