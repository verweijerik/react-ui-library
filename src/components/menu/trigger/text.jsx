import React from 'react';
import cx from 'classnames';
import { FaAngleDown, FaAngleRight } from 'react-icons/fa';
import styles from '../menu.module.less';

export const Text = ({ label, carat, disabled }) => {
  return (
    <div className={cx(styles.trigger, disabled ? styles.disabled : null)}>
      <span className={styles.label} title={label}>
        {label}
      </span>
      {carat && carat === 'right' ? (
        <FaAngleRight className={styles.arrow} />
      ) : carat && carat === 'down' ? (
        <FaAngleDown className={styles.arrow} />
      ) : null}
    </div>
  );
};
