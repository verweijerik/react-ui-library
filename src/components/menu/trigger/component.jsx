import React from 'react';
import cx from 'classnames';
import styles from '../menu.module.less';

export const Component = ({ component, disabled }) => {
  return (
    <div className={cx(styles.component, disabled ? styles.disabled : null)}>
      {component}
    </div>
  );
};
