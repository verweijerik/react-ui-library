import React from 'react';
import cx from 'classnames';
import { FaAngleDown } from 'react-icons/fa';
import styles from '../menu.module.less';
import { Button as BasicButton } from '../../button/button';

export const Button = ({
  trigger,
  label,
  colored,
  small,
  width,
  disabled,
  groupOrder,
}) => {
  const buttonLabel =
    trigger === 'Button' ? (
      label
    ) : trigger === 'DropDownButton' ? (
      <span className={styles.middleAlignedInline}>
        <span className={styles.buttonLabel}>{label}</span>
        <FaAngleDown className={styles.buttonCaret} />
      </span>
    ) : null;

  return (
    <div className={cx(styles.trigger, disabled ? styles.disabled : null)}>
      <BasicButton
        label={buttonLabel}
        colored={colored}
        small={small}
        width={width}
        groupOrder={groupOrder}
        onClick={() => {} /*handled in parent*/}
      />
    </div>
  );
};
