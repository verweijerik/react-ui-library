import React from 'react';
import { Button } from './button';
import { Text } from './text';
import { Component } from './component';

export const Trigger = ({
  isDisabled,
  width,
  element,
  trigger,
  label,
  onClickTrigger,
  colored,
  small,
  isNested,
  contextMenu,
  component,
  groupOrder,
  fullHeight,
}) => {
  return (
    <div
      style={
        width
          ? { width }
          : {
              overflow: 'hidden',
              ...(fullHeight && {
                height: '100%',
                display: 'flex',
              }),
            }
      }
      onClick={(evt) => {
        evt.stopPropagation();
        onClickTrigger(evt);
      }}
    >
      {!isDisabled ? element : null}
      {trigger === 'Button' || trigger === 'DropDownButton' ? (
        <Button
          trigger={trigger}
          label={label}
          colored={colored}
          small={small}
          width={width}
          disabled={isDisabled}
          groupOrder={groupOrder}
        />
      ) : trigger === 'Text' ? (
        <Text
          label={label}
          disabled={isDisabled}
          carat={!contextMenu && (isNested ? 'right' : 'down')}
        />
      ) : trigger === 'Component' ? (
        <Component component={component} disabled={isDisabled} />
      ) : null}
    </div>
  );
};
