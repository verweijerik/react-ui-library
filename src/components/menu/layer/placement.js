export const placement = (anchor, isNested = false) => ({
  autoAdjust: true,
  preferX: 'RIGHT',
  preferY: 'BOTTOM',
  anchor: anchor || (isNested ? 'RIGHT_TOP' : 'BOTTOM_LEFT'),
});
