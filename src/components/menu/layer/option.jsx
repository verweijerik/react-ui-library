import React, { isValidElement } from 'react';
import { FaCheck } from 'react-icons/fa';
import cx from 'classnames';
import { Icon } from '../../icon/icon';
import styles from '../menu.module.less';

export const Option = ({
  label,
  onClick,
  description,
  icon,
  selected,
  closeOnOptionClick,
  close,
  disabled,
  inline,
  title,
  upload,
  onChange,
}) => {
  return (
    <div
      className={cx(
        styles.option,
        disabled ? styles.disabled : '',
        inline ? styles.inline : '',
        selected ? styles.selected : '',
      )}
      disabled={disabled}
      onClick={(evt) => {
        evt.stopPropagation();
        if (disabled || upload) {
          return;
        }
        if (closeOnOptionClick) {
          close();
        }
        onClick(evt);
      }}
    >
      {icon ? (
        <span className={styles.icon}>
          <Icon icon={icon} size={14} />
        </span>
      ) : null}
      <div className={styles.optionContent}>
        {upload && (
          <input
            type="file"
            className={styles.fileInput}
            onChange={(evt) => onChange(evt.target.files)}
          />
        )}
        <span className={styles.label} title={title || label}>
          {label}
        </span>
        <span className={styles.description}>{description}</span>
      </div>
    </div>
  );
};
