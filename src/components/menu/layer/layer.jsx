import React, { isValidElement } from 'react';
import cx from 'classnames';
import styles from '../menu.module.less';
import { Section } from './section';
import { childPath } from './path';

export const Layer = ({
  sections,
  isNested,
  width,
  closeOnOptionClick,
  close,
  tree,
  path,
  maxHeight,
}) => {
  if (isValidElement(sections)) {
    return <>{sections}</>;
  }
  const hasSubmenu = sections.findIndex((s) => s.type === 'Menu') > -1;
  return (
    <div
      className={cx(styles.layer, isNested ? styles.nested : null)}
      style={{
        maxWidth: width,
        maxHeight: !hasSubmenu && maxHeight ? maxHeight : 'auto',
        overflowY: !hasSubmenu && maxHeight ? 'auto' : 'default',
      }}
    >
      <ul>
        {sections.map((section, i) => {
          return (
            <li key={i}>
              <Section
                section={section}
                closeOnOptionClick={closeOnOptionClick}
                close={close}
                tree={tree}
                path={childPath(path, i)}
              />
            </li>
          );
        })}
      </ul>
    </div>
  );
};
