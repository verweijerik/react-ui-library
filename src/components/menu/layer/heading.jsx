import React from 'react';
import styles from '../menu.module.less';

export const Heading = ({ label, onClick, icon }) => (
  <div
    onClick={(evt) => evt.stopPropagation()} //don't close menu
    className={styles.heading}
  >
    {label}
    <span className={styles.headingIcon} onClick={onClick}>
      {icon}
    </span>
  </div>
);
