import React from 'react';
import { Heading } from './heading';
import { Divider } from './divider';
import { Option } from './option';
import { DropDownMenu } from '../menu';

export const Section = ({ section, closeOnOptionClick, close, tree, path }) => {
  switch (section.type) {
    case 'Heading':
      return (
        <Heading
          label={section.label}
          onClick={section.onClick}
          icon={section.icon}
        />
      );
    case 'Divider':
      return <Divider />;
    case 'Option':
      return (
        <Option
          label={section.label}
          onClick={section.onClick}
          description={section.description}
          icon={section.icon}
          selected={section.selected}
          closeOnOptionClick={closeOnOptionClick}
          close={close}
          disabled={section.disabled}
          inline={section.inline}
          title={section.title}
          upload={section.upload}
          onChange={section.onChange}
        />
      );
    case 'Menu':
      return (
        <DropDownMenu
          menu={section.menu}
          closeOnOptionClick={closeOnOptionClick}
          isNested
          closeParent={close}
          tree={tree}
          path={path}
        />
      );
    default:
      return null;
  }
};
