import _ from 'lodash';

/*
  Path helpers for menus with subsections
  Mainly to help keep track of the close() handlers for nested layers
  See menu.test.js for examples
 */

/*
  parentPath gets the parent path
  foo.bar[45] -> foo.bar
 */

export const parentPath = (path) =>
  path.includes('[') ? path.substr(0, path.lastIndexOf('[')) : path;

/*
  childPath gets the child path given the parent path and section index
  foo -> foo.sections[2]
 */

export const childPath = (path, sectionIndex) =>
  `${path}.sections[${sectionIndex}]`.replace(/^\./, '');

/*
  closePath gets the close handler's path given the path to the section
  foo -> foo.close
 */

export const closePath = (path) => `${path}.close`.replace(/^\./, '');

/*
  siblings gets the siblings given any tree and path to a child
 */

export const siblings = (tree, path) => _.get(tree, parentPath(path), []);

/*
  registerClose sets the close handler given any tree and path to a child
  (mutates intentionally, unavoidable in how we use the tree in Menu)
 */

export const registerClose = (tree, path, close) =>
  _.set(tree, closePath(path), close);
