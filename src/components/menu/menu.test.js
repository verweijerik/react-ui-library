import {
  childPath,
  parentPath,
  closePath,
  siblings,
  registerClose,
} from './layer/path';

const tree = {
  close: () => {},
  sections: [
    null,
    null,
    {
      close: () => {},
    },
    {
      close: () => {},
      sections: [
        null,
        null,
        null,
        {
          close: () => {},
        },
      ],
    },
  ],
};

describe('menu paths', () => {
  test('parentPath', () => {
    expect(parentPath('')).toBe('');
    expect(parentPath('foo.bar.baz')).toBe('foo.bar.baz');
    expect(parentPath('foo.bar[45]')).toBe('foo.bar');
  });
  test('childPath', () => {
    expect(childPath('', 6)).toBe('sections[6]');
    expect(childPath('foo', 2)).toBe('foo.sections[2]');
  });
  test('closePath', () => {
    expect(closePath('')).toBe('close');
    expect(closePath('foo.bar.baz')).toBe('foo.bar.baz.close');
  });
  test('siblings', () => {
    expect(siblings(tree, 'sections[3].sections[3]')).toStrictEqual(
      tree.sections[3].sections,
    );
  });
  test('registerClose', () => {
    expect(registerClose(tree, 'sections[3]', 'monkeys')).toStrictEqual({
      ...tree,
      sections: tree.sections.map((s, i) =>
        i === 3 ? { ...s, close: 'monkeys' } : s,
      ),
    });
  });
});
