import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import {
  FaChessKing,
  FaChessQueen,
  FaChessKnight,
  FaChessPawn,
  FaChartPie,
  FaChartLine,
  FaChartArea,
  FaChartBar,
  FaArrowAltCircleDown,
  FaProjectDiagram,
} from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Spacer } from '../layout/spacer/spacer';
import { Menu } from './menu';
import svgIcon from '../../images/icons/icons8-junction.svg';
import pngIcon from '../../images/icons/icon-drop.png';

const style = {
  height: '500px',
  width: '500px',
};

const onClickMenuItem = (evt) => action('onClickMenuItem')(evt);

const onFileChange = async (selectedFiles) => {
  await console.log('onFileChange', selectedFiles);
};

const onClickIcon = () => {
  console.log('heading icon event');
};

const basic = {
  label: 'Select',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'King A',
      upload: true,
      onChange: onFileChange,
    },
    {
      type: 'Option',
      label: 'Queen',
      description: 'killer',
      onClick: onClickMenuItem,
    },
    { type: 'Divider' },
    {
      type: 'Option',
      label: 'Knight',
      onClick: onClickMenuItem,
    },
    { type: 'Divider' },
    { type: 'Heading', label: 'Basic' },
    {
      type: 'Option',
      label: 'Pawn',
      onClick: onClickMenuItem,
    },
  ],
};

const right = {
  label: '123 m',
  trigger: 'DropDownButton',
  sections: [
    {
      type: 'Option',
      label: '403.5433',
      description: 'ft',
      inline: true,
      onClick: onClickMenuItem,
    },
    {
      type: 'Option',
      label: '123',
      description: 'm',
      inline: true,
      onClick: onClickMenuItem,
      selected: true,
    },
    {
      type: 'Option',
      label: '0.123',
      description: 'km',
      inline: true,
      onClick: onClickMenuItem,
    },
    {
      type: 'Option',
      label: '4842.4608',
      description: 'in',
      inline: true,
      onClick: onClickMenuItem,
    },
  ],
};

const rightLong = {
  label: '123 m',
  trigger: 'DropDownButton',
  sections: Array(30).fill({
    type: 'Option',
    label: '403.5433',
    description: 'ft',
    inline: true,
    onClick: onClickMenuItem,
  }),
};

const uploadFile = {
  label: 'Select',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'King A',
      upload: true,
      onChange: onFileChange,
    },
    {
      type: 'Option',
      label: 'Queen',
      description: 'killer',
      onClick: onClickMenuItem,
    },
  ],
};

const withIcons = {
  label: 'Select',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'King',
      icon: <FaChessKing color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    {
      type: 'Option',
      label: 'Queen',
      description: 'killer',
      icon: <FaChessQueen color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    { type: 'Divider' },
    {
      type: 'Option',
      label: 'Knight',
      icon: <FaChessKnight color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    { type: 'Divider' },
    { type: 'Heading', label: 'SVG and PNG icons' },
    {
      type: 'Option',
      label: 'Hydrated',
      icon: svgIcon,
      onClick: onClickMenuItem,
    },
    {
      type: 'Option',
      label: 'Hydrated',
      icon: pngIcon,
      onClick: onClickMenuItem,
    },
  ],
};

const disabled = {
  label: 'Select',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'King',
      icon: <FaChessKing color="#ff6600" />,
      onClick: onClickMenuItem,
    },
  ],
};

const withDisabledOptionItem = {
  label: 'Select',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'King',
      onClick: onClickMenuItem,
    },
    {
      type: 'Option',
      label: 'Queen',
      description: 'killer',
      onClick: onClickMenuItem,
      title: 'Killer queen',
    },
    { type: 'Divider' },
    {
      type: 'Option',
      label: 'Knight',
      disabled: true,
      title: 'Disabled because it is not a King or Queen.',
      onClick: onClickMenuItem,
    },
    { type: 'Divider' },
    { type: 'Heading', label: 'Basic' },
    {
      type: 'Option',
      label: 'Pawn',
      onClick: onClickMenuItem,
    },
  ],
};

const empty = {
  label: 'Select',
  trigger: 'Text',
  sections: [],
};

const withSelectedState = {
  label: 'Select',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'King',
      icon: <FaChessKing color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    {
      type: 'Option',
      label: 'Queen',
      description: 'killer',
      icon: <FaChessQueen color="#ff6600" />,
      onClick: () => console.log('deselect'),
      selected: true,
    },
    { type: 'Divider' },
    {
      type: 'Option',
      label: 'Knight',
      icon: <FaChessKnight color="#ff6600" />,
      onClick: onClickMenuItem,
    },
  ],
};

export const withSubMenus = {
  label: 'Menu',
  trigger: 'DropDownButton',
  sections: [
    { type: 'Heading', label: 'Charts' },
    {
      type: 'Option',
      label: 'Pie chart',
      icon: <FaChartPie color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    {
      type: 'Menu',
      menu: {
        trigger: 'Text',
        label: 'Network',
        sections: [
          { type: 'Heading', label: 'Network charts' },
          {
            type: 'Option',
            label: 'Network chart',
            icon: <FaProjectDiagram color="#ff6600" />,
            onClick: onClickMenuItem,
          },
        ],
      },
    },
    {
      type: 'Menu',
      menu: {
        trigger: 'Text',
        label: 'Time-series',
        sections: [
          { type: 'Heading', label: 'Time-series charts' },
          {
            type: 'Option',
            label: 'Bar chart',
            icon: <FaChartBar color="#ff6600" />,
            description: 'default',
            onClick: onClickMenuItem,
          },
          {
            type: 'Option',
            label: 'Line chart',
            icon: <FaChartLine color="#ff6600" />,
            onClick: onClickMenuItem,
          },
          {
            type: 'Menu',
            trigger: 'Text',
            menu: {
              trigger: 'Text',
              anchor: 'LEFT_TOP',
              label: 'Area',
              sections: [
                { type: 'Heading', label: 'Area charts' },
                {
                  type: 'Option',
                  label: 'Area chart',
                  icon: <FaChartArea color="#ff6600" />,
                  onClick: onClickMenuItem,
                },
              ],
            },
          },
        ],
      },
    },
  ],
};

const withDeprecatedSemanticIcon = {
  label: 'Menu',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Options' },
    {
      type: 'Option',
      label: 'Hydrated',
      icon: 'question circle outline',
      onClick: onClickMenuItem,
    },
  ],
};

const withButtonTrigger = {
  label: 'Menu',
  trigger: 'Button',
  sections: [
    { type: 'Heading', label: 'Options' },
    {
      type: 'Option',
      label: 'Hydrated',
      icon: <FaChartBar color="#ff6600" />,
      onClick: onClickMenuItem,
    },
  ],
};

const withDropDownButtonTrigger = {
  label: 'Menu',
  trigger: 'DropDownButton',
  colored: true,
  sections: [
    { type: 'Heading', label: 'Options' },
    {
      type: 'Option',
      label: 'Hydrated',
      icon: <FaChartBar color="#ff6600" />,
      onClick: onClickMenuItem,
    },
  ],
};

const withCustomTrigger = {
  trigger: 'Component',
  component: (
    <div>
      <FaArrowAltCircleDown color="#ff6600" />
    </div>
  ),
  sections: [
    { type: 'Heading', label: 'Options' },
    {
      type: 'Option',
      label: 'Hydrated',
      icon: <FaChartBar color="#ff6600" />,
      onClick: onClickMenuItem,
    },
  ],
};

const withLongOverflowingText = {
  label:
    'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
  trigger: 'Text',
  colored: true,
  sections: [
    {
      type: 'Heading',
      label:
        'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
    },
    {
      type: 'Option',
      label:
        'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
      icon: <FaChartPie color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    {
      type: 'Menu',
      menu: {
        trigger: 'Text',
        label:
          'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
        sections: [
          { type: 'Heading', label: 'Time-series charts' },
          {
            type: 'Option',
            label:
              'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
            icon: <FaChartBar color="#ff6600" />,
            description: 'default',
            onClick: onClickMenuItem,
          },
          {
            type: 'Option',
            label: 'Line chart',
            icon: <FaChartLine color="#ff6600" />,
            onClick: onClickMenuItem,
          },
        ],
      },
    },
  ],
};

const withLongOverflowingTextButton = {
  label:
    'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
  trigger: 'DropDownButton',
  colored: true,
  sections: [
    {
      type: 'Heading',
      label:
        'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
    },
    {
      type: 'Option',
      label:
        'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
      icon: <FaChartPie color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    {
      type: 'Menu',
      menu: {
        trigger: 'Text',
        label:
          'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
        sections: [
          { type: 'Heading', label: 'Time-series charts' },
          {
            type: 'Option',
            label:
              'Loremipsumdolorsitametconsecteturadipiscingelitseddoeiusmodtemporincididunt',
            icon: <FaChartBar color="#ff6600" />,
            description: 'default',
            onClick: onClickMenuItem,
          },
          {
            type: 'Option',
            label: 'Line chart',
            icon: <FaChartLine color="#ff6600" />,
            onClick: onClickMenuItem,
          },
        ],
      },
    },
  ],
};

const withAwkwardPlacement = {
  label: 'Select',
  trigger: 'Text',
  anchor: 'TOP_LEFT',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'King',
      icon: <FaChessKing color="#ff6600" />,
      onClick: onClickMenuItem,
    },
  ],
};

const withClickableHeadingIcon = {
  label: 'Select',
  trigger: 'Text',
  sections: [
    { type: 'Heading', label: 'Chess' },
    {
      type: 'Option',
      label: 'Knight',
      icon: <FaChessKnight color="#ff6600" />,
      onClick: onClickMenuItem,
    },
    { type: 'Divider' },
    {
      type: 'Heading',
      label: 'icon',
      icon: <FaChartBar />,
      onClick: onClickIcon,
    },
  ],
};

storiesOf('Basic/Menu', module)
  .add('default', () => (
    <Container style={style}>
      <Menu menu={basic} />
    </Container>
  ))
  .add('description on right', () => (
    <Container style={style}>
      <Menu menu={right} />
    </Container>
  ))
  .add('maxHeight', () => (
    <Container style={style}>
      <Menu menu={rightLong} maxHeight="240px" />
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style}>
      <Menu menu={disabled} disabled />
    </Container>
  ))
  .add('with disabled item', () => (
    <Container style={style}>
      <Menu menu={withDisabledOptionItem} />
    </Container>
  ))
  .add('empty', () => (
    <Container style={style}>
      <Menu menu={empty} />
    </Container>
  ))
  .add('with icons', () => (
    <Container style={style}>
      <Menu menu={withIcons} />
    </Container>
  ))
  .add('with selected state', () => (
    <Container style={style}>
      <Menu menu={withSelectedState} />
    </Container>
  ))
  .add('with sub-menus', () => (
    <Container style={style}>
      <Menu menu={withSubMenus} />
    </Container>
  ))
  .add('context menu', () => (
    <Container style={style}>
      <Menu
        menu={{ ...withSubMenus, trigger: 'Text', label: 'Right-click me' }}
        contextMenu
      />
    </Container>
  ))
  .add('with button trigger', () => (
    <Container style={style}>
      <Menu menu={withButtonTrigger} />
    </Container>
  ))
  .add('with dropdown button trigger', () => (
    <Container style={style}>
      <Menu menu={withDropDownButtonTrigger} />
    </Container>
  ))
  .add('small trigger', () => (
    <Container style={style}>
      <Menu menu={{ ...withDropDownButtonTrigger, small: true }} />
    </Container>
  ))
  .add('with custom trigger', () => (
    <Container style={style}>
      <Menu menu={withCustomTrigger} />
    </Container>
  ))
  .add('with fixed width', () => (
    <Container style={style}>
      <Menu menu={withDropDownButtonTrigger} width="400px" />
    </Container>
  ))
  .add('very wide', () => (
    <Container>
      <Menu menu={withSubMenus} width="700px" />
    </Container>
  ))
  .add('without close on option click', () => (
    <Container style={style}>
      <Menu menu={withSubMenus} closeOnOptionClick={false} />
    </Container>
  ))
  .add('without option with input file', () => (
    <Container style={style}>
      <Menu menu={uploadFile} closeOnOptionClick={false} />
    </Container>
  ))
  .add('with deprecated Semantic UI icon', () => (
    <Container style={style}>
      <p style={{ color: 'red' }}>Deprecated (use react-icons)</p>
      <Menu menu={withDeprecatedSemanticIcon} />
    </Container>
  ))
  .add('with clickable Heading Icon', () => (
    <Container style={style}>
      <Menu menu={withClickableHeadingIcon} />
    </Container>
  ))
  .add('with overflowing text', () => (
    <Container style={style}>
      <div style={{ marginBottom: 30 }}>
        <Menu menu={withLongOverflowingText} />
      </div>
      <div>
        <Menu menu={withLongOverflowingTextButton} />
      </div>
    </Container>
  ))
  .add('with auto-adjust placement', () => (
    <Container margin={false}>
      <div
        style={{
          height: '500px',
          width: 'auto',
          overflow: 'hidden',
          backgroundColor: '#f5f7f9',
        }}
      >
        <div style={{ float: 'right' }}>
          <Menu menu={withAwkwardPlacement} />
        </div>
      </div>
    </Container>
  ))
  .add('with JSX layer', () => (
    <Container>
      <Menu
        menu={{
          label: 'Select',
          trigger: 'DropDownButton',
          sections: <div>Testing</div>,
        }}
      />
    </Container>
  ));
