import React, { isValidElement } from 'react';
import PropTypes from 'prop-types';
import ResizeObserver from 'resize-observer-polyfill';
import { useToggleLayer } from 'react-laag';
import { Trigger, Layer, placement, siblings, registerClose } from '.';
import styles from './menu.module.less';

const useContextMenu = (
  sections,
  width,
  onClick,
  anchor,
  possibleAnchors,
  closeOnOptionClick,
  closeParent,
  tree,
  path,
  groupOrder,
  fixedPosition,
  maxHeight,
) => {
  const [element, props] = useToggleLayer(
    ({ isOpen, layerProps, close }) =>
      isOpen && (
        <div
          {...layerProps} // eslint-disable-line react/jsx-props-no-spreading
        >
          <Layer
            isNested={false}
            width={width}
            sections={sections}
            closeOnOptionClick={closeOnOptionClick}
            close={closeParent || close}
            tree={tree}
            path={path}
            groupOrder={groupOrder}
            maxHeight={maxHeight}
          />
        </div>
      ),
    {
      placement: {
        ...placement(anchor),
        possibleAnchors,
      },
      fixed: fixedPosition,
      ResizeObserver,
      closeOnOutsideClick: !closeParent,
    },
  );
  return [element, props.openFromContextMenuEvent];
};

const ContextMenu = ({
  menu,
  width,
  disabled,
  closeOnOptionClick,
  closeParent,
  tree,
  path,
  groupOrder,
}) => {
  const {
    label,
    trigger,
    fullHeightTrigger,
    colored,
    small,
    component,
    anchor,
    possibleAnchors,
  } = menu;
  const [element, onContextMenu] = useContextMenu(
    menu.sections,
    width,
    () => {},
    anchor,
    possibleAnchors,
    closeOnOptionClick,
    closeParent,
    tree,
    path,
  );
  const isDisabled =
    disabled || (!isValidElement(menu.sections) && !menu.sections.length);
  return (
    <div onContextMenu={onContextMenu}>
      <Trigger
        width={width}
        fullHeight={fullHeightTrigger}
        isDisabled={isDisabled}
        trigger={trigger}
        label={label}
        onClickTrigger={() => {}}
        colored={colored}
        small={small}
        isNested
        component={component}
        contextMenu
        groupOrder={groupOrder}
      />
      {element}
    </div>
  );
};

export const DropDownMenu = ({
  menu,
  width,
  disabled,
  isNested,
  closeOnOptionClick,
  closeParent,
  tree,
  path,
  groupOrder,
  fixedPosition,
  maxHeight,
}) => {
  const {
    label,
    trigger,
    fullHeightTrigger,
    colored,
    small,
    sections,
    component,
    anchor,
    possibleAnchors,
  } = menu;

  const [element, toggleLayerProps] = useToggleLayer(
    ({ layerProps, isOpen, close }) =>
      isOpen && (
        <div
          {...layerProps} // eslint-disable-line react/jsx-props-no-spreading
          className={styles.layerContainer}
        >
          <Layer
            sections={sections}
            isNested={isNested}
            width={width}
            closeOnOptionClick={closeOnOptionClick}
            close={closeParent || close}
            tree={tree}
            path={path}
            groupOrder={groupOrder}
            maxHeight={maxHeight}
          />
        </div>
      ),
    {
      placement: {
        ...placement(anchor, isNested),
        possibleAnchors,
      },
      fixed: fixedPosition,
      ResizeObserver,
      closeOnOutsideClick: !closeParent,
    },
  );

  const isDisabled =
    disabled || (!isValidElement(menu.sections) && !menu.sections.length);

  //keep a record of close handlers so we can close sibling layers
  registerClose(tree, path, toggleLayerProps.close);

  const onClickTrigger = (evt) => {
    if (!disabled) {
      if (toggleLayerProps.isOpen) {
        toggleLayerProps.close();
      } else {
        //first close any sibling menus
        siblings(tree, path).forEach((section) => {
          if (section.close) {
            section.close();
          }
        });
        toggleLayerProps.openFromMouseEvent(evt);
      }
    }
  };

  return (
    <Trigger
      isDisabled={isDisabled}
      width={width}
      fullHeight={fullHeightTrigger}
      element={element}
      trigger={trigger}
      label={label}
      onClickTrigger={onClickTrigger}
      colored={colored}
      small={small}
      isNested={isNested}
      component={component}
      groupOrder={groupOrder}
    />
  );
};

export const Menu = (props) => {
  return props.contextMenu ? (
    <ContextMenu
      {...props} // eslint-disable-line react/jsx-props-no-spreading
      tree={{}}
      path=""
    />
  ) : (
    <DropDownMenu
      {...props} // eslint-disable-line react/jsx-props-no-spreading
      tree={{}}
      path=""
    />
  );
};

const menuSectionShape = PropTypes.oneOfType([
  PropTypes.shape({
    //Heading
    type: PropTypes.oneOf(['Heading']),
    label: PropTypes.string.isRequired,
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    onClick: PropTypes.func,
  }),
  PropTypes.shape({
    //Divider
    type: PropTypes.oneOf(['Divider']),
  }),
  PropTypes.shape({
    //Options
    type: PropTypes.oneOf(['Option']),
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    description: PropTypes.string,
    onClick: PropTypes.func,
    selected: PropTypes.bool,
    disabled: PropTypes.bool,
    inline: PropTypes.bool,
    title: PropTypes.string,
    download: PropTypes.bool,
    onChange: PropTypes.func,
  }),
  PropTypes.shape({
    //Sub-menu
    type: PropTypes.oneOf(['Menu']),
    menu: PropTypes.object,
  }),
]);

const menuShape = PropTypes.shape({
  trigger: PropTypes.oneOf(['Text', 'Button', 'DropDownButton', 'Component']),
  colored: PropTypes.bool,
  small: PropTypes.bool,
  label: PropTypes.string,
  component: PropTypes.node, //JSX
  sections: PropTypes.oneOfType([
    PropTypes.arrayOf(menuSectionShape),
    PropTypes.node, //JSX
  ]).isRequired,
  anchor: PropTypes.string,
  possibleAnchors: PropTypes.arrayOf(PropTypes.string),
});

Menu.defaultProps = {
  width: '',
  fullHeightTrigger: false,
  disabled: false,
  closeOnOptionClick: true,
  groupOrder: null,
  fixedPosition: true,
  maxHeight: null,
};

Menu.propTypes = {
  menu: menuShape.isRequired,
  disabled: PropTypes.bool,
  width: PropTypes.string,
  fullHeightTrigger: PropTypes.bool,
  closeOnOptionClick: PropTypes.bool,
  groupOrder: PropTypes.string,
  fixedPosition: PropTypes.bool,
  maxHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
