import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from './link';
import styles from './breadcrumb.module.less';

export const Breadcrumb = ({ links, small }) => {
  return (
    <div className={cx(styles.breadcrumb, small && styles.small)}>
      {links.map((link, i) => {
        const isFinal = links.length === i + 1;
        const { type, label, url, onClick, active, disabled, element } = link;
        return (
          <span key={i}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <Link
              type={type}
              label={label}
              url={url}
              onClick={onClick}
              active={active}
              disabled={disabled}
              element={element}
            />
            {!isFinal && <span className={styles.separator}>/</span>}
          </span>
        );
      })}
    </div>
  );
};

Breadcrumb.defaultProps = {
  small: false,
};

Breadcrumb.propTypes = {
  links: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.oneOf(['label', 'link', 'custom']),
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      url: PropTypes.string,
      onClick: PropTypes.func,
      active: PropTypes.bool,
      element: PropTypes.node,
    }),
  ).isRequired,
  small: PropTypes.bool,
};
