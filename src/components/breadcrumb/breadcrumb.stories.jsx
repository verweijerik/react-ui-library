import React from 'react';
import { storiesOf } from '@storybook/react';
import { Breadcrumb } from './breadcrumb';
import { Container } from '../../helpers/container';

const style = {
  height: '500px',
  width: '500px',
};

storiesOf('Navigation/Breadcrumb', module)
  .add('default', () => (
    <Container style={style}>
      <Breadcrumb
        links={[
          {
            label: 'Animals',
            onClick: () => console.log('label clicked'),
          },
          {
            label: 'Mammals',
            url: 'animals/mammals',
            disabled: true,
          },
          {
            label: 'Cats',
            url: 'animals/mammals/cats',
          },
          {
            type: 'custom', //e.g. react router <Link>
            element: <span>Large</span>,
          },
          {
            label: 'Tiger',
            url: 'animals/mammals/cats/tiger',
            active: true,
          },
        ]}
      />
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <Breadcrumb
        small
        links={[
          {
            type: 'label',
            label: 'Animals',
            url: 'animals',
            onClick: () => console.log('link clicked'),
          },
          {
            type: 'link',
            label: 'Mammals',
            url: 'animals/mammals',
            disabled: true,
          },
          {
            type: 'link',
            label: 'Cats',
            url: 'animals/mammals/cats',
          },
          {
            type: 'custom', //e.g. react router <Link>
            element: <span>Large</span>,
          },
          {
            type: 'link',
            label: 'Tiger',
            url: 'animals/mammals/cats/tiger',
            active: true,
          },
        ]}
      />
    </Container>
  ));
