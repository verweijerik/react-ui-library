import React, { isValidElement } from 'react';
import cx from 'classnames';
import styles from './breadcrumb.module.less';

export const Link = ({
  type,
  label,
  url,
  onClick,
  active,
  disabled,
  element,
}) => {
  const isLink = type === 'link' && url !== undefined;
  const isCustom = type === 'custom' && isValidElement(element);
  return (
    <span
      onClick={onClick}
      className={cx(
        styles.labelContainer,
        active && styles.active,
        disabled && styles.disabled,
      )}
    >
      {isLink ? <a href={url}>{label}</a> : isCustom ? element : label}
    </span>
  );
};
