import _ from 'lodash';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './table.module.less';
import { Title } from './title/title';
import { Footer } from './footer/footer';
import { Row, rowShape } from './row/row';
import { getColumnCount, hasRowActions } from './table.viewdata';
import { paginationShape } from '../pagination/pagination';
import { actionsShape } from '../actions/actions';

export const Table = (props) => {
  const {
    fixedWidth,
    columnWidths,
    footer,
    name,
    rows,
    actions,
    columnAlignment,
    stickyHeaders,
    bordered = true,
  } = props.table;
  const headers = _.get(props, 'table.headers', []);
  const columnCount = getColumnCount(rows, headers);
  const rowActions = hasRowActions(rows, headers);
  const colSpan = columnCount + (rowActions ? 1 : 0);
  const [rowsMetaData, setRowsMetaData] = useState([]);

  return (
    <table
      className={cx(styles.table, bordered ? styles.bordered : '')}
      style={{ minWidth: fixedWidth || '' }}
    >
      <thead>
        <Title actions={actions} name={name} colSpan={colSpan} />
        {headers.map((row, rowIndex) => (
          <Row
            rowIndex={rowIndex}
            isHeader
            row={row}
            columnCount={columnCount}
            columnWidths={columnWidths}
            colSpan={colSpan}
            hasRowActions={rowActions}
            key={`0_${rowIndex}`}
            columnAlignment={columnAlignment}
            rowsMetaData={stickyHeaders ? rowsMetaData : undefined}
            setRowsMetaData={stickyHeaders ? setRowsMetaData : undefined}
          />
        ))}
      </thead>
      <tbody>
        {rows.map((row, rowIndex) => (
          <Row
            rowIndex={rowIndex}
            row={row}
            columnCount={columnCount}
            colSpan={colSpan}
            hasRowActions={rowActions}
            key={`1_${rowIndex}`}
            columnAlignment={columnAlignment}
          />
        ))}
      </tbody>
      {footer && (
        <Footer
          colSpan={colSpan}
          pagination={footer.pagination}
          actions={footer.actions}
        />
      )}
    </table>
  );
};

Table.defaultProps = {};

Table.propTypes = {
  table: PropTypes.shape({
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    actions: actionsShape,
    fixedWidth: PropTypes.string,
    columnWidths: PropTypes.arrayOf(PropTypes.string),
    className: PropTypes.string,
    columnAlignment: PropTypes.arrayOf(PropTypes.oneOf(['left', 'right'])),
    stickyHeaders: PropTypes.string,
    headers: PropTypes.arrayOf(rowShape),
    rows: PropTypes.arrayOf(rowShape),
    footer: PropTypes.shape({
      pagination: paginationShape,
      actions: actionsShape,
    }),
    bordered: PropTypes.bool,
  }).isRequired,
};
