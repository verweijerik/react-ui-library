import { hasRowActions, getColumnCount } from './table.viewdata';

describe('table', () => {
  test('hasRowActions', () => {
    const withActions = [{ cells: [], actions: [{}] }, { cells: [] }];
    const withOutActions = [{ cells: [] }, { cells: [] }];
    expect(hasRowActions(withOutActions, withOutActions)).toBe(false);
    expect(hasRowActions(withActions, withOutActions)).toBe(true);
  });

  test('getColumnCount', () => {
    const noRows = [];
    const unevenColumns = [
      { cells: [{}, {}, {}] },
      { cells: [{}, {}, {}, {}, {}] },
      { cells: [{}, {}, {}] },
    ];
    expect(getColumnCount(noRows, unevenColumns)).toBe(5);
  });

  test('getColumnCount with colSpan', () => {
    const noRows = [];
    const unevenColumns = [
      { cells: [{}, { colSpan: 2 }, {}] },
      { cells: [{}, {}] },
      { cells: [{}, {}, {}] },
    ];
    expect(getColumnCount(noRows, unevenColumns)).toBe(4);
  });
});
