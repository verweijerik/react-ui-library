import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './cell.module.less';
import iconStyles from '../icon/icon.module.less';
import actionStyles from '../../actions/actions.module.less';
import { CheckBox } from '../../check-box/check-box';
import { Select } from '../../select/select';
import { selectOptionShape } from '../../select/custom-select/custom-select';
import { Input } from '../../input/input';
import { Actions, actionsShape } from '../../actions/actions';
import { Tooltip } from '../../tooltip/tooltip';
import { Popover } from '../../popover/popover';
import { Slider } from '../../slider/slider';
import { Icon } from '../../icon/icon';
import { isStringNumberOrNode } from '../../../helpers/types';

const InputCell = (props) => {
  const { cell, columnAlignment } = props;
  return (
    <Input
      value={cell.value}
      onChange={(ev) => cell.onChange(ev)}
      placeholder={cell.placeholder}
      error={cell.error}
      warning={cell.warning}
      disabled={cell.disabled}
      isInTable
      maxTooltipWidth={cell.maxTooltipWidth}
      right={columnAlignment === 'right'}
    />
  );
};

const SelectCell = (props) => {
  const { cell, columnAlignment } = props;

  return (
    <Select
      borderRadius={0}
      menu={cell.menu} //deprecated
      options={cell.options}
      onChange={(ev) => cell.onChange(ev)}
      onCreate={cell.onCreate ? (ev) => cell.onCreate(ev) : undefined}
      error={cell.error}
      warning={cell.warning}
      disabled={cell.disabled}
      placeholder={cell.placeholder}
      isInTable
      value={cell.value}
      native={cell.native}
      clearable={cell.clearable}
      deprecatedEventHandler={cell.deprecatedEventHandler}
      maxTooltipWidth={cell.maxTooltipWidth}
      width={cell.width}
      autoLayerWidth={cell.autoLayerWidth}
      right={columnAlignment === 'right'}
    />
  );
};

const LinkCell = (props) => {
  const { cell } = props;
  const { error, warning, tooltip, maxTooltipWidth } = cell;
  return (
    <Tooltip
      error={!!error}
      warning={!!warning}
      text={tooltip || error || warning}
      enabled={
        (tooltip && isStringNumberOrNode(tooltip)) ||
        (error && isStringNumberOrNode(error)) ||
        (warning && isStringNumberOrNode(warning)) ||
        false
      }
      maxWidth={maxTooltipWidth}
    >
      <div
        className={cx(
          styles.staticCell,
          error ? styles.error : warning ? styles.warning : '',
        )}
      >
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a
          className={cx(cell.disabled && styles.disabledLink)}
          onClick={
            cell.disabled
              ? () => {}
              : (evt) => {
                  evt.stopPropagation();
                  cell.onClick(evt);
                }
          }
        >
          {cell.value}
        </a>
      </div>
    </Tooltip>
  );
};

const CheckBoxCell = (props) => {
  const { cell } = props;
  return (
    <CheckBox
      label={cell.label}
      checked={cell.checked}
      isInTable
      disabled={cell.disabled}
      onChange={(ev) => cell.onChange(ev)}
    />
  );
};

const SliderCell = (props) => {
  const { cell } = props;
  return (
    <div className={styles.inputWrapper}>
      <Slider
        label={cell.label}
        value={cell.value}
        min={cell.min}
        max={cell.max}
        step={cell.step}
        marks={cell.marks}
        onChange={(ev) => cell.onChange(ev)}
        showArrows={cell.showArrows}
        showToolTop={cell.showTooltip}
        tooltipFormatter={cell.tooltipFormatter}
        disabled={cell.disabled}
      />
    </div>
  );
};

const Sort = (props) => {
  const { cell } = props;
  const { hasSort, sort, sortPriority, onSort } = cell;
  return hasSort ? (
    <span className={styles.sortingCellIcon}>
      <i
        className={`sort amount ${sort || 'up disabled grey'} icon`}
        onClick={(evt) => onSort(evt)}
      />
      {sortPriority && sort ? <sup>{sortPriority}</sup> : null}
    </span>
  ) : null;
};

const IconCell = (props) => {
  const { cell } = props;
  return (
    <div className={cx(actionStyles.actions)}>
      <span className={cx(actionStyles.action)}>
        <Icon icon={cell.icon} />
      </span>
    </div>
  );
};

const ActionsCell = (props) => {
  const { cell } = props;
  return <Actions actions={cell.actions} />;
};

const StaticCell = (props) => {
  const { cell, isHeader } = props;
  const { error, warning, tooltip, maxTooltipWidth } = cell;
  const field = (
    <div
      className={cx(
        styles.staticCell,
        error ? styles.error : warning ? styles.warning : '',
      )}
    >
      {cell.value}
      {isHeader ? <Sort cell={cell} /> : null}
    </div>
  );
  return (
    <Tooltip
      error={!!error}
      warning={!!warning}
      text={tooltip || error || warning}
      enabled={
        (tooltip && isStringNumberOrNode(tooltip)) ||
        (error && isStringNumberOrNode(error)) ||
        (warning && isStringNumberOrNode(warning)) ||
        false
      }
      maxWidth={maxTooltipWidth}
      display="block"
    >
      {field}
    </Tooltip>
  );
};

const PopoverCell = (props) => {
  const { cell } = props;
  const { content, fullWidth, closeOnOutsideClick } = cell;

  return (
    <Popover
      content={content}
      fullWidth={fullWidth}
      closeOnOutsideClick={closeOnOutsideClick}
    >
      <div className={styles.popover}>
        <StaticCell cell={cell} />
      </div>
    </Popover>
  );
};

const InputCellWrapper = (props) => {
  const { cell, columnAlignment } = props;

  return (
    <div className={styles.inputWrapper}>
      {cell.type === 'Input' ? (
        <InputCell cell={cell} columnAlignment={columnAlignment} />
      ) : cell.type === 'Select' ? (
        <SelectCell cell={cell} columnAlignment={columnAlignment} />
      ) : null}
    </div>
  );
};

const CellWrapper = (props) => {
  const { cell, isHeader, columnAlignment } = props;
  if (cell) {
    switch (cell.type) {
      case 'Input':
      case 'Select':
        return (
          <InputCellWrapper cell={cell} columnAlignment={columnAlignment} />
        );
      case 'Link':
        return <LinkCell cell={cell} />;
      case 'CheckBox':
        return <CheckBoxCell cell={cell} />;
      case 'Slider':
        return <SliderCell cell={cell} />;
      case 'Icon':
        return <IconCell cell={cell} />;
      case 'Actions':
        return <ActionsCell cell={cell} />;
      case 'Popover':
        return <PopoverCell cell={cell} />;
      default:
        return <StaticCell cell={cell} isHeader={isHeader} />;
    }
  }
};

export const Cell = (props) => {
  const { cell, isHeader, columnAlignment, rowMetaData, width } = props;
  const { style, colSpan } = cell;

  const className = cx(
    styles.cellWrapper,
    cell.type === 'Input' || cell.type === 'Select' || cell.type === 'Popover'
      ? styles.inputCell
      : '',
    cell.type === 'Slider' ? styles.sliderCell : '',
    cell.type === 'CheckBox' ? styles.checkBoxCell : '',
    cell.type === 'Icon' || cell.type === 'Actions' ? styles.iconCell : '',
    cell.type === 'Icon' ? iconStyles.noHighlight : '',
    cell.hasSort ? styles.sortingCell : null,
    columnAlignment === 'right' ? styles.rightAligned : styles.leftAligned,
  );

  const stickyStyle = () => {
    let styles = {};
    if (rowMetaData) {
      styles = rowMetaData.stickyOffset
        ? {
            position: 'sticky',
            top: `${rowMetaData.stickyOffset}px`,
            zIndex: 1,
          }
        : { position: 'sticky', top: 0, zIndex: 1 };
    }
    return styles;
  };

  const isWidthCustomSelect =
    cell.type === 'Select' && cell.native !== true && cell.width !== undefined;

  return isHeader ? (
    <th
      className={className}
      style={{
        ...style,
        ...stickyStyle(),
        ...(isWidthCustomSelect ? { width: '1%' } : {}),
        ...(width ? { minWidth: width } : {}),
      }}
      colSpan={colSpan}
    >
      <CellWrapper
        cell={cell}
        columnAlignment={columnAlignment}
        isHeader={isHeader}
      />
    </th>
  ) : (
    <td
      className={className}
      style={{
        ...style,
        ...(isWidthCustomSelect ? { width: '1%' } : {}),
        ...(width ? { minWidth: width } : {}),
      }}
      colSpan={colSpan}
    >
      <CellWrapper
        cell={cell}
        columnAlignment={columnAlignment}
        isHeader={isHeader}
      />
    </td>
  );
};

const cellType = PropTypes.oneOf([
  'Static',
  'Icon',
  'Select',
  'Input',
  'Popover',
  'CheckBox',
  'Actions',
  'Link',
]);

export const cellShape = PropTypes.oneOfType([
  PropTypes.shape({
    //Static
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    type: cellType,
    colSpan: PropTypes.number,
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    warning: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    tooltip: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    maxTooltipWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  PropTypes.shape({
    //Icon
    type: cellType.isRequired,
    icon: PropTypes.string.isRequired,
    colSpan: PropTypes.number,
  }),
  PropTypes.shape({
    //Select
    options: PropTypes.arrayOf(selectOptionShape),
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    type: cellType.isRequired,
    onChange: PropTypes.func,
    onCreate: PropTypes.func,
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    warning: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    tooltip: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    colSpan: PropTypes.number,
    clearable: PropTypes.bool,
    native: PropTypes.bool,
    deprecatedEventHandler: PropTypes.bool,
    maxTooltipWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    width: PropTypes.string,
    autoLayerWidth: PropTypes.bool,
  }),
  PropTypes.shape({
    //Input
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    type: cellType.isRequired,
    onChange: PropTypes.func,
    placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    warning: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    tooltip: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    hasSort: PropTypes.bool,
    sort: PropTypes.oneOf(['', 'up', 'down']), //only for header cells
    sortPriority: PropTypes.number, //only for header cells
    onSort: PropTypes.func, //only for header cells
    colSpan: PropTypes.number,
    maxTooltipWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  PropTypes.shape({
    //CheckBox
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    checked: PropTypes.bool,
    type: cellType.isRequired,
    onClick: PropTypes.func,
    colSpan: PropTypes.number,
  }),
  PropTypes.shape({
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    value: PropTypes.number.isRequired,
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    step: PropTypes.number,
    marks: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    showArrows: PropTypes.bool,
    showToolTop: PropTypes.bool,
    tooltipFormatter: PropTypes.func,
    disabled: PropTypes.bool,
  }),
  PropTypes.shape({
    //Actions
    actions: PropTypes.arrayOf(actionsShape),
    type: cellType.isRequired,
    colSpan: PropTypes.number,
  }),
  PropTypes.shape({
    //Link
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    type: cellType.isRequired,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    colSpan: PropTypes.number,
    error: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    warning: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    tooltip: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
  }),
  PropTypes.shape({
    //JSX / sub-component
    value: PropTypes.node, //React component
    onClick: PropTypes.func,
    colSpan: PropTypes.number,
  }),
]);

Cell.defaultProps = {
  isHeader: false,
  style: {},
  cell: {},
};

Cell.propTypes = {
  isHeader: PropTypes.bool,
  style: PropTypes.object,
  cell: cellShape,
};
