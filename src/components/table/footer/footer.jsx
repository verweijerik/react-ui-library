import React from 'react';
import PropTypes from 'prop-types';
import styles from './footer.module.less';
import { Pagination, paginationShape } from '../../pagination/pagination';
import { Actions, actionsShape } from '../../actions/actions';

export const Footer = (props) => {
  const { colSpan, pagination, actions } = props;
  const hasActions = actions && actions.length;

  const hasPagination = () => {
    if (pagination) {
      const { rowCount, rowsPerPage } = pagination;
      const minRows = rowsPerPage.options[0].value;
      return rowCount > minRows;
    }
    return false;
  };

  const showFooter = hasActions || hasPagination();

  return showFooter ? (
    <tfoot>
      <tr>
        <td colSpan={colSpan}>
          <div className={styles.footer}>
            {hasPagination() ? <Pagination pagination={pagination} /> : <div />}
            <div>
              <Actions actions={actions} isBlockElement />
            </div>
          </div>
        </td>
      </tr>
    </tfoot>
  ) : null;
};

Footer.defaultProps = {
  actions: [],
  pagination: null,
};

Footer.propTypes = {
  actions: actionsShape,
  colSpan: PropTypes.number.isRequired,
  pagination: paginationShape,
};
