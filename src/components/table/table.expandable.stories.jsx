import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaPlus, FaMinus } from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Table } from './table';

const style = {
  height: '500px',
  width: '200px',
};

const TableContainer = () => {
  const [expandedRow, setExpandedRow] = useState(null);

  const toggleExpandedRow = (rowIndex) =>
    expandedRow === rowIndex ? setExpandedRow(null) : setExpandedRow(rowIndex);

  const table = {
    fixedWidth: '700px',
    headers: [
      {
        cells: [
          { value: 'Name' },
          { value: 'Weight' },
          { value: 'Energy' },
          { value: 'Origin' },
        ],
        actions: [{ label: 'Add', icon: <FaPlus />, onClick: () => {} }],
      },
      {
        cells: [
          {},
          {
            value: 'Click on table rows to expand an inline form',
            colSpan: 3,
          },
        ],
      },
      {
        cells: [
          {},
          { value: 'kg' },
          {
            options: [
              { label: 'kcal / 100g', value: 'kcal / 100g' },
              { label: 'kJ / 100g', value: 'kJ / 100g' },
            ],
            value: { label: 'kJ / 100g', value: 'kJ / 100g' },
            type: 'Select',
            native: true,
            onChange: () => {},
          },
          {},
        ],
      },
    ],
    rows: [
      {
        cells: [
          { value: 'Brown rice', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 361 },
          { value: 'Vietnam' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(0),
        expandedContent: {
          content:
            expandedRow === 0 ? <div>Example expanded content</div> : null,
        },
      },
      {
        cells: [
          { value: 'Buckwheat', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 358 },
          { value: 'Poland' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(1),
        expandedContent: {
          content:
            expandedRow === 1 ? <div>Example expanded content</div> : null,
        },
      },
      {
        cells: [
          { value: 'Couscous', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 368 },
          { value: 'France' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(2),
        expandedContent: {
          content:
            expandedRow === 2 ? <div>Example expanded content</div> : null,
        },
      },
      {
        cells: [
          { value: 'Oatmeal', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 374 },
          { value: 'Russia' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(3),
        expandedContent: {
          content:
            expandedRow === 3 ? <div>Example expanded content</div> : null,
        },
      },
      {
        cells: [
          { value: 'Pasta', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 359 },
          { value: 'Italy' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(4),
        expandedContent: {
          content:
            expandedRow === 4 ? <div>Example expanded content</div> : null,
        },
      },
      {
        cells: [
          { value: 'Polenta', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 370 },
          { value: 'Italy' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(5),
        expandedContent: {
          content:
            expandedRow === 5 ? <div>Example expanded content</div> : null,
        },
      },
      {
        cells: [
          { value: 'Potatoes', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 85 },
          { value: 'China' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(6),
        expandedContent: {
          content:
            expandedRow === 6 ? <div>Example expanded content</div> : null,
        },
      },
      {
        cells: [
          { value: 'White rice', type: 'Link', onClick: () => {} },
          { value: 50 },
          { value: 355 },
          { value: 'India' },
        ],
        actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
        onRowClick: () => toggleExpandedRow(7),
        expandedContent: {
          content:
            expandedRow === 7 ? <div>Example expanded content</div> : null,
          flush: true,
        },
      },
    ],
  };

  return <Table table={table} />;
};

storiesOf('Basic/Table', module).add('expandable', () => (
  <Container style={style}>
    <TableContainer />
  </Container>
));
