import React from 'react';
import PropTypes from 'prop-types';
import styles from './title.module.less';
import { Actions, actionsShape } from '../../actions/actions';

export const Title = (props) => {
  const { colSpan, name, actions } = props;
  return name ? (
    <tr>
      <th colSpan={colSpan} className={styles.cellWrapper}>
        <div className={styles.title}>
          <div>{name}</div>
          <div>{actions && <Actions actions={actions} isBlockElement />}</div>
        </div>
      </th>
    </tr>
  ) : null;
};

Title.defaultProps = {
  name: null,
  actions: null,
};

Title.propTypes = {
  actions: actionsShape,
  colSpan: PropTypes.number.isRequired,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
