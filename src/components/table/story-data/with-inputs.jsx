import React from 'react';
import { Slider } from '../../slider/slider';

export const withInputs = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight' },
        { value: 'Energy' },
        { value: 'Origin' },
        { value: 'Order' },
        {},
      ],
    },
    {
      cells: [
        {},
        { value: 'kg' },
        {
          type: 'Select',
          options: [
            { label: 'kcal / 100g', value: 'kcal / 100g' },
            { label: 'kJ / 100g', value: 'kJ / 100g' },
          ],
          value: { label: 'kcal / 100g', value: 'kcal / 100g' },
          onChange: () => {},
        },
        {},
        {},
        {},
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        { value: 361, type: 'Input', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Vietnam', value: 'Vietnam' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Vietnam', value: 'Vietnam' },
          onChange: () => {},
        },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        { value: 358, type: 'Input', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Poland', value: 'Poland' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Poland', value: 'Poland' },
          onChange: () => {},
        },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        { value: 368, type: 'Input', onChange: () => {} },
        { value: 'France' },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        { value: 374, type: 'Input', onChange: () => {} },
        { value: 'Russia' },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        {
          type: 'Slider',
          value: 35,
          min: 0,
          max: 100,
          step: 1,
          marks: [
            { value: 20, label: 'S1...', tooltip: 'S1 Production' },
            { value: 40 },
            { value: 60, label: <strong>60 (best)</strong> },
            { value: 80, label: 'S4...', tooltip: 'S4 Shut-in' },
            { value: 100, label: 'S5...', tooltip: 'S5 Circulate' },
          ],
          onChange: () => {},
          showArrows: true,
          showTooltip: true,
        },
        { value: 'Italy' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          onChange: () => {},
        },
        { value: 370, type: 'Input', onChange: () => {} },
        { value: 'Italy' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        {
          type: 'Select',
          native: true,
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 100, value: 100 },
          onChange: () => {},
        },
        { value: 85, type: 'Input', onChange: () => {} },
        { value: 'China' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        {
          type: 'Select', //deprecated usage (use new interface)
          menu: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: 100,
          onChange: () => {},
        },
        { value: 355, type: 'Input', onChange: () => {} },
        { value: 'India' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
  ],
};
