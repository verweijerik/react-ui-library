export const withTooltipStaticCells = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name', tooltip: 'Tooltip for name' },
        { value: 'Weight (kg)', tooltip: 'Tooltip for weight' },
        { value: 'Energy (kcal / 100g)', tooltip: 'Tooltip for energy' },
        { value: 'Origin', tooltip: 'Tooltip for origin' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice', tooltip: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam', tooltip: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat', tooltip: 'buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland', tooltip: 'Poland' },
      ],
    },
  ],
};
