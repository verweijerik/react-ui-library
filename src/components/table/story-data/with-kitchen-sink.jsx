import React from 'react';
import {
  FaPlus,
  FaMinus,
  FaArrowUp,
  FaArrowDown,
  FaShareSquare,
  FaHeartbeat,
  FaCog,
  FaQuestionCircle,
  FaExclamationTriangle,
  FaChartLine,
} from 'react-icons/fa';

export const withKitchenSink = {
  name: 'Food',
  fixedWidth: '700px',
  actions: [
    {
      childComponent: (
        <div>
          <FaChartLine />
        </div>
      ),
    },
    {
      label: 'More',
      subActions: [
        {
          label: 'Settings',
          icon: <FaCog />,
          onClick: () => {},
        },
      ],
    },
    { label: 'Add', icon: <FaPlus />, onClick: () => {} },
  ],
  headers: [
    {
      cells: [
        {},
        {
          value: 'Name',
          sort: 'up',
          sortPriority: 1,
          onSort: () => {},
        },
        { value: 'Weight' },
        {
          value: 'Energy',
          sort: 'down',
          sortPriority: 2,
          onSort: () => {},
        },
        { value: 'Origin' },
        { value: 'Order' },
        {},
      ],
    },
    {
      cells: [
        {},
        {
          type: 'Actions',
          actions: [
            {
              label: 'Help',
              icon: <FaQuestionCircle />,
              onClick: () => {},
            },
          ],
        },
        {
          type: 'Select',
          options: [
            { label: 'kg', value: 'kg' },
            { label: 'tonnes', value: 'tonnes' },
          ],
          value: { label: 'kg', value: 'kg' },
          native: true,
          onChange: () => {},
        },
        {
          type: 'Select',
          options: [
            { label: 'kcal / 100g', value: 'kcal / 100g' },
            { label: 'kJ / 100g', value: 'kJ / 100g' },
          ],
          value: { label: 'kcal / 100g', value: 'kcal / 100g' },
          native: true,
          onChange: () => {},
        },
        {},
        {},
        {},
      ],
    },
    {
      cells: [
        {},
        { value: '', type: 'Input', placeholder: 'Search', onChange: () => {} },
        { value: '', type: 'Input', placeholder: 'Search', onChange: () => {} },
        { value: '', type: 'Input', placeholder: 'Search', onChange: () => {} },
        { value: '', type: 'Input', placeholder: 'Search', onChange: () => {} },
        {},
        {},
      ],
    },
  ],
  rows: [
    {
      cells: [
        {
          type: 'Actions',
          actions: [
            {
              childComponent: (
                <div>
                  <FaChartLine />
                </div>
              ),
            },
          ],
        },
        { value: 'Brown rice' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          disabled: true,
          onChange: () => {},
        },
        { value: 361, type: 'Input', disabled: true, onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Vietnam', value: 'Vietnam' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Vietnam', value: 'Vietnam' },
          native: true,
          disabled: true,
          onChange: () => {},
        },
        { checked: true, type: 'CheckBox', disabled: true, onClick: () => {} },
        { value: 'Pricing', type: 'Link', disabled: true, onClick: () => {} },
      ],
      actions: [{ label: 'Delete', icon: <FaMinus />, onClick: () => {} }],
    },
    {
      cells: [
        {},
        { value: 'Buckwheat' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          clearable: true,
          onChange: () => {},
        },
        { value: 358, type: 'Input', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Poland', value: 'Poland' },
            {
              label: 'United Kingdom of Great Britain and Northern Ireland',
              value: 'United Kingdom of Great Britain and Northern Ireland',
            },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Poland', value: 'Poland' },
          autoLayerWidth: true,
          onChange: () => {},
        },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
      onRowClick: () => console.log('onRowClick event'),
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { type: 'Icon', icon: <FaExclamationTriangle /> },
        { value: 'Couscous' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 368, type: 'Input', error: 'Too big', onChange: () => {} },
        { value: 'France' },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        {},
        { value: 'Oatmeal' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 374, type: 'Input', onChange: () => {} },
        { value: 'Russia' },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { type: 'Icon', icon: <FaExclamationTriangle /> },
        { value: 'Pasta' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        {
          type: 'Slider',
          value: 35,
          min: 0,
          max: 100,
          step: 1,
          onChange: () => {},
          showTooltip: true,
        },
        { value: 'Italy' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
      actions: [
        { label: 'Health', icon: <FaHeartbeat />, onClick: () => {} },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        {},
        { value: 'Polenta' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 370, type: 'Input', onChange: () => {} },
        { value: 'Italy' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        {},
        {
          value: 'Potatoes',
          style: {
            backgroundColor: 'lemonchiffon',
            border: '1px solid moccasin',
          },
        },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 85, type: 'Input', onChange: () => {} },
        { value: 'China' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        {},
        { value: 'White rice' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 355, type: 'Input', onChange: () => {} },
        { value: 'India' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
  ],
  footer: {
    pagination: {
      rowCount: 500,
      selectedPage: 10,
      rowsPerPage: {
        onChange: () => {},
        options: [
          { label: '10 / page', value: 10 },
          { label: '20 / page', value: 20 },
          { label: '50 / page', value: 50 },
        ],
        value: 20,
      },
      onSelectPage: () => {},
      small: true,
    },
    actions: [{ label: 'Share', icon: <FaShareSquare />, onClick: () => {} }],
  },
};
