export const withDisabledCells = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight' },
        { value: 'Energy' },
        { value: 'Origin' },
        { value: 'Order' },
        {},
      ],
    },
    {
      cells: [
        {},
        { value: 'kg' },
        {
          type: 'Select',
          options: [
            { label: 'kcal / 100g', value: 'kcal / 100g' },
            { label: 'kJ / 100g', value: 'kJ / 100g' },
          ],
          value: { label: 'kcal / 100g', value: 'kcal / 100g' },
          native: true,
          onChange: () => {},
        },
        {},
        {},
        {},
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 361, type: 'Input', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Vietnam', value: 'Vietnam' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Vietnam', value: 'Vietnam' },
          native: true,
          onChange: () => {},
        },
        { checked: true, type: 'CheckBox', disabled: true, onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
          disabled: true,
        },
        { value: 358, type: 'Input', disabled: true, onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Poland', value: 'Poland' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Poland', value: 'Poland' },
          disabled: true,
          onChange: () => {},
        },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 368, type: 'Input', onChange: () => {} },
        { value: 'France' },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 374, type: 'Input', onChange: () => {} },
        { value: 'Russia' },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 359, type: 'Input', onChange: () => {} },
        { value: 'Italy' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 370, type: 'Input', onChange: () => {} },
        { value: 'Italy' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 85, type: 'Input', onChange: () => {} },
        { value: 'China' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        {
          type: 'Select',
          options: [
            { label: 50, value: 50 },
            { label: 100, value: 100 },
          ],
          value: { label: 50, value: 50 },
          native: true,
          onChange: () => {},
        },
        { value: 355, type: 'Input', onChange: () => {} },
        { value: 'India' },
        { type: 'CheckBox', onClick: () => {} },
        { value: 'Pricing', type: 'Link', onClick: () => {} },
      ],
    },
  ],
};
