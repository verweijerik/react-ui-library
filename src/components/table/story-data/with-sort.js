export const withSort = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name' },
        {
          value: 'Weight (kg)',
          hasSort: true,
          sort: 'up',
          sortPriority: 1,
          onSort: () => {},
        },
        {
          value: 'Energy (kcal / 100g)',
          hasSort: true,
          sort: 'down',
          sortPriority: 2,
          onSort: () => {},
        },
        {
          value: 'Origin',
          hasSort: true,
          sort: '',
          sortPriority: 1,
          onSort: () => {},
        },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
  ],
};
