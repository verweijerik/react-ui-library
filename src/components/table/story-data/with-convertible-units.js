export const withConvertibleUnits = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight' },
        { value: 'Energy' },
        { value: 'Origin' },
      ],
    },
    {
      cells: [
        {},
        {
          options: [
            { label: 'kg', value: 'kg' },
            { label: 'tonnes', value: 'tonnes' },
          ],
          type: 'Select',
          native: true,
          value: { label: 'kg', value: 'kg' },
          onChange: () => {},
        },
        {
          options: [
            { label: 'kcal / 100g', value: 'kcal / 100g' },
            { label: 'kJ / 100g', value: 'kJ / 100g' },
          ],
          type: 'Select',
          native: true,
          value: { label: 'kcal / 100g', value: 'kcal / 100g' },
          onChange: () => {},
        },
        {},
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
  ],
};
