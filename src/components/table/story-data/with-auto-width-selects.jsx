import React from 'react';

const options = [
  'Lorem ipsum',
  'dolor sit amet',
  'consectetur adipiscing elit, sed do eiusmod tempor',
  'incididunt ut labore',
];

export const withAutoWidthSelects = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Native' },
        { value: 'Custom' },
        { value: 'Enabled' },
        { value: 'Details' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Lorem' },
        {
          type: 'Select',
          native: true,
          options,
          value: options[0],
          onChange: () => {},
        },
        {
          type: 'Select',
          options,
          value: options[0],
          onChange: () => {},
          width: 'auto',
        },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Details', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Lorem' },
        {
          type: 'Select',
          native: true,
          options,
          value: options[0],
          onChange: () => {},
        },
        {
          type: 'Select',
          options,
          value: options[0],
          onChange: () => {},
          width: 'auto',
        },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Details', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Lorem' },
        {
          type: 'Select',
          native: true,
          options,
          value: options[0],
          onChange: () => {},
        },
        {
          type: 'Select',
          options,
          value: options[0],
          onChange: () => {},
          width: 'auto',
        },
        { checked: true, type: 'CheckBox', onClick: () => {} },
        { value: 'Details', type: 'Link', onClick: () => {} },
      ],
    },
  ],
};
