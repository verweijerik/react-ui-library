import React from 'react';
import { FaPlus, FaMinus, FaArrowUp, FaArrowDown } from 'react-icons/fa';

export const withSubActions = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight (kg)' },
        { value: 'Energy (kcal / 100g)' },
        { value: 'Origin' },
      ],
      actions: [
        {
          label: 'Add',
          icon: <FaPlus />,
          onClick: () => {},
        },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
      actions: [
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
        { label: 'Delete', icon: <FaMinus />, onClick: () => {} },
      ],
    },
  ],
};
