import React from 'react';

const subComponent = () => {
  return (
    <ul>
      <li>Carrots</li>
      <li>Chocolate</li>
      <li>Chorizo</li>
    </ul>
  );
};

const jsx = (
  <div>
    Some <span style={{ fontWeight: 'bold' }}>bold content</span>
  </div>
);

export const withJsxInCells = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [{ value: 'Name' }, { value: 'Content' }],
    },
  ],
  rows: [
    {
      cells: [{ value: 'Sub-component' }, { value: subComponent() }],
    },
    {
      cells: [{ value: 'JSX' }, { value: jsx }],
    },
  ],
};
