export const withFluidWidthEditable = {
  bordered: false,
  headers: [
    {
      cells: [{ value: 'Name' }, { value: 'Origin' }],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice', type: 'Input', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Vietnam', value: 'Vietnam' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Vietnam', value: 'Vietnam' },
          native: true,
          onChange: () => {},
        },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat', type: 'Input', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'Poland', value: 'Poland' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'Poland', value: 'Poland' },
          native: true,
          onChange: () => {},
        },
      ],
    },
    {
      cells: [
        { value: 'Couscous', type: 'Input', onChange: () => {} },
        {
          type: 'Select',
          options: [
            { label: 'France', value: 'France' },
            { label: 'Other', value: 'Other' },
          ],
          value: { label: 'France', value: 'France' },
          native: true,
          onChange: () => {},
        },
      ],
    },
  ],
};
