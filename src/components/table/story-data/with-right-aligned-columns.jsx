import React from 'react';
import { FaDownload, FaExclamationTriangle } from 'react-icons/fa';

const selectOptions = [
  { label: 'Aardvarks', value: 'termites' },
  { label: 'Kangaroos', value: 'grass' },
  { label: 'Monkeys', value: 'bananas', selected: true },
  { label: 'Possums', value: 'slugs' },
];

export const withRightAlignedColumns = {
  fixedWidth: '700px',
  columnAlignment: ['left', 'right', 'right', 'left', 'right'],
  columnWidths: ['auto', 'auto', 'auto', 'auto', '150px'],
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight' },
        { value: 'Energy' },
        { value: 'Origin' },
        {},
      ],
    },
    {
      cells: [
        {},
        {
          options: [
            { label: 'kg', value: 'kg' },
            { label: 'tonnes', value: 'tonnes' },
          ],
          type: 'Select',
          native: true,
          value: { label: 'kg', value: 'kg' },
          onChange: () => {},
        },
        {
          options: [
            { label: 'kcal / 100g', value: 'kcal / 100g' },
            { label: 'kJ / 100g', value: 'kJ / 100g' },
          ],
          type: 'Select',
          native: true,
          value: { label: 'kcal / 100g', value: 'kcal / 100g' },
          onChange: () => {},
        },
        {},
        {},
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
        { type: 'CheckBox', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
        { type: 'Input', value: 'Example', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
        { value: 'Static' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
        {
          type: 'Select',
          options: selectOptions,
          value: { label: 'Aardvarks', value: 'termites' },
          onChange: () => {},
        },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
        {
          type: 'Select',
          options: selectOptions,
          name: 'default_combobox',
          onChange: () => {},
        },
      ],
    },
    {
      cells: [
        { value: 'Pizza' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
        {
          type: 'Select',
          options: selectOptions,
          native: true,
          onChange: () => {},
        },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
        { type: 'Icon', icon: <FaExclamationTriangle /> },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
        { value: 'Link', type: 'Link', onClick: () => {} },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
        {
          type: 'Actions',
          actions: [
            {
              label: 'Download',
              icon: <FaDownload />,
              onClick: () => {},
            },
          ],
        },
      ],
    },
  ],
};
