export const withEditableCells = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight (kg)' },
        { value: 'Energy (kcal / 100g)' },
        { value: 'Origin' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice', type: 'Input', onChange: () => {} },
        { value: 100, type: 'Input', onChange: () => {} },
        { value: 361, type: 'Input', onChange: () => {} },
        { value: 'Vietnam', type: 'Input', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat', type: 'Input', onChange: () => {} },
        { value: 50, type: 'Input', onChange: () => {} },
        { value: 358, type: 'Input', onChange: () => {} },
        { value: 'Poland', type: 'Input', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Couscous', type: 'Input', onChange: () => {} },
        { value: 10, type: 'Input', onChange: () => {} },
        { value: 368, type: 'Input', onChange: () => {} },
        { value: 'France', type: 'Input', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal', type: 'Input', onChange: () => {} },
        { value: 650, type: 'Input', onChange: () => {} },
        { value: 374, type: 'Input', onChange: () => {} },
        { value: 'Russia', type: 'Input', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Pasta', type: 'Input', onChange: () => {} },
        { value: 120, type: 'Input', onChange: () => {} },
        { value: 359, type: 'Input', onChange: () => {} },
        { value: 'Italy', type: 'Input', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Polenta', type: 'Input', onChange: () => {} },
        { value: 560, type: 'Input', onChange: () => {} },
        { value: 370, type: 'Input', onChange: () => {} },
        { value: 'Italy', type: 'Input', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'Potatoes', type: 'Input', onChange: () => {} },
        { value: 25, type: 'Input', onChange: () => {} },
        { value: 85, type: 'Input', onChange: () => {} },
        { value: 'China', type: 'Input', onChange: () => {} },
      ],
    },
    {
      cells: [
        { value: 'White rice', type: 'Input', onChange: () => {} },
        { value: 400, type: 'Input', onChange: () => {} },
        { value: 355, type: 'Input', onChange: () => {} },
        { value: 'India', type: 'Input', onChange: () => {} },
      ],
    },
  ],
};
