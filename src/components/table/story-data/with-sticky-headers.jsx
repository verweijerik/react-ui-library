import React from 'react';
import { FaPlus, FaCog } from 'react-icons/fa';

export const withStickyHeaders = {
  fixedWidth: '700px',
  name: 'Food',
  stickyHeaders: 'true',
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight' },
        { value: ' Energy' },
        { value: 'Origin' },
      ],
      actions: [{ icon: 'plus', label: 'Add', onClick: () => {} }],
    },
    {
      cells: [{}, { value: 'kg' }, { value: 'kcal / 100g' }, {}],
      actions: [],
    },
  ],
  rows: [
    {
      cells: [
        { type: 'Input', value: 'Brown rice' },
        { type: 'Input', value: 100 },
        { type: 'Input', value: 361 },
        { type: 'Input', value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { type: 'Input', value: 'Buckwheat' },
        { type: 'Input', value: 50 },
        { type: 'Input', value: 358 },
        { type: 'Input', value: 'Poland' },
      ],
    },
    {
      cells: [
        { type: 'Input', value: 'Couscous' },
        { type: 'Input', value: 10 },
        { type: 'Input', value: 368 },
        { type: 'Input', value: 'France' },
      ],
    },
    {
      cells: [
        { type: 'Input', value: 'Oatmeal' },
        { type: 'Input', value: 650 },
        { type: 'Input', value: 374 },
        { type: 'Input', value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
    {
      cells: [
        { value: 'Oatmeal' },
        { value: 650 },
        { value: 374 },
        { value: 'Russia' },
      ],
    },
    {
      cells: [
        { value: 'Pasta' },
        { value: 120 },
        { value: 359 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Polenta' },
        { value: 560 },
        { value: 370 },
        { value: 'Italy' },
      ],
    },
    {
      cells: [
        { value: 'Potatoes' },
        { value: 25 },
        { value: 85 },
        { value: 'China' },
      ],
    },
    {
      cells: [
        { value: 'White rice' },
        { value: 400 },
        { value: 355 },
        { value: 'India' },
      ],
    },
  ],
};
