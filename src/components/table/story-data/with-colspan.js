export const withColSpan = {
  fixedWidth: '700px',
  headers: [
    {
      cells: [{ value: 'Datum', colSpan: 2 }, { value: 'Type' }],
    },
    {
      cells: [{ value: 'x' }, { value: 'y' }, {}],
    },
  ],
  rows: [
    {
      cells: [{ value: 1 }, { value: 1 }, { value: 'Initial' }],
    },
    {
      cells: [{ value: 2 }, { value: 2 }, { value: 'Subsequent' }],
    },
    {
      cells: [{ value: 3 }, { value: 4 }, { value: 'Other' }],
    },
    {
      cells: [{ value: 'Total', colSpan: 3 }],
    },
  ],
};
