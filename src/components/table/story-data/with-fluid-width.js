export const withFluidWidth = {
  bordered: false,
  headers: [
    {
      cells: [{ value: 'Name' }, { value: 'Origin' }],
    },
  ],
  rows: [
    {
      cells: [{ value: 'Brown rice' }, { value: 'Vietnam' }],
    },
    {
      cells: [{ value: 'Buckwheat' }, { value: 'Poland' }],
    },
    {
      cells: [{ value: 'Couscous' }, { value: 'France' }],
    },
  ],
};
