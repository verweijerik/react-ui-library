import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Button, Field, Input, Table } from '../../..';

const style = {
  height: '500px',
  width: '200px',
};

/*
  Mock table data store (real apps should use Redux or similar)
*/

const headings = ['Section', 'Width', 'Height'];
let units = ['', 'm', 'm'];
let data = [...Array(10).keys()].map((c, i) => [i, i * 2, i * 2]);

const PopoverTable = () => {
  const CellInputForm = ({ close, value }) => (
    <>
      <Field label="Input">
        <Input value={value} width="100%" />
      </Field>
      <Button colored label="Save" onClick={() => {}} />
      <Button label="Cancel" onClick={close} />
    </>
  );

  const table = {
    fixedWidth: '700px',
    headers: [
      {
        cells: headings.map((h) => ({ value: h })),
      },
      {
        cells: [
          {},
          {
            menu: [
              { label: 'm', value: 'm' },
              { label: 'ft', value: 'ft' },
            ],
            value: units[1],
            type: 'Static',
          },
          {
            menu: [
              { label: 'm', value: 'm' },
              { label: 'ft', value: 'ft' },
            ],
            value: units[2],
            type: 'Static',
          },
        ],
      },
    ],
    rows: data.map((row, i) => {
      return {
        cells: [...Array(3).keys()].map((c, i) => ({
          //https://stackoverflow.com/a/33352604
          value: row[i],
          type: 'Popover',
          content: <CellInputForm value={row[i]} />,
        })),
      };
    }),
  };

  return <Table table={table} />;
};

storiesOf('Basic/Table', module).add('with popover', () => (
  <Container style={style}>
    <PopoverTable />
  </Container>
));
