import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Table } from './table.jsx';
import { Card, Spacer, Heading } from '../../..';
import {
  basic,
  withEditableCells,
  withTitle,
  withUnits,
  withConvertibleUnits,
  withInputs,
  withErrors,
  withDisabledCells,
  withCustomCellStyles,
  withFilter,
  withSort,
  withIconCells,
  withActions,
  withSubActions,
  withJsxInCells,
  withFooter,
  withPagination,
  withSmallPagination,
  withPaginationHidden,
  withChildComponents,
  withColSpan,
  withKitchenSink,
  withRightAlignedColumns,
  withWarnings,
  withStickyHeaders,
  withFluidWidth,
  withAutoWidthSelects,
  withSpecifiedColumnWidths,
  withTooltipStaticCells,
} from './story-data';

const style = {
  height: '500px',
  width: '500px',
};

storiesOf('Basic/Table', module)
  .add('basic', () => (
    <Container style={style}>
      <Table table={basic} />
    </Container>
  ))
  .add('with colspan', () => (
    <Container style={style}>
      <Table table={withColSpan} />
    </Container>
  ))
  .add('with editable cells', () => (
    <Container style={style}>
      <Table table={withEditableCells} />
    </Container>
  ))
  .add('with title', () => (
    <Container style={style}>
      <Table table={withTitle} />
    </Container>
  ))
  .add('with sticky headers', () => (
    <Container style={style}>
      <Table table={withStickyHeaders} />
    </Container>
  ))
  .add('with units', () => (
    <Container style={style}>
      <Table table={withUnits} />
    </Container>
  ))
  .add('with convertible units', () => (
    <Container style={style}>
      <Table table={withConvertibleUnits} />
    </Container>
  ))
  .add('with right-aligned columns', () => (
    <Container style={style}>
      <Table table={withRightAlignedColumns} />
    </Container>
  ))
  .add('with inputs', () => (
    <Container style={style}>
      <Table table={withInputs} />
    </Container>
  ))
  .add('with auto-width selects', () => (
    <Container
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <Table table={withAutoWidthSelects} />
    </Container>
  ))
  .add('with errors', () => (
    <Container style={style}>
      <Table table={withErrors} />
    </Container>
  ))
  .add('with warnings', () => (
    <Container style={style}>
      <Table table={withWarnings} />
    </Container>
  ))
  .add('with disabled cells', () => (
    <Container style={style}>
      <Table table={withDisabledCells} />
    </Container>
  ))
  .add('with custom cell styles', () => (
    <Container style={style}>
      <Table table={withCustomCellStyles} />
    </Container>
  ))
  .add('with filter', () => (
    <Container style={style}>
      <Table table={withFilter} />
    </Container>
  ))
  .add('with sort', () => (
    <Container style={style}>
      <Table table={withSort} />
    </Container>
  ))
  .add('with icon cells', () => (
    <Container style={style}>
      <Table table={withIconCells} />
    </Container>
  ))
  .add('with actions', () => (
    <Container style={style}>
      <Table table={withActions} />
    </Container>
  ))
  .add('with sub actions', () => (
    <Container style={style}>
      <Table table={withSubActions} />
    </Container>
  ))
  .add('with child components', () => (
    <Container style={style}>
      <Table table={withChildComponents} />
    </Container>
  ))
  .add('with JSX in cells', () => (
    <Container style={style}>
      <Table table={withJsxInCells} />
    </Container>
  ))
  .add('with footer', () => (
    <Container style={style}>
      <Table table={withFooter} />
    </Container>
  ))
  .add('with pagination', () => (
    <Container style={style}>
      <Heading top>Pagination</Heading>
      <Table table={withPagination} />
      <Spacer />
      <Heading top>Small pagination</Heading>
      <Table table={withSmallPagination} />
      <Spacer />
      <Heading top>Test: hidden if fewer rows than minimum page size</Heading>
      <Table table={withPaginationHidden} />
      <Spacer />
    </Container>
  ))
  .add('in card', () => (
    <Container style={style}>
      <Card heading="Card heading" padding={0}>
        <Table table={withFluidWidth} />
      </Card>
    </Container>
  ))
  .add('with specified column widths', () => {
    const message = `When setting explicit column widths, use 1% to shrink a 
    column to fit its content, and auto to use remaining space`;
    return (
      <Container warningMessage={message}>
        <div
          style={{
            height: '100%',
            width: '100%',
          }}
        >
          <Table table={withSpecifiedColumnWidths} />
        </div>
        <div
          style={{
            height: '500px',
            width: '600px',
            overflow: 'scroll',
            marginTop: '20px',
            display: 'inline-block',
          }}
        >
          <Table table={withSpecifiedColumnWidths} />
        </div>
      </Container>
    );
  })
  .add('with kitchen sink', () => (
    <Container style={style}>
      <Table table={withKitchenSink} />
    </Container>
  ))
  .add('static cells with tooltips', () => (
    <Container style={style}>
      <Table table={withTooltipStaticCells} />
    </Container>
  ));
