import React from 'react';
import cx from 'classnames';
import styles from './row.module.less';

export const ExpandedContentRow = ({ colSpan, children, flush }) => {
  return (
    <tr>
      <td
        colSpan={colSpan}
        className={cx(styles.expandableRow, flush ? styles.flush : '')}
      >
        {children}
      </td>
    </tr>
  );
};
