import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Cell, cellShape } from '../cell/cell';
import { ExpandedContentRow } from './expanded-content-row';
import { actionsShape } from '../../actions/actions';
import styles from './row.module.less';

export const Row = (props) => {
  const {
    row,
    rowIndex,
    isHeader,
    columnCount,
    columnAlignment,
    columnWidths,
    colSpan,
    hasRowActions,
    rowsMetaData,
    setRowsMetaData,
  } = props;

  const targetRef = useRef();
  if (rowsMetaData && setRowsMetaData) {
    useEffect(() => {
      if (targetRef.current) {
        setRowsMetaData((data) => {
          const lastElement = data[data.length - 1];
          return [
            ...data,
            {
              width: targetRef.current.offsetWidth,
              height: targetRef.current.offsetHeight,
              isHeader: !!isHeader,
              stickyOffset: lastElement
                ? lastElement.height + lastElement.stickyOffset
                : 0,
            },
          ];
        });
      }
    }, []);
  }

  const { onRowClick, expandedContent } = row;
  const cells = row.cells.map((c, i) => {
    const key = `${isHeader ? 0 : 1}_${rowIndex}_${i}`;
    return (
      <Cell
        cell={c}
        key={key}
        isHeader={isHeader}
        columnAlignment={columnAlignment[i]}
        width={columnWidths ? columnWidths[i] : undefined}
        rowMetaData={rowsMetaData ? rowsMetaData[rowIndex] : undefined}
      />
    );
  });

  const rowActions = hasRowActions && (
    <Cell
      cell={{
        type: 'Actions',
        actions: row.actions,
      }}
      key={columnCount}
      isHeader={isHeader}
      rowMetaData={rowsMetaData ? rowsMetaData[rowIndex] : undefined}
    />
  );
  return (
    <>
      <tr
        ref={targetRef}
        key={rowIndex}
        onClick={onRowClick}
        className={onRowClick ? styles.clickableRow : null}
      >
        {cells}
        {rowActions}
      </tr>
      {expandedContent && expandedContent.content ? (
        <ExpandedContentRow
          key={`${rowIndex}_expanded_content`}
          colSpan={colSpan}
          flush={expandedContent.flush === true}
        >
          {expandedContent.content}
        </ExpandedContentRow>
      ) : null}
    </>
  );
};

export const rowShape = PropTypes.shape({
  cells: PropTypes.arrayOf(cellShape),
  onRowClick: PropTypes.func,
  actions: actionsShape,
  expandedContent: PropTypes.shape({
    content: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
    flush: PropTypes.bool,
  }),
});

Row.defaultProps = {
  isHeader: false,
  columnAlignment: [],
};

Row.propTypes = {
  row: rowShape.isRequired,
  rowIndex: PropTypes.number.isRequired,
  isHeader: PropTypes.bool,
  columnCount: PropTypes.number.isRequired,
  colSpan: PropTypes.number.isRequired,
  columnAlignment: PropTypes.arrayOf(PropTypes.oneOf(['left', 'right'])),
  hasRowActions: PropTypes.bool.isRequired,
};
