import React, { useEffect, useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaPlus, FaMinus } from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Table } from './table';

const style = {
  height: '500px',
  width: '200px',
};

/*
  Mock table data store (real apps should use Redux or similar)
*/

const headings = ['Section', 'Width', 'Height'];
let units = ['', 'm', 'm'];
let data = [...Array(1000).keys()].map((c, i) => [i, i * 2, i * 2]);
let keyedData = [...Array(1000).keys()].map((c, i) => ({
  Section: i,
  Width: i * 2,
  Height: i * 2,
}));

/*
  Mock table data actions (real apps should use Redux or similar)
*/

const convertUnit = (fromUnit, toUnit, value) => {
  return fromUnit === 'm' && toUnit === 'ft'
    ? value * 3.28084
    : fromUnit === 'ft' && toUnit === 'm'
    ? value / 3.28084
    : value;
};

const onChangeValue = (evt, rowIndex, cellIndex, render) => {
  data[rowIndex][cellIndex] = evt.target.value;
  render();
};
const onAddRow = (render) => {
  data = data.concat([['', '', '']]);
  render();
};

const onDeleteRow = (rowIndex, render) => {
  data = data.filter((r, i) => i !== rowIndex);
  render();
};

const onChangeUnit = (evt, columnIndex, render) => {
  const fromUnit = units[columnIndex];
  const toUnit = evt.target.value;
  units = units.map((u, i) => (i === columnIndex ? toUnit : u));
  data = data.map((r) =>
    r.map((c, i) => (i === columnIndex ? convertUnit(fromUnit, toUnit, c) : c)),
  );
  render();
};

/*
  Container component manages state and configuration of table
*/

const ManagedTable = () => {
  const rowsPerPageOptions = [
    { label: '10 / page', value: 10 },
    { label: '20 / page', value: 20 },
    { label: '50 / page', value: 50 },
  ];
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [selectedPage, setSelectedPage] = useState(1);

  //hack to make Storybook re-render after event handlers alter data
  //only using this because example does not manage data with Redux / props
  const [toggle, forceRender] = useState(false);
  const render = () => {
    forceRender(!toggle);
  };

  const firstVisibleRow = (selectedPage - 1) * rowsPerPage;
  const lastVisibleRow = firstVisibleRow + rowsPerPage;

  const table = {
    fixedWidth: '700px',
    headers: [
      {
        cells: headings.map((h) => ({ value: h })),
        actions: [
          {
            label: 'Add',
            icon: <FaPlus />,
            onClick: () => onAddRow(render),
          },
        ],
      },
      {
        cells: [
          {},
          {
            options: [
              { label: 'm', value: 'm' },
              { label: 'ft', value: 'ft' },
            ],
            value: units[1],
            type: 'Select',
            native: true,
            onChange: (evt) => onChangeUnit(evt, 1, render),
          },
          {
            options: [
              { label: 'm', value: 'm' },
              { label: 'ft', value: 'ft' },
            ],
            value: units[2],
            type: 'Select',
            native: true,
            onChange: (evt) => onChangeUnit(evt, 2, render),
          },
        ],
      },
    ],
    rows: data.slice(firstVisibleRow, lastVisibleRow).map((row, i) => {
      const rowIndex = firstVisibleRow + i;
      return {
        cells: [...Array(3).keys()].map((c, i) => ({
          //https://stackoverflow.com/a/33352604
          value: row[i],
          type: 'Input',
          onChange: (evt) => onChangeValue(evt, rowIndex, i, render),
        })),
        actions: [
          {
            label: 'Delete',
            icon: <FaMinus />,
            onClick: () => onDeleteRow(rowIndex, render),
          },
        ],
      };
    }),
    footer: {
      pagination: {
        rowCount: data.length,
        selectedPage,
        rowsPerPage: {
          onChange: (evt) => {
            const { value } = evt.target;
            setRowsPerPage(parseInt(value));
            setSelectedPage(1);
          },
          options: rowsPerPageOptions,
          value: rowsPerPage,
        },
        onSelectPage: (evt) => setSelectedPage(evt),
        small: true,
      },
    },
  };

  return <Table table={table} />;
};

const TableWithSortAndFilter = () => {
  const rowsPerPageOptions = [
    { label: '10 / page', value: 10 },
    { label: '20 / page', value: 20 },
    { label: '50 / page', value: 50 },
  ];
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [selectedPage, setSelectedPage] = useState(1);
  const [filters, setFilters] = useState({});
  const [sorts, setSorts] = useState({});

  useEffect(() => {
    setSelectedPage(1);
  }, [filters, sorts]);

  const firstVisibleRow = (selectedPage - 1) * rowsPerPage;
  const lastVisibleRow = firstVisibleRow + rowsPerPage;

  const filterAndSortDataRows = (dataRows, filters, sorts) =>
    dataRows
      .filter((row) =>
        Object.keys(filters).every((key) => {
          return filters[key] === ''
            ? true
            : row[key].toString().includes(filters[key]);
        }),
      )
      .sort((a, b) =>
        Object.entries(sorts)
          .map(([key, value]) => {
            switch (value) {
              case 'up': {
                return a[key] - b[key];
              }
              case 'down': {
                return b[key] - a[key];
              }
              default:
                return 0;
            }
          })
          .reduce((a, acc) => a || acc, 0),
      );

  const dataHeaders = (dataRowsKeys, filters, setFilters, sorts, setSorts) => {
    const dataSortCells = dataRowsKeys.map((key) => {
      const sort = Object.keys(sorts).includes(key) ? sorts[key] : '';
      const prettifyHeaderValue = `${key[0].toUpperCase()}${key.slice(1)}`;
      return {
        key,
        value: prettifyHeaderValue,
        hasSort: true,
        sort,
        onSort: () => {
          const newSort = sort === '' ? 'up' : sort === 'up' ? 'down' : '';
          setSorts({ ...sorts, [key]: newSort });
        },
      };
    });

    const dataFilterCells = dataRowsKeys.map((key) => {
      const filterValue = Object.keys(filters).includes(key)
        ? filters[key]
        : '';
      return {
        key,
        value: filterValue,
        type: 'Input',
        placeholder: 'Filter',
        onChange: (ev) => setFilters({ ...filters, [key]: ev.target.value }),
      };
    });

    return { dataSortCells, dataFilterCells };
  };

  const { dataSortCells, dataFilterCells } = dataHeaders(
    headings,
    filters,
    setFilters,
    sorts,
    setSorts,
  );

  const filteredAndSortedData = filterAndSortDataRows(
    keyedData,
    filters,
    sorts,
  );

  const dataRows = [
    ...filteredAndSortedData
      .slice(firstVisibleRow, lastVisibleRow)
      .map((dataRow) => {
        const rowsCells = Object.entries(dataRow).map(([key, value]) => ({
          key,
          value,
          type: 'Input',
          disabled: true,
        }));
        return {
          cells: rowsCells,
        };
      }),
  ];

  const table = {
    fixedWidth: '700px',
    headers: [
      {
        cells: dataSortCells,
      },
      {
        cells: dataFilterCells,
      },
    ],
    rows: dataRows,
    footer: {
      pagination: {
        rowCount: filteredAndSortedData.length,
        selectedPage,
        rowsPerPage: {
          onChange: (evt) => {
            const { value } = evt.target;
            setRowsPerPage(parseInt(value));
            setSelectedPage(1);
          },
          options: rowsPerPageOptions,
          value: rowsPerPage,
        },
        onSelectPage: (evt) => setSelectedPage(evt),
        small: true,
      },
    },
  };

  return <Table table={table} />;
};

storiesOf('Basic/Table', module).add('managed', () => (
  <Container style={style}>
    <ManagedTable />
  </Container>
));

storiesOf('Basic/Table', module).add('managed, with filter and sort', () => (
  <Container style={style}>
    <TableWithSortAndFilter />
  </Container>
));
