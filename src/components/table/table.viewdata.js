const getMaxCellCount = (rows) => {
  const count = Math.max(
    ...rows.reduce((acc, row) => {
      const count = row.cells
        .map((c) => (c.colSpan ? c.colSpan : 1))
        .reduce((a, b) => a + b, 0);
      return acc.concat(count);
    }, []),
  );
  return !isNaN(count) ? count : 0;
};

export const getColumnCount = (rows, headers) => {
  const cellCount = [getMaxCellCount(headers), getMaxCellCount(rows)];
  return Math.max(...cellCount);
};

const hasActions = (rows) =>
  rows.reduce(
    (acc, row) => (row.actions && row.actions.length > 0 ? 1 : acc),
    0,
  );

export const hasRowActions = (rows, headers) => {
  return hasActions(headers) > 0 || hasActions(rows) > 0;
};
