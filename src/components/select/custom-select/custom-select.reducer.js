export const initialTriggerFocus = {
  options: ['none', 'clearAll'],
  currentIndex: 0,
  currentOption: false,
  clearAll: false,
};

const initialLayerFocusIndex = (firstSelectedOptionIndex) =>
  firstSelectedOptionIndex !== null ? firstSelectedOptionIndex + 1 : 0;

export const initialLayerFocus = (options, firstSelectedOptionIndex) => {
  const layerFocusOptions = options && [
    'none',
    ...(options &&
      options
        .map((o, i) => (o.type !== 'Heading' ? i : null))
        .filter((o) => o !== null)),
  ];
  const layerFocusIndex = initialLayerFocusIndex(firstSelectedOptionIndex);
  return {
    options: layerFocusOptions,
    currentIndex: layerFocusIndex,
    current: layerFocusOptions[layerFocusIndex],
  };
};

export const nextLayerFocus = (direction, layerFocus) => {
  const sign = direction === 'up' ? -1 : 1;
  const increment = layerFocus.currentIndex + sign;
  const nextIndex =
    (increment < 0 ? layerFocus.options.length - 1 : increment) %
    layerFocus.options.length;
  const next = layerFocus.options[nextIndex];
  return {
    ...layerFocus,
    currentIndex: nextIndex,
    current: next,
  };
};

const nextTriggerFocus = (direction, selectedOptions, triggerFocus) => {
  const isMulti = selectedOptions instanceof Array;
  const sign = direction === 'left' ? -1 : 1;
  const nextOptions = isMulti
    ? [
        'none',
        ...Array(selectedOptions ? selectedOptions.length : 0).keys(),
        'clearAll',
      ]
    : ['none', 'clearAll'];
  const increment = triggerFocus.currentIndex + sign;
  const nextIndex =
    (increment < 0 ? nextOptions.length - 1 : increment) % nextOptions.length;
  const next = nextOptions[nextIndex];
  const clearAll = next === 'clearAll';
  return {
    options: nextOptions,
    currentIndex: nextIndex,
    currentOption: next,
    clearAll,
  };
};

const blurTriggerFocus = () => initialTriggerFocus;

const blurLayerFocus = (layerFocus) => ({
  ...layerFocus,
  currentIndex: 0,
  current: 'none',
});

export const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_VISIBLE_OPTIONS': {
      return {
        ...state,
        visibleOptions: action.options,
      };
    }
    case 'RESET_LAYER_FOCUS': {
      return {
        ...state,
        layerFocus: initialLayerFocus(
          action.options,
          action.firstSelectedOptionIndex,
        ),
      };
    }
    case 'CLEAR_SEARCH': {
      return {
        ...state,
        visibleOptions: action.options,
        searchValue: '',
      };
    }
    case 'ON_CHANGE_SEARCH': {
      let visibleOptions = action.options.filter((o) =>
        String(o.label)
          .toLowerCase()
          .includes(String(action.value).toLowerCase()),
      );
      if (action.createAble && action.value !== '') {
        visibleOptions = visibleOptions.concat({
          label: `Create "${action.value}"`,
          value: action.value,
          createAble: true,
        });
      }
      return {
        ...state,
        searchValue: action.value,
        visibleOptions,
        layerFocus: blurLayerFocus(state.layerFocus),
      };
    }
    case 'FOCUS_TRIGGER_INPUTS': {
      const { direction, selectedOptions } = action;
      return {
        ...state,
        triggerFocus: nextTriggerFocus(
          direction,
          selectedOptions,
          state.triggerFocus,
        ),
        layerFocus: blurLayerFocus(state.layerFocus),
      };
    }
    case 'BLUR_TRIGGER_INPUTS': {
      return {
        ...state,
        triggerFocus: blurTriggerFocus(),
      };
    }
    case 'FOCUS_LAYER_OPTIONS': {
      const { nextLayerFocus } = action;
      return {
        ...state,
        layerFocus: nextLayerFocus,
        triggerFocus: blurTriggerFocus(),
      };
    }
    default:
      return state;
  }
};
