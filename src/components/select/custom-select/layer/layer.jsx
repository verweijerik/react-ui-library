import React, { useEffect } from 'react';
import cx from 'classnames';
import { FixedSizeList as List } from 'react-window';
import styles from '../custom-select.module.less';
import { Section } from './section';

export const Layer = ({
  listRef,
  isMulti,
  sections,
  selectedOptions,
  onSelectOption,
  focusedOptionIndex,
  width,
  firstSelectedOptionIndex,
}) => {
  useEffect(() => {
    if (firstSelectedOptionIndex !== null && listRef.current !== null) {
      listRef.current.scrollToItem(firstSelectedOptionIndex, 'start');
    }
  }, []);
  const optionHeight = 38;
  return (
    <div className={cx(styles.layer)}>
      {!sections.length ? (
        <ul>
          <li>
            <div className={styles.message} style={{ width }}>
              No matches
            </div>
          </li>
        </ul>
      ) : (
        <List
          ref={listRef}
          height={
            sections.length < 10
              ? optionHeight * sections.length
              : optionHeight * 10
          }
          itemCount={sections.length}
          itemSize={optionHeight}
          width={width}
        >
          {({ index, style }) => (
            <div style={style}>
              <Section
                section={sections[index]}
                selected={
                  isMulti &&
                  sections[index].type !== 'Heading' &&
                  selectedOptions
                    ? selectedOptions.some(
                        (v) => v && v.value === sections[index].value,
                      )
                    : selectedOptions
                    ? sections[index].value === selectedOptions.value
                    : false
                }
                focused={focusedOptionIndex === index}
                onSelectOption={(evt) => {
                  onSelectOption(evt, sections[index]);
                }}
              />
            </div>
          )}
        </List>
      )}
    </div>
  );
};
