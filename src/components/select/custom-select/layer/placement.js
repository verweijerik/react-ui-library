export const placement = {
  autoAdjust: true,
  possibleAnchors: ['BOTTOM_LEFT', 'TOP_LEFT'],
  anchor: 'BOTTOM_LEFT',
  preferX: 'RIGHT',
  preferY: 'BOTTOM',
};
