import React from 'react';
import { Heading } from './heading';
import { Option } from './option';

export const Section = ({ section, selected, focused, onSelectOption }) => {
  switch (section.type) {
    case 'Heading':
      return <Heading label={section.label} />;
    case 'Option':
    default:
      return (
        <Option
          label={section.label}
          icon={section.icon}
          disabled={section.disabled}
          selected={selected}
          focused={focused}
          onSelectOption={onSelectOption}
        />
      );
  }
};
