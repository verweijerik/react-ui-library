import React from 'react';
import styles from '../custom-select.module.less';

export const Heading = ({ label }) => {
  return (
    <div
      onClick={(evt) => evt.stopPropagation()} //don't close menu
      className={styles.heading}
    >
      {label}
    </div>
  );
};
