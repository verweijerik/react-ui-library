import React from 'react';
import cx from 'classnames';
import { FaCheck } from 'react-icons/fa';
import styles from '../custom-select.module.less';

export const Option = ({
  label,
  icon,
  disabled,
  selected,
  focused,
  onSelectOption,
}) => {
  return (
    <div
      className={cx(
        styles.option,
        disabled ? styles.disabled : '',
        selected ? styles.selected : '',
        focused ? styles.focused : '',
      )}
      onClick={!disabled ? onSelectOption : () => {}}
    >
      <div className={styles.optionContent}>
        <span className={styles.label} title={label}>
          {label}
          {icon && <span className={styles.icon}>{icon}</span>}
        </span>
      </div>
      {selected ? (
        <span className={styles.check}>
          <FaCheck />
        </span>
      ) : null}
    </div>
  );
};
