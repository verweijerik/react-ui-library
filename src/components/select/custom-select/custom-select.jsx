import { uniqWith, isEqual } from 'lodash';
import React, { useRef, useReducer, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useToggleLayer } from 'react-laag';
import ResizeObserver from 'resize-observer-polyfill';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import { useFocus } from '../../../hooks';
import { placement } from './layer/placement';
import { Layer } from './layer/layer';
import { Trigger } from './trigger/trigger';
import {
  initialLayerFocus,
  initialTriggerFocus,
  nextLayerFocus,
  reducer,
} from './custom-select.reducer';
import { getTextWidth } from '../../../helpers/text';
import styles from './custom-select.module.less';

export const CustomSelect = ({
  multi: isMulti,
  disabled,
  error,
  warning,
  tooltip,
  options,
  selectedOptions,
  onChange,
  onCreate,
  small,
  isInTable,
  tabIndex,
  width,
  autoLayerWidth,
  onFocus,
  onBlur,
  searchable,
  clearable,
  groupOrder,
  maxTooltipWidth,
  firstSelectedOptionIndex,
  placeholder,
  right,
}) => {
  const createAble = onCreate !== null;
  const listRef = useRef(null);
  const [triggerRef, setTriggerFocus] = useFocus();

  const [state, dispatch] = useReducer(
    reducer,
    { options, firstSelectedOptionIndex },
    ({ options }) => ({
      searchValue: '',
      visibleOptions: options,
      layerFocus: initialLayerFocus(options, firstSelectedOptionIndex), //also gets reset when layer opens (because options can change)
      triggerFocus: initialTriggerFocus,
    }),
  );

  //Update visible options upon external changes to the options prop
  const isFirstRun = useRef(true);
  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false;
    } else {
      dispatch({ type: 'SET_VISIBLE_OPTIONS', options });
    }
  }, [options]);

  const onSelectOption = (evt, option, close) => {
    if (!disabled) {
      if (close) {
        close();
      }
      if (option) {
        if (option.createAble) {
          onCreate(option.value);
        } else {
          if (!option.disabled) {
            const newSelectedOptions =
              selectedOptions instanceof Array
                ? uniqWith(selectedOptions.concat(option), isEqual) //de-duplicate
                : option;
            onChange(evt, newSelectedOptions);
          }
        }
      }
      dispatch({ type: 'CLEAR_SEARCH', options });
      setTriggerFocus();
    }
  };

  /*
    For auto-width custom selects, we measure the width of the options before
    rendering, using a temporary canvas element. Then we add some extra width for
    the input UI chrome/elements.
  */

  const isAutoWidth = width === 'auto';
  const calcAutoWidth = isAutoWidth || autoLayerWidth;
  const font = 'bold 14px sans-serif'; //bold font to match selected value width in dropdown
  const autoWidth = calcAutoWidth //text width in px of the longest option
    ? Math.ceil(
        options.reduce((acc, option) => {
          const optionWidth = getTextWidth(option.label, font);
          acc = optionWidth > acc ? optionWidth : acc;
          return acc;
        }, 0),
      )
    : null;
  const extraTriggerWidth = calcAutoWidth
    ? 14 * 2 + // option label padding
      24 + // tick or arrow icon
      (isMulti ? 11 : 0) + //selected multi option box
      (clearable ? 28 : 0) + // clearable icon
      12 //extra space for scrollbar on Chrome/Safari
    : 0;
  const triggerWidth = isAutoWidth
    ? `calc(${autoWidth}px + ${extraTriggerWidth}px)`
    : width;
  const extraLayerWidth = autoLayerWidth
    ? 14 * 2 + // option label padding
      24 + // tick
      12 //extra space for scrollbar on Chrome/Safari
    : 0;
  const layerWidth = autoLayerWidth
    ? `calc(${autoWidth}px + ${extraLayerWidth}px)`
    : '0px';

  const [element, toggleLayerProps] = useToggleLayer(
    ({ layerProps, isOpen, close, triggerRect }) =>
      isOpen && (
        <div
          {...layerProps} // eslint-disable-line react/jsx-props-no-spreading
          className={styles.layerContainer}
        >
          <Layer
            listRef={listRef}
            isMulti={isMulti}
            sections={state.visibleOptions}
            selectedOptions={selectedOptions}
            onSelectOption={(evt, option) => {
              onSelectOption(evt, option, close);
            }}
            triggerRect={triggerRect}
            width={`max(${layerWidth}, ${triggerRect.width}px)`}
            focusedOptionIndex={state.layerFocus.current}
            firstSelectedOptionIndex={firstSelectedOptionIndex}
          />
        </div>
      ),
    {
      fixed: true,
      placement,
      ResizeObserver,
      closeOnOutsideClick: true,
    },
  );

  const onClickTrigger = (evt) => {
    if (!disabled) {
      if (toggleLayerProps.isOpen) {
        toggleLayerProps.close();
        setTriggerFocus();
      } else {
        dispatch({
          type: 'RESET_LAYER_FOCUS',
          options,
          firstSelectedOptionIndex,
        });
        toggleLayerProps.openFromMouseEvent(evt);
      }
    }
  };

  //reset search filter when layer closes
  const isFirstRender = useRef(true);
  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
    } else {
      if (!toggleLayerProps.isOpen) {
        dispatch({ type: 'CLEAR_SEARCH', options });
      }
    }
  }, [toggleLayerProps.isOpen]);

  const onClickDeselectOption = (evt, options) => {
    const newSelectedOptions =
      selectedOptions instanceof Array
        ? selectedOptions.filter((option) => option.value !== options.value)
        : null;
    onChange(evt, newSelectedOptions);
  };

  const onClickClearAll = (evt) => {
    const newSelectedOptions =
      selectedOptions instanceof Array ? [] : { value: null };
    onChange(evt, newSelectedOptions);
  };

  const onChangeSearch = (value) =>
    dispatch({
      type: 'ON_CHANGE_SEARCH',
      options,
      value,
      createAble,
    });

  /*
     Keyboard actions
   */

  const clickTrigger = () => {
    if (triggerRef.current) {
      //would probably be better to use useToggleLayer.open() here
      //the ref click as workaround until this ticket is answered:
      //https://github.com/everweij/react-laag/issues/37
      triggerRef.current.click();
    }
    dispatch({ type: 'BLUR_TRIGGER_INPUTS' });
  };

  const scrollToItem = (index) => {
    if (listRef.current && Number.isInteger(index)) {
      listRef.current.scrollToItem(index);
    }
  };
  const focusNextLayerOption = (direction) => {
    const next = nextLayerFocus(direction, state.layerFocus);
    dispatch({ type: 'FOCUS_LAYER_OPTIONS', nextLayerFocus: next });
    scrollToItem(next.current);
  };
  const focusTriggerInputs = (direction) => {
    dispatch({
      type: 'FOCUS_TRIGGER_INPUTS',
      direction,
      selectedOptions,
    });
  };
  const closeLayer = () => {
    if (toggleLayerProps.isOpen) {
      toggleLayerProps.close();
      setTriggerFocus();
    }
  };
  const clearAll = (evt) => onClickClearAll(evt);
  const deselectIsFocused =
    selectedOptions && selectedOptions[state.triggerFocus.currentOption];
  const deselectOption = (evt) => {
    onClickDeselectOption(
      evt,
      selectedOptions[state.triggerFocus.currentOption],
    );
  };
  const selectOption = (evt) => {
    onSelectOption(
      evt,
      state.visibleOptions[state.layerFocus.current],
      toggleLayerProps.close,
    );
  };
  const clearAllIsFocused = state.triggerFocus.clearAll;

  const onKeyEvent = (key, evt) => {
    switch (key) {
      case 'up': {
        focusNextLayerOption(key);
        break;
      }
      case 'down': {
        if (toggleLayerProps.isOpen) {
          focusNextLayerOption(key);
        } else {
          clickTrigger();
        }
        break;
      }
      case 'left':
      case 'right': {
        focusTriggerInputs(key);
        break;
      }
      case 'enter': {
        if (clearAllIsFocused) {
          clearAll(evt);
        } else if (deselectIsFocused) {
          deselectOption(evt);
        } else {
          selectOption(evt);
        }
        break;
      }
      case 'esc': {
        closeLayer();
        break;
      }
      default:
    }
  };

  return (
    <KeyboardEventHandler
      handleKeys={['tab', 'up', 'down', 'left', 'right', 'enter', 'esc']}
      onKeyEvent={onKeyEvent}
    >
      <Trigger
        selectedOptions={selectedOptions}
        searchValue={state.searchValue}
        isOpen={toggleLayerProps.isOpen}
        focusedSelectedOptionIndex={state.triggerFocus.currentOption}
        clearAllIsFocused={clearAllIsFocused}
        width={triggerWidth}
        searchable={searchable}
        triggerRef={triggerRef}
        element={element}
        error={error}
        warning={warning}
        tooltip={tooltip}
        small={small}
        isInTable={isInTable}
        disabled={disabled}
        clearable={clearable}
        tabIndex={tabIndex}
        onClickTrigger={onClickTrigger}
        onChangeSearch={onChangeSearch}
        onClickDeselect={onClickDeselectOption}
        onClickClear={onClickClearAll}
        onFocus={onFocus}
        onBlur={onBlur}
        groupOrder={groupOrder}
        maxTooltipWidth={maxTooltipWidth}
        placeholder={placeholder}
        right={right}
      />
    </KeyboardEventHandler>
  );
};

export const selectOptionShape = PropTypes.oneOfType([
  PropTypes.shape({
    //option
    label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    icon: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  }),
  PropTypes.shape({
    //Heading
    label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    type: PropTypes.string,
  }),
]);

const selectedOptionShape = {
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

export const selectedOptionsShape = PropTypes.oneOfType([
  PropTypes.string, //single-select
  PropTypes.number, //number
  PropTypes.shape(selectedOptionShape), //single-select
  PropTypes.arrayOf(PropTypes.shape(selectedOptionShape)), //multi-select
  PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])), //multi-select
]);

CustomSelect.defaultProps = {
  multi: false,
  disabled: false,
  error: null,
  warning: null,
  tooltip: null,
  selectedOptions: null,
  onChange: () => {},
  onCreate: null,
  small: false,
  tabIndex: 0,
  width: null,
  autoLayerWidth: false,
  onFocus: () => {},
  onBlur: () => {},
  searchable: true,
  clearable: false,
  //private props (don't use)
  isInTable: false,
  groupOrder: null,
  firstSelectedOptionIndex: null,
  right: false,
};

export const CustomSelectShape = {
  multi: PropTypes.bool,
  disabled: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  warning: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  tooltip: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  options: PropTypes.arrayOf(selectOptionShape).isRequired,
  selectedOptions: selectedOptionsShape,
  onChange: PropTypes.func,
  onCreate: PropTypes.func,
  small: PropTypes.bool,
  tabIndex: PropTypes.number,
  width: PropTypes.string,
  autoLayerWidth: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  searchable: PropTypes.bool,
  clearable: PropTypes.bool,
  //private props (don't use)
  isInTable: PropTypes.bool,
  groupOrder: PropTypes.string,
  firstSelectedOptionIndex: PropTypes.number,
  right: PropTypes.bool,
};

CustomSelect.propTypes = CustomSelectShape;
