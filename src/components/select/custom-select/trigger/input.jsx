import React from 'react';
import * as PropTypes from 'prop-types';
import cx from 'classnames';
import { FaTimes } from 'react-icons/fa';
import styles from './trigger.module.less';

export const Input = ({
  selectedOptions,
  searchable,
  searchValue,
  inputRef,
  onChange,
  onClickDeselect,
  error,
  focusedSelectedOptionIndex,
  placeholder,
}) => {
  const isMulti = selectedOptions instanceof Array;
  const hasSelectedValues = isMulti
    ? selectedOptions.length > 0
    : selectedOptions !== null;
  const placeHolder = !searchValue && !hasSelectedValues ? placeholder : '';
  const selectedSingleValue =
    !isMulti && !searchValue && selectedOptions ? selectedOptions.label : '';
  const size = searchValue.length || 2;
  return (
    <div className={styles.triggerInputContainer}>
      {placeHolder && (
        <span
          className={cx(
            styles.placeHolder,
            searchable ? styles.searchable : '',
          )}
        >
          {placeHolder}
        </span>
      )}
      {selectedSingleValue && (
        <span
          className={cx(
            styles.selectedSingleValue,
            searchable ? styles.searchable : '',
          )}
        >
          {selectedSingleValue}
        </span>
      )}
      {isMulti ? (
        <div className={styles.multiOptions}>
          {selectedOptions
            .filter((option) => option && option.label)
            .map((option, index) => (
              <span key={index} className={styles.multiOptionWrapper}>
                <span
                  className={styles.multiOption}
                  key={index}
                  onClick={(evt) => {
                    evt.stopPropagation();
                  }}
                >
                  <span className={styles.label}>{option.label}</span>
                  <span
                    className={cx(
                      styles.closeMultiOption,
                      focusedSelectedOptionIndex === index ? styles.focus : '',
                    )}
                    onClick={(evt) => {
                      evt.stopPropagation();
                      onClickDeselect(evt, option);
                    }}
                  >
                    <FaTimes className={styles.icon} />
                  </span>
                </span>
              </span>
            ))}
        </div>
      ) : null}

      {searchable ? (
        <input
          tabIndex={-1}
          ref={inputRef}
          size={size}
          className={cx(styles.input, error ? styles.error : '')}
          value={searchValue}
          onChange={(evt) => onChange(evt.target.value)}
        />
      ) : null}
    </div>
  );
};

Input.defaultProps = {
  placeholder: 'Select...',
};

Input.propTypes = {
  placeholder: PropTypes.string,
};
