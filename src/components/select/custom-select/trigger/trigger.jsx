import React from 'react';
import cx from 'classnames';
import { FaTimes } from 'react-icons/fa';
import { useFocus } from '../../../../hooks/use-focus';
import { Input } from './input';
import { Tooltip } from '../../../tooltip/tooltip';
import styles from './trigger.module.less';
import { isStringNumberOrNode } from '../../../../helpers/types';

export const Trigger = ({
  selectedOptions,
  searchValue,
  searchable,
  clearable,
  onClickTrigger,
  width,
  element,
  error,
  warning,
  tooltip,
  small,
  isInTable,
  disabled,
  onChangeSearch,
  onClickDeselect,
  onClickClear,
  tabIndex,
  isOpen,
  triggerRef,
  clearAllIsFocused,
  focusedSelectedOptionIndex,
  onFocus,
  onBlur,
  groupOrder,
  maxTooltipWidth,
  placeholder,
  right,
}) => {
  const [inputRef, setInputFocus] = useFocus();

  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case 'first':
          return styles.groupOrderFirst;
        case 'last':
          return styles.groupOrderLast;
        default:
          return styles.groupOrderMiddle;
      }
    }
    return '';
  })();

  const triggerInput = (
    <div
      style={{
        ...(width ? { width } : { overflow: 'hidden' }),
      }}
    >
      {element}
      <div
        ref={triggerRef}
        tabIndex={tabIndex}
        className={cx(
          styles.trigger,
          isOpen ? styles.isOpen : '',
          error ? styles.error : warning ? styles.warning : '',
          disabled ? styles.disabled : '',
          small ? styles.small : '',
          isInTable ? styles.isInTable : '',
          right ? styles.right : '',
          order,
        )}
        onClick={(evt) => {
          evt.stopPropagation();
          setInputFocus();
          onClickTrigger(evt);
        }}
        onFocus={onFocus}
        onBlur={onBlur}
      >
        <Input
          selectedOptions={selectedOptions}
          placeholder={placeholder}
          searchable={searchable}
          searchValue={searchValue}
          inputRef={inputRef}
          onChange={onChangeSearch}
          error={error}
          onClickDeselect={onClickDeselect}
          focusedSelectedOptionIndex={focusedSelectedOptionIndex}
        />
        <span className={styles.icons}>
          {clearable ? (
            <span
              className={cx(
                styles.clearAll,
                clearAllIsFocused ? styles.focus : '',
              )}
              onClick={(evt) => {
                evt.stopPropagation();
                onClickClear(evt);
              }}
            >
              <FaTimes className={cx(styles.iconClear)} />
            </span>
          ) : null}
          <span className={cx(styles.iconOpen)} />
        </span>
      </div>
    </div>
  );

  return (
    <>
      <Tooltip
        error={!!error}
        warning={!!warning}
        text={tooltip || error || warning}
        enabled={
          (tooltip && isStringNumberOrNode(tooltip)) ||
          (error && isStringNumberOrNode(error)) ||
          (warning && isStringNumberOrNode(warning)) ||
          false
        }
        maxWidth={maxTooltipWidth}
      >
        {triggerInput}
      </Tooltip>
    </>
  );
};
