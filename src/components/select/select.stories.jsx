import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaExclamationTriangle, FaSkull } from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Select } from './select';
import { Spacer, Field } from '../../..';
import { Message } from '../message/message';

const style = { height: '500px', width: '500px' };

const onChange = (evt) => {
  const { name, value, label } = evt.target;
  const isMulti = value instanceof Array;
  console.log(name);
  if (!isMulti) {
    console.log(value);
    console.log(label);
  } else {
    value.map((option) => {
      const { value, label } = option;
      console.log(value);
      console.log(label);
    });
  }
};

storiesOf('Forms/Select', module)
  .add('default', () => (
    <Container style={style}>
      <h4>Select (preferred):</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Spacer />
      <h4>Native Select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
      />
    </Container>
  ))
  .add('multi-select', () => (
    <Container style={style}>
      <Field label="Multi-select">
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={onChange}
          value={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
          ]}
        />
      </Field>
      <Field label="Test: option overflow">
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            {
              label:
                'Very long option goes here lorem ipsum dolor east compendum artus lorem ipsum dolor est compendum lorem ipsum',
              value: 'long',
            },
          ]}
          onChange={onChange}
          value={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            {
              label:
                'Very long option goes here lorem ipsum dolor east compendum artus lorem ipsum dolor est compendum lorem ipsum',
              value: 'long',
            },
          ]}
        />
      </Field>
    </Container>
  ))
  .add('empty (no options)', () => {
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    const [nativeValue, setNativeValue] = useState(null);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={[]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={[]}
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
        />
        <Spacer />
        <h4>Native-select:</h4>
        <Select
          name="example"
          native
          options={[]}
          onChange={(evt) => setNativeValue({ value: evt.target.value })}
          value={nativeValue}
        />
        <Spacer />
      </Container>
    );
  })
  .add('unselected', () => {
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    const [nativeValue, setNativeValue] = useState(null);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
        />
        <Spacer />
        <h4>Native-select:</h4>
        <Message
          message={{
            visible: true,
            content:
              'Prefer custom select when null or invalid values can happen',
            type: 'Warning',
          }}
        />
        <Spacer />
        <Select
          name="example"
          native
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setNativeValue({ value: evt.target.value })}
          value={nativeValue}
        />
        <Spacer />
      </Container>
    );
  })
  .add('clearable', () => {
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    const [nativeValue, setNativeValue] = useState(null);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
          clearable
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
          clearable
        />
        <Spacer />
        <h4>Native-select:</h4>
        <Message
          message={{
            visible: true,
            content: 'Prefer custom select for clearable variant',
            type: 'Warning',
          }}
        />
        <Spacer />
        <Select
          name="example"
          native
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setNativeValue({ value: evt.target.value })}
          value={nativeValue}
          clearable
        />
        <Spacer />
      </Container>
    );
  })
  .add('custom placeholder', () => {
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    const [nativeValue, setNativeValue] = useState(null);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          placeholder="Select the best animal..."
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          placeholder="Select the best animal..."
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
        />
        <Spacer />
        <h4>Native-select:</h4>
        <Message
          message={{
            visible: true,
            content:
              'Prefer custom select when null or invalid values can happen',
            type: 'Warning',
          }}
        />
        <Spacer />
        <Select
          name="example"
          native
          placeholder="Select the best animal..."
          options={[
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setNativeValue({ value: evt.target.value })}
          value={nativeValue}
        />
        <Spacer />
      </Container>
    );
  })
  .add('with headings', () => (
    <Container style={style}>
      <Select
        name="example"
        options={[
          { type: 'Heading', label: 'Large' },
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { type: 'Heading', label: 'Small' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
    </Container>
  ))
  .add('with duplicated labels', () => {
    const [value, setValue] = useState({ label: 'Kangaroos', value: 'moss' });
    return (
      <Container style={style}>
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Kangaroos', value: 'moss' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
          clearable
        />
      </Container>
    );
  })
  .add('without search filter', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        searchable={false}
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
        ]}
        searchable={false}
      />
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        small
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
        ]}
        small
      />
      <Spacer />
      <h4>Native-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
        small
      />
    </Container>
  ))
  .add('right', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        right
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
        ]}
        right
      />
      <Spacer />
      <h4>Native-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
        right
      />
    </Container>
  ))
  .add('auto width', () => {
    const options = [
      { label: 'Aardvarks', value: 'termites' },
      { label: 'Kangaroos and wallabies', value: 'grass' },
      { label: 'Monkeys', value: 'bananas' },
      { label: 'Possums', value: 'slugs' },
      {
        label:
          'Very long label lorem ipsum dolor sit amet, consectetur adipiscing elit',
        value: 'too long',
      },
    ];
    const message =
      'Avoid `width: auto` for very long lists in custom selects (slow)';
    const [value, setValue] = useState(options[2]);
    const [multiValue, setMultiValue] = useState([
      options[1],
      options[2],
      options[3],
    ]);
    const [nativeValue, setNativeValue] = useState(options[2]);
    return (
      <Container style={style} warningMessage={message}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          width="auto"
          options={options}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          width="auto"
          options={options}
          clearable
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
        />
        <Spacer />
        <h4>Native-select:</h4>
        <Select
          name="example"
          width="auto"
          options={options}
          onChange={(evt) => setNativeValue({ value: evt.target.value })}
          value={nativeValue}
          native
        />
      </Container>
    );
  })
  .add('auto layer width', () => {
    const options = [
      { label: 'Aardvarks', value: 'termites' },
      { label: 'Kangaroos and wallabies', value: 'grass' },
      { label: 'Monkeys', value: 'bananas' },
      { label: 'Possums', value: 'slugs' },
      {
        label: 'Very long label that is wider than input width (layer expands)',
        value: 'too long',
      },
    ];
    const message =
      'Avoid autoLayerWidth for very long lists in custom selects (slow)';
    const [value, setValue] = useState(options[2]);
    const [multiValue, setMultiValue] = useState([
      options[1],
      options[2],
      options[3],
    ]);
    return (
      <Container style={style} warningMessage={message}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          width="150px"
          options={options}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
          autoLayerWidth
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          width="150px"
          options={options}
          clearable
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
          autoLayerWidth
        />
      </Container>
    );
  })
  .add('fixed width', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        width="200px"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        width="200px"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        clearable
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
      />
      <Spacer />
      <h4>Native-select:</h4>
      <Select
        name="example"
        width="200px"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
      />
    </Container>
  ))
  .add('with tooltip', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        tooltip="Some info"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        tooltip="Some info"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
      />
      <Spacer />
      <h4>Native-select:</h4>
      <Select
        name="example"
        tooltip="Some info"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
      />
    </Container>
  ))
  .add('with error', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        error="Error message goes here"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        error="Error message goes here"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
      />
      <Spacer />
      <h4>Native-select:</h4>
      <Select
        name="example"
        error="Error message goes here"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
      />
    </Container>
  ))
  .add('with warning', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        warning="Warning message goes here"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        warning="Warning message goes here"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
      />
      <Spacer />
      <h4>Native-select:</h4>
      <Select
        name="example"
        warning="Warning message goes here"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
      />
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        disabled
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        disabled
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
      />
      <Spacer />
      <h4>Native-select:</h4>
      <Select
        name="example"
        disabled
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
        native
      />
    </Container>
  ))
  .add('numeric', () => {
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    const [nativeValue, setNativeValue] = useState(null);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={[
            { label: 1, value: 1 },
            { label: 2, value: 2 },
            { label: 3, value: 3 },
            { label: 4, value: 4 },
          ]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={[
            { label: 1, value: 1 },
            { label: 2, value: 2 },
            { label: 3, value: 3 },
            { label: 4, value: 4 },
          ]}
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
        />
        <Spacer />
        <h4>Native select:</h4>
        <Message
          message={{
            visible: true,
            content:
              'Native input onChange always returns string type according to HTML spec (not a bug)',
            type: 'Warning',
          }}
        />
        <Spacer />
        <Select
          name="example"
          native
          options={[
            { label: 1, value: 1 },
            { label: 2, value: 2 },
            { label: 3, value: 3 },
            { label: 4, value: 4 },
          ]}
          onChange={(evt) => {
            setNativeValue({ value: evt.target.value });
          }}
          value={nativeValue}
        />
      </Container>
    );
  })
  .add('with icons', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <Select
        name="example"
        options={[
          {
            label: 'Aardvarks',
            value: 'termites',
          },
          { label: 'Monkeys', value: 'bananas' },
          {
            label: 'Vampires',
            value: 'blood',
            icon: <FaExclamationTriangle color="#f2711c" />,
          },
          {
            label: 'Zombies',
            value: 'brains',
            icon: <FaSkull color="#db2828" />,
          },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
      <Spacer />
      <h4>Multi-select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          {
            label: 'Vampires',
            value: 'blood',
            icon: <FaExclamationTriangle color="#f2711c" />,
          },
          {
            label: 'Zombies',
            value: 'brains',
            icon: <FaSkull color="#db2828" />,
          },
        ]}
        onChange={onChange}
        value={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
        ]}
      />
    </Container>
  ))
  .add('with disabled options', () => (
    <Container style={style}>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass', disabled: true },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs', disabled: true },
        ]}
        onChange={onChange}
        value={{ label: 'Monkeys', value: 'bananas' }}
      />
    </Container>
  ))
  .add('managed', () => {
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
          clearable
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
          clearable
        />
      </Container>
    );
  })
  .add('autoScroll (on by default)', () => {
    const [value, setValue] = useState({ label: 'Tiger', value: 'deer' });
    const [multiValue, setMultiValue] = useState([
      { label: 'Impala', value: 'shrubs' },
      { label: 'Bat', value: 'insects' },
    ]);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
            { label: 'Gorilla', value: 'ants' },
            { label: 'Puma', value: 'coyotes' },
            { label: 'Patagonian Mara', value: 'cactus' },
            { label: 'Tarsier', value: 'moths' },
            { label: 'Koala', value: 'eucalyptus' },
            { label: 'Panda', value: 'bamboo' },
            { label: 'Red Panda', value: 'roots' },
            { label: 'Impala', value: 'shrubs' },
            { label: 'Tiger', value: 'deer' },
            { label: 'Crocodile', value: 'lizards' },
            { label: 'Bat', value: 'insects' },
          ]}
          onChange={(evt) => setValue({ value: evt.target.value })}
          value={value}
          clearable
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
            { label: 'Gorilla', value: 'ants' },
            { label: 'Puma', value: 'coyotes' },
            { label: 'Patagonian Mara', value: 'cactus' },
            { label: 'Tarsier', value: 'moths' },
            { label: 'Koala', value: 'eucalyptus' },
            { label: 'Panda', value: 'bamboo' },
            { label: 'Red Panda', value: 'roots' },
            { label: 'Impala', value: 'shrubs' },
            { label: 'Tiger', value: 'deer' },
            { label: 'Crocodile', value: 'lizards' },
            { label: 'Bat', value: 'insects' },
          ]}
          onChange={(evt) => setMultiValue(evt.target.value)}
          value={multiValue}
          clearable
        />
      </Container>
    );
  })
  .add('createable', () => {
    const [options, setOptions] = useState([
      { label: 'Aardvarks', value: 'termites' },
      { label: 'Kangaroos', value: 'grass' },
      { label: 'Monkeys', value: 'bananas' },
      { label: 'Possums', value: 'slugs' },
    ]);
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={options}
          onChange={(evt) => setValue({ value: evt.target.value })}
          onCreate={(evt) =>
            setOptions(
              options.concat({
                label: evt,
                value: evt,
              }),
            )
          }
          value={value}
          clearable
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={options}
          onChange={(evt) => setMultiValue(evt.target.value)}
          onCreate={(evt) =>
            setOptions(
              options.concat({
                label: evt,
                value: evt,
              }),
            )
          }
          value={multiValue}
          clearable
        />
      </Container>
    );
  })
  .add('simple values format', () => (
    <Container style={style}>
      <h4>Select:</h4>
      <Select
        name="example"
        options={['Aardvarks', 'Kangaroos', 'Monkeys', 'Possums']}
        onChange={onChange}
        value="Monkeys"
      />
      <Spacer />
      <h4>Multiselect:</h4>
      <Select
        name="example"
        options={['Aardvarks', 'Kangaroos', 'Monkeys', 'Possums']}
        onChange={onChange}
        value={['Kangaroos', 'Monkeys']}
      />
      <Spacer />
      <h4>Native Select:</h4>
      <Select
        name="example"
        options={['Aardvarks', 'Kangaroos', 'Monkeys', 'Possums']}
        onChange={onChange}
        value="Monkeys"
        native
      />
    </Container>
  ))
  .add('mixed values format', () => (
    <Container style={style}>
      <h4>Select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value="bananas"
      />
      <Spacer />
      <h4>Multiselect:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value={['grass', { value: 'bananas' }]}
      />
      <Spacer />
      <h4>Native Select:</h4>
      <Select
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas' },
          { label: 'Possums', value: 'slugs' },
        ]}
        onChange={onChange}
        value="bananas"
        native
      />
    </Container>
  ))
  .add('with long list', () => {
    const options = [...Array(10000).keys()].map((o, i) => ({
      label: Math.random().toString(36).substring(7),
      value: i,
    }));
    const message =
      'Avoid `width: auto` and autoLayerWidth for very long lists in custom selects (slow)';
    return (
      <Container style={style} warningMessage={message}>
        <Select
          name="example"
          options={options}
          onChange={onChange}
          value={[options[3], options[56]]}
          clearable
          autoLayerWidth={false}
        />
      </Container>
    );
  })
  .add('overflowing container', () => (
    <Container style={style}>
      <h4>Single-select:</h4>
      <div
        style={{
          background: '#eee',
          overflow: 'scroll',
          padding: 40,
          width: 250,
          position: 'relative',
        }}
      >
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys long title goes here', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={onChange}
          value={{ label: 'Monkeys long title goes here', value: 'bananas' }}
        />
      </div>
      <h4>Multi-select:</h4>
      <div
        style={{
          background: '#eee',
          overflow: 'scroll',
          padding: 40,
          width: 250,
          position: 'relative',
        }}
      >
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys long title goes here', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={onChange}
          value={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys long title goes here', value: 'bananas' },
          ]}
        />
      </div>
    </Container>
  ))
  .add('with deprecated event handlers', () => {
    const [value, setValue] = useState(null);
    const [multiValue, setMultiValue] = useState([]);
    return (
      <Container style={style}>
        <h4>Single-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          deprecatedEventHandler
          onChange={(option) => setValue(option)}
          value={value}
          clearable
        />
        <Spacer />
        <h4>Multi-select:</h4>
        <Select
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          deprecatedEventHandler
          onChange={(options) => setMultiValue(options)}
          value={multiValue}
          clearable
        />
      </Container>
    );
  });
