import {
  isMulti,
  isSimpleOptions,
  isSimpleSelectedOptions,
  standardizeOptions,
  standardizeSelectedOptions,
  standardizeSelectedOption,
  getFirstSelectedOptionIndex,
} from './select.input';

describe('select (inputs)', () => {
  test('is multi select', () => {
    expect(isMulti([])).toBe(true);
    expect(isMulti([{ label: 'Monkeys', value: 'bananas' }])).toBe(true);
    expect(isMulti(null)).toBe(false);
    expect(isMulti('Monkeys')).toBe(false);
    expect(isMulti({ label: 'Monkeys', value: 'bananas' })).toBe(false);
  });

  test('is simple options', () => {
    expect(isSimpleOptions(['Cat', 'Mouse'])).toBe(true);
    expect(isSimpleOptions([1, 2])).toBe(true);
    expect(
      isSimpleOptions([
        { label: 'Cat', value: 'Cat' },
        { label: 'Mouse', value: 'Mouse' },
      ]),
    ).toBe(false);
    expect(isSimpleOptions([{ label: 'Cat', value: 'Cat' }, 'Mouse', 4])).toBe(
      true,
    );
    expect(isSimpleOptions([])).toBe(false);
    expect(isSimpleOptions(undefined)).toBe(false);
  });

  test('is simple selected options', () => {
    expect(isSimpleSelectedOptions(['Cat', 'Mouse'])).toBe(true);
    expect(isSimpleSelectedOptions([11, 12])).toBe(true);
    expect(isSimpleSelectedOptions('Cat')).toBe(true);
    expect(isSimpleSelectedOptions(6)).toBe(true);
    expect(
      isSimpleOptions([
        { label: 'Cat', value: 'Cat' },
        { label: 'Mouse', value: 'Mouse' },
      ]),
    ).toBe(false);
    expect(isSimpleOptions([{ label: 'Cat', value: 'Cat' }, 'Mouse', 7])).toBe(
      true,
    );
    expect(isSimpleOptions([])).toBe(false);
    expect(isSimpleOptions(undefined)).toBe(false);
  });

  test('standardize options', () => {
    expect(
      standardizeOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        false,
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(standardizeOptions(['Cat', 'Mouse'], true)).toStrictEqual([
      { label: 'Cat', value: 'Cat' },
      { label: 'Mouse', value: 'Mouse' },
    ]);
    expect(standardizeOptions([5, 6], true)).toStrictEqual([
      { label: 5, value: 5 },
      { label: 6, value: 6 },
    ]);
    expect(
      standardizeOptions([{ label: 'Cat', value: 'cat' }, 'Mouse', 9], true),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'Mouse' },
      { label: 9, value: 9 },
    ]);
    expect(standardizeOptions([], true)).toStrictEqual([]);
    expect(standardizeOptions(undefined, true)).toStrictEqual(undefined);
  });

  test('standardize selected options (single select)', () => {
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], {
        label: 'Cat',
        value: 'cat',
      }),
    ).toStrictEqual({ label: 'Cat', value: 'cat' });
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], 'cat'),
    ).toStrictEqual({
      label: 'Cat',
      value: 'cat',
    });
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], {
        value: 'cat',
        monkeys: 'bananas',
      }),
    ).toStrictEqual({
      label: 'Cat',
      value: 'cat',
    });
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], 'Foobar'),
    ).toStrictEqual(undefined);
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], {
        value: '',
      }),
    ).toStrictEqual(undefined);
    expect(
      standardizeSelectedOptions([{ label: 'Cat', value: 'cat' }], null),
    ).toStrictEqual(undefined);
    expect(
      standardizeSelectedOptions([{ label: 8, value: 8 }], 8),
    ).toStrictEqual({
      label: 8,
      value: 8,
    });
    expect(
      standardizeSelectedOptions([{ label: 8, value: 8 }], '8'), //allow coercion
    ).toStrictEqual({
      label: 8,
      value: 8,
    });
    expect(standardizeSelectedOptions([], null)).toStrictEqual(undefined);
    expect(standardizeSelectedOptions([], undefined)).toStrictEqual(undefined);
  });

  test('standardize selected options (multi-select)', () => {
    expect(
      standardizeSelectedOptions(null, [
        { label: 'Cat', value: 'cat' },
        { label: 'Mouse', value: 'mouse' },
      ]),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        ['cat', 'mouse'],
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 1, value: 1 },
          { label: 2, value: 2 },
        ],
        [1, 2],
      ),
    ).toStrictEqual([
      { label: 1, value: 1 },
      { label: 2, value: 2 },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 1, value: 1 },
          { label: 2, value: 2 },
        ],
        ['1', '2'], //allow coercion
      ),
    ).toStrictEqual([
      { label: 1, value: 1 },
      { label: 2, value: 2 },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 5, value: 5 },
        ],
        ['cat', { label: 'Mouse', value: 'mouse' }, 5],
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
      { label: 5, value: 5 },
    ]);
    expect(
      standardizeSelectedOptions(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 5, value: 5 },
        ],
        [
          { value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          null,
          undefined,
          '',
        ],
      ),
    ).toStrictEqual([
      { label: 'Cat', value: 'cat' },
      { label: 'Mouse', value: 'mouse' },
    ]);
    expect(standardizeSelectedOptions(null, [])).toStrictEqual([]);
  });

  test('standardize selected option', () => {
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        { label: 'Mouse', value: 'mouse' },
      ),
    ).toStrictEqual({ label: 'Mouse', value: 'mouse' });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        { value: 'mouse' },
      ),
    ).toStrictEqual({ label: 'Mouse', value: 'mouse' });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        'mouse',
      ),
    ).toStrictEqual({ label: 'Mouse', value: 'mouse' });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        { value: 1 },
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        { value: '1' }, //allow coercion
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        1,
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
        ],
        '1', //allow coercion
      ),
    ).toStrictEqual({ label: 'Cat', value: 1 });
    expect(
      standardizeSelectedOption(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
        ],
        null,
      ),
    ).toStrictEqual(undefined);
    expect(standardizeSelectedOption([], 'mouse')).toStrictEqual(undefined);
    expect(standardizeSelectedOption(undefined, 'mouse')).toStrictEqual(
      undefined,
    );
  });

  test('get first selected option index', () => {
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        { label: 'Mouse', value: 'mouse' },
      ),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
      ),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
          { label: 'Dog', value: 3 },
        ],
        { label: 'Mouse', value: 2 },
      ),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 1 },
          { label: 'Mouse', value: 2 },
          { label: 'Dog', value: 3 },
        ],
        { label: 'Mouse', value: '2' }, //allow coercion
      ),
    ).toBe(1);
    expect(getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'], 'Mouse')).toBe(
      1,
    );
    expect(getFirstSelectedOptionIndex([1, 2, 3], 2)).toBe(1);
    expect(getFirstSelectedOptionIndex([1, 2, 3], '2')).toBe(
      //allow coercion
      1,
    );
    expect(
      getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'], ['Mouse', 'Dog']),
    ).toBe(1);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        { label: 'Camel', value: 'camel' },
      ),
    ).toBe(null);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [{ label: 'Camel', value: 'camel' }],
      ),
    ).toBe(null);
    expect(getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'], 'Camel')).toBe(
      null,
    );
    expect(
      getFirstSelectedOptionIndex(['Cat', 'Mouse', 'Dog'], ['Camel', 'Sheep']),
    ).toBe(null);
    expect(getFirstSelectedOptionIndex([], 'Camel')).toBe(null);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        null,
      ),
    ).toBe(null);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        [],
      ),
    ).toBe(null);
    expect(
      getFirstSelectedOptionIndex(
        [
          { label: 'Cat', value: 'cat' },
          { label: 'Mouse', value: 'mouse' },
          { label: 'Dog', value: 'dog' },
        ],
        undefined,
      ),
    ).toBe(null);
  });
});
