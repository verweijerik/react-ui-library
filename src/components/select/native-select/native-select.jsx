import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './native-select.module.less';
import { Tooltip } from '../../tooltip/tooltip';
import { isStringNumberOrNode } from '../../../helpers/types';

export const NativeSelect = ({
  disabled,
  error,
  tooltip,
  options,
  selectedOption,
  right,
  small,
  width,
  borderRadius,
  onChange,
  tabIndex,
  warning,
  onFocus,
  onBlur,
  maxTooltipWidth,
  placeholder,
  clearable,
}) => {
  const internalValue =
    selectedOption === null
      ? 'unselected'
      : selectedOption.value === null
      ? 'unselected'
      : selectedOption.value; //null not allowed as a React prop to a select input
  const isUnselected = internalValue === 'unselected';

  const viewOptions = (internalValue === 'unselected' || clearable
    ? [
        <option key="unselected" value="unselected" disabled={!clearable}>
          {placeholder || 'Select...'}
        </option>,
      ]
    : []
  ).concat(
    options.map((option, i) => {
      return (
        <option key={i} value={option.value}>
          {option.label}
        </option>
      );
    }),
  );

  return (
    <Tooltip
      error={!!error}
      warning={!!warning}
      text={tooltip || error || warning}
      enabled={
        (tooltip && isStringNumberOrNode(tooltip)) ||
        (error && isStringNumberOrNode(error)) ||
        (warning && isStringNumberOrNode(warning)) ||
        false
      }
      maxWidth={maxTooltipWidth}
    >
      <select
        className={cx(
          styles.select,
          isUnselected ? styles.unSelected : '',
          error ? styles.error : warning ? styles.warning : '',
          right ? styles.right : '',
          small ? styles.small : '',
        )}
        style={{
          width: width || '',
          borderRadius: borderRadius || '',
        }}
        onChange={(evt) => {
          const { value } = evt.target;
          const nextValue =
            value === 'unselected'
              ? { value: null }
              : options.find((o) => String(o.value) === String(value));
          onChange(evt, nextValue);
        }}
        onFocus={onFocus}
        onBlur={onBlur}
        tabIndex={tabIndex}
        value={internalValue}
        disabled={disabled}
      >
        {viewOptions}
      </select>
    </Tooltip>
  );
};

export const selectOptionShape = {
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

NativeSelect.defaultProps = {
  borderRadius: null,
  disabled: false,
  error: null,
  warning: null,
  tooltip: null,
  onChange: () => {},
  onFocus: () => {},
  onBlur: () => {},
  right: false,
  small: false,
  tabIndex: 0,
  selectedOption: null,
  width: null,
};

export const NativeSelectShape = {
  borderRadius: PropTypes.number,
  disabled: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  warning: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  tooltip: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  options: PropTypes.arrayOf(PropTypes.shape(selectOptionShape)).isRequired,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  right: PropTypes.bool,
  small: PropTypes.bool,
  tabIndex: PropTypes.number,
  selectedOption: PropTypes.shape(selectOptionShape),
  width: PropTypes.string,
};

NativeSelect.propTypes = NativeSelectShape;
