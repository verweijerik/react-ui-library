export const isMulti = (selectedOptions) => selectedOptions instanceof Array;

export const standardizeOptions = (options, simple) => {
  return simple && options instanceof Array
    ? options.map((o) =>
        typeof o === 'string' || typeof o === 'number'
          ? { label: o, value: o }
          : o,
      )
    : options;
};

export const standardizeSelectedOption = (
  standardizedOptions,
  selectedOption,
) => {
  if (
    selectedOption !== null &&
    typeof selectedOption === 'object' &&
    selectedOption.hasOwnProperty('label') &&
    selectedOption.hasOwnProperty('value')
  ) {
    return selectedOption;
  } else {
    const value =
      selectedOption !== null &&
      typeof selectedOption === 'object' &&
      selectedOption.hasOwnProperty('value')
        ? selectedOption.value
        : selectedOption;
    return standardizedOptions instanceof Array
      ? standardizedOptions.find((o) => o.value == value /* allow coercion */)
      : undefined;
  }
};

export const standardizeSelectedOptions = (
  standardizedOptions,
  selectedOptions,
) => {
  const isMulti = selectedOptions instanceof Array;
  if (isMulti) {
    return selectedOptions
      .map((selectedOption) =>
        standardizeSelectedOption(standardizedOptions, selectedOption),
      )
      .filter((selectedOption) => selectedOption !== undefined);
  } else {
    return selectedOptions === null
      ? undefined
      : standardizeSelectedOption(standardizedOptions, selectedOptions);
  }
};

export const isSimpleOptions = (options) =>
  options instanceof Array &&
  options.some((o) => typeof o === 'string' || typeof o === 'number');

export const isSimpleSelectedOptions = (selectedOptions) => {
  return (
    typeof selectedOptions === 'string' ||
    typeof selectedOptions === 'number' ||
    (selectedOptions instanceof Array && isSimpleOptions(selectedOptions))
  );
};

export const getFirstSelectedOptionIndex = (options, selectedOptions) => {
  const firstSelected =
    selectedOptions instanceof Array
      ? selectedOptions.length
        ? selectedOptions[0]
        : null
      : selectedOptions;
  const firstSelectedValue =
    firstSelected === null || firstSelected === undefined
      ? null
      : typeof firstSelected === 'string' || typeof firstSelected === 'number'
      ? firstSelected
      : firstSelected.value;
  const index = options.findIndex((o) => {
    if (typeof o === 'string' || typeof o === 'number') {
      return o == firstSelectedValue; //allow coercion
    } else {
      return o.value == firstSelectedValue; //allow coercion
    }
  });
  return index === -1 ? null : index;
};

export const standardizeInputs = (
  options,
  selectedOptions,
  enableAutoScroll,
) => {
  const cleanedOptions = options instanceof Array ? options : []; // don't crash when options is null or wrong type
  const multi = isMulti(selectedOptions);
  const simpleOptions = isSimpleOptions(cleanedOptions);
  const simpleSelectedOptions = isSimpleSelectedOptions(selectedOptions);
  const standardizedOptions = standardizeOptions(cleanedOptions, simpleOptions);
  const firstSelectedOptionIndex = enableAutoScroll
    ? getFirstSelectedOptionIndex(cleanedOptions, selectedOptions)
    : null;
  return {
    options: standardizedOptions,
    selectedOptions: standardizeSelectedOptions(
      standardizedOptions,
      selectedOptions,
      simpleSelectedOptions,
      multi,
    ),
    multi,
    simpleInputs: simpleOptions,
    firstSelectedOptionIndex,
  };
};
