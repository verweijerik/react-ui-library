import React, { useReducer } from 'react';
import PropTypes from 'prop-types';
import {
  FaInfoCircle,
  FaCheckCircle,
  FaExclamationCircle,
  FaExclamationTriangle,
} from 'react-icons/fa';
import cx from 'classnames';
import { Dismiss } from './dismiss';
import styles from './message.module.less';

const initialState = {
  detailsVisible: false,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'TOGGLE_DETAILS': {
      return {
        ...state,
        detailsVisible: !state.detailsVisible,
      };
    }
    default:
      return state;
  }
};

const Details = ({ details, visible, dispatch }) => {
  return (
    <div>
      <div
        className={cx(styles.legendToggle)}
        onClick={() => dispatch({ type: 'TOGGLE_DETAILS' })}
      >
        {visible ? 'Hide details' : 'Show details'}
      </div>
      {visible ? <div className={styles.detailsText}>{details}</div> : null}
    </div>
  );
};

const DialogIcon = ({ type }) => {
  const size = 16;
  switch (type) {
    case 'Info':
      return <FaInfoCircle size={size} />;
    case 'Success':
      return <FaCheckCircle size={size} />;
    case 'Warning':
      return <FaExclamationCircle size={size} />;
    case 'Error':
      return <FaExclamationTriangle size={size} />;
    default:
      return null;
  }
};

export const Message = ({ message }) => {
  const {
    visible,
    type,
    icon,
    heading,
    content,
    details,
    withDismiss,
    onClose,
    width,
  } = message;
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <>
      {visible ? (
        <div
          className={cx(
            styles.container,
            width ? styles.block : null,
            type === 'Info'
              ? styles.info
              : type === 'Success'
              ? styles.success
              : type === 'Warning'
              ? styles.warning
              : type === 'Error'
              ? styles.error
              : null,
          )}
          style={{ width }}
        >
          <div>
            {icon ? (
              <div className={styles.icon}>
                <DialogIcon type={type} />
              </div>
            ) : null}
          </div>
          <div className={styles.content}>
            {heading ? <div className={styles.heading}>{heading}</div> : null}
            <div>{content}</div>
            {details ? (
              <Details
                details={details}
                visible={state.detailsVisible}
                dispatch={() => dispatch({ type: 'TOGGLE_DETAILS' })}
              />
            ) : null}
          </div>
          {withDismiss ? <Dismiss type={type} onClose={onClose} /> : null}
        </div>
      ) : null}
    </>
  );
};

const messageShape = PropTypes.shape({
  visible: PropTypes.bool,
  type: PropTypes.oneOf(['Info', 'Success', 'Warning', 'Error']),
  icon: PropTypes.bool,
  heading: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  details: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  withDismiss: PropTypes.bool,
  onClose: PropTypes.func,
  width: PropTypes.string,
});

Message.propTypes = {
  message: messageShape.isRequired,
};
