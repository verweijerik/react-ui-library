import React, { useReducer } from 'react';
import { storiesOf } from '@storybook/react';

import { Container } from '../../helpers/container';
import { Message } from './message';

const style = {
  height: '500px',
  width: '500px',
};

const initialState = {
  visible: true,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'ON_CLOSE': {
      return {
        ...state,
        visible: false,
      };
    }
    default:
      return state;
  }
};

const ManagedMessage = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Message
      message={{
        visible: state.visible,
        content: 'Here is some info',
        type: 'Info',
        withDismiss: true,
        onClose: () => dispatch({ type: 'ON_CLOSE' }),
      }}
    />
  );
};

storiesOf('Basic/Message', module).add('managed', () => (
  <Container style={style}>
    <ManagedMessage />
  </Container>
));
