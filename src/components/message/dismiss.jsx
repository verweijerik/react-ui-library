import React from 'react';
import cx from 'classnames';
import { FaTimes } from 'react-icons/fa';
import styles from './message.module.less';

export const Dismiss = ({ type, onClose, isInToast }) => {
  return (
    <div
      className={cx(
        styles.dismiss,
        isInToast ? styles.absolute : '',
        type === 'Info'
          ? styles.info
          : type === 'Success'
          ? styles.success
          : type === 'Warning'
          ? styles.warning
          : type === 'Error'
          ? styles.error
          : null,
      )}
    >
      <FaTimes onClick={onClose} />
    </div>
  );
};
