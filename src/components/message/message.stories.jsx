import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Message } from '../../..';

const style = {
  height: '500px',
  width: '500px',
};

const infoMessage = {
  visible: true,
  content: 'Here is some info',
  type: 'Info',
};

const successMessage = {
  visible: true,
  content: 'Great results',
  type: 'Success',
};

const warningMessage = {
  visible: true,
  content: 'Something seems sketchy',
  type: 'Warning',
};

const errorMessage = {
  visible: true,
  content: 'Something went wrong',
  type: 'Error',
};

const withHeading = {
  visible: true,
  heading: 'Info heading',
  content: 'Here is some info',
  type: 'Info',
};

const infoMessageWithIcon = {
  visible: true,
  icon: true,
  content: 'Info message',
  type: 'Info',
};
const successMessageWithIcon = {
  visible: true,
  icon: true,
  content: 'Success message',
  type: 'Success',
};
const warningMessageWithIcon = {
  visible: true,
  icon: true,
  content: 'Warning message',
  type: 'Warning',
};
const errorMessageWithIcon = {
  visible: true,
  icon: true,
  content: 'Error message',
  type: 'Error',
};

const withDismiss = {
  visible: true,
  content: 'Here is some info',
  type: 'Info',
  withDismiss: true,
};

const withDetails = {
  visible: true,
  content: 'Here is some info',
  type: 'Info',
  details: 'Lorem ipsum dolor sit amet',
};

const fixedWidth = {
  visible: true,
  heading: 'Error heading',
  content: 'An error has happened',
  icon: true,
  type: 'Error',
  width: '300px',
  withDismiss: true,
};

const maxWidth = {
  visible: true,
  heading: 'Error heading',
  content: 'An error has happened',
  icon: true,
  type: 'Error',
  width: '100%',
  withDismiss: true,
};

const kitchenSink = {
  visible: true,
  heading: 'Error heading',
  content: 'An error has happened',
  details: 'Bad input data',
  icon: true,
  type: 'Error',
  withDismiss: true,
};

storiesOf('Basic/Message', module)
  .add('info', () => (
    <Container style={style}>
      <Message message={infoMessage} />
    </Container>
  ))
  .add('success', () => (
    <Container style={style}>
      <Message message={successMessage} />
    </Container>
  ))
  .add('warning', () => (
    <Container style={style}>
      <Message message={warningMessage} />
    </Container>
  ))
  .add('error', () => (
    <Container style={style}>
      <Message message={errorMessage} />
    </Container>
  ))
  .add('with heading', () => (
    <Container style={style}>
      <Message message={withHeading} />
    </Container>
  ))
  .add('with icon', () => (
    <Container style={style}>
      <div style={{ marginBottom: 20 }}>
        <Message message={infoMessageWithIcon} />
      </div>
      <div style={{ marginBottom: 20 }}>
        <Message message={successMessageWithIcon} />
      </div>
      <div style={{ marginBottom: 20 }}>
        <Message message={warningMessageWithIcon} />
      </div>
      <div style={{ marginBottom: 20 }}>
        <Message message={errorMessageWithIcon} />
      </div>
    </Container>
  ))
  .add('with dismiss', () => (
    <Container style={style}>
      <Message message={withDismiss} />
    </Container>
  ))
  .add('with details', () => (
    <Container style={style}>
      <Message message={withDetails} />
    </Container>
  ))
  .add('fixed width', () => (
    <Container style={style}>
      <Message message={fixedWidth} />
    </Container>
  ))
  .add('full width', () => (
    <Container style={style}>
      <Message message={maxWidth} />
    </Container>
  ))
  .add('kitchen sink', () => (
    <Container style={style}>
      <Message message={kitchenSink} />
    </Container>
  ));
