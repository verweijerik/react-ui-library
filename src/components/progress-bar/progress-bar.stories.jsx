import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { ProgressBar } from './progress-bar.jsx';
import { Button } from '../button/button';

const style = {
  height: '500px',
  width: '500px',
};

storiesOf('Progress/ProgressBar', module)
  .add('default', () => (
    <Container style={style}>
      <ProgressBar percentage={40} />
    </Container>
  ))
  .add('grayscale', () => (
    <Container style={style}>
      <ProgressBar percentage={40} colored={false} />
    </Container>
  ))
  .add('without label', () => (
    <Container style={style}>
      <ProgressBar percentage={40} noLabel />
    </Container>
  ))
  .add('with progress colors', () => (
    <Container style={style}>
      <ProgressBar percentage={4} showProgressColors />
      <br />
      <ProgressBar percentage={15} showProgressColors />
      <br />
      <ProgressBar percentage={30} showProgressColors />
      <br />
      <ProgressBar percentage={55} showProgressColors />
      <br />
      <ProgressBar percentage={70} showProgressColors />
      <br />
      <ProgressBar percentage={90} showProgressColors />
    </Container>
  ))
  .add('inverted', () => (
    <Container style={{ ...style, backgroundColor: '#262626' }}>
      <ProgressBar percentage={40} inverted />
    </Container>
  ))
  .add('fixed width', () => (
    <Container style={style}>
      <ProgressBar
        percentage={40}
        progressStatus="Installing..."
        width="200px"
      />
    </Container>
  ))
  .add('managed', () => {
    const [percentage, setPercentage] = useState(5);
    const set = (dir) =>
      setPercentage(
        (percentage + ((10 * (dir === '+' ? 1 : -1)) % 100) + 100) % 100,
      );
    return (
      <Container style={style}>
        <Button label="-" onClick={() => set('-')} small />
        <Button label="+" onClick={() => set('+')} small />
        <div style={{ marginTop: '20px', marginBottom: '50px' }}>
          <h4>Default:</h4>
          <ProgressBar percentage={percentage} progressStatus="Installing..." />
        </div>
        <div style={{ marginTop: '20px', marginBottom: '50px' }}>
          <h4>Grayscale:</h4>
          <ProgressBar
            percentage={percentage}
            progressStatus="Installing..."
            colored={false}
          />
        </div>
        <div style={{ marginBottom: '50px' }}>
          <h4>With progress colors:</h4>
          <ProgressBar
            percentage={percentage}
            progressStatus="Installing..."
            showProgressColors
          />
        </div>
        <div
          style={{
            marginBottom: '50px',
            height: '100px',
            backgroundColor: '#262626',
          }}
        >
          <h4 style={{ color: 'white' }}>Inverted:</h4>
          <ProgressBar
            percentage={percentage}
            progressStatus="Installing..."
            inverted
          />
        </div>
      </Container>
    );
  });
