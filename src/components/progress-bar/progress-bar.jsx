import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './progress-bar.module.less';

const getColor = (perct) => {
  if (perct < 5) return styles.grey;
  if (perct < 20) return styles.red;
  if (perct < 45) return styles.orange;
  if (perct < 60) return styles.yellow;
  if (perct < 80) return styles.olive;
  if (perct < 100) return styles.green;
  return 'green';
};

export const ProgressBar = ({
  width,
  inverted,
  colored,
  showProgressColors,
  percentage,
  noLabel,
}) => {
  return (
    <div style={{ width }} data-percent={percentage}>
      <div
        className={cx(styles.progress, inverted ? styles.inverted : '')}
        style={{ width }}
      >
        <div
          className={cx(
            styles.bar,
            showProgressColors
              ? getColor(percentage)
              : colored
              ? styles.colored
              : '',
          )}
          style={{ width: `${percentage}%` }}
        >
          <div className={styles.label}>{!noLabel && `${percentage}%`}</div>
        </div>
      </div>
    </div>
  );
};

ProgressBar.defaultProps = {
  width: 'auto',
  inverted: false,
  percentage: 0,
  colored: true,
  showProgressColors: false,
  noLabel: false,
};

ProgressBar.propTypes = {
  width: PropTypes.string,
  colored: PropTypes.bool,
  inverted: PropTypes.bool,
  percentage: PropTypes.number,
  showProgressColors: PropTypes.bool,
  noLabel: PropTypes.bool,
};
