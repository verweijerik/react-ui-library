import React from 'react';
import cx from 'classnames';
import { CheckBox } from '../check-box/check-box';
import styles from './option-dropdown.module.less';

export const Option = ({ option, onChange }) => {
  const { key, label, selected } = option;
  return (
    <div key={key} className={styles.item}>
      <CheckBox label={label} noMargin checked={selected} onChange={onChange} />
    </div>
  );
};
