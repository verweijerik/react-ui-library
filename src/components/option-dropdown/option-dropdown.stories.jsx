import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { OptionDropdown } from './option-dropdown';

const style = {
  height: '500px',
  width: '200px',
};

storiesOf('Forms/OptionDropdown', module).add('default', () => {
  const [options, setOptions] = useState([
    { label: 'Aardvarks', value: 'aardvarks' },
    { label: 'Camels', value: 'camels' },
    { label: 'Giraffes', value: 'giraffes', selected: true },
    { label: 'Monkeys', value: 'monkeys' },
    { label: 'Wallabies', value: 'wallabies' },
  ]);
  return (
    <Container style={style}>
      <OptionDropdown
        name="example"
        label="Examples"
        options={options}
        onChange={(evt) => {
          const { value } = evt.target;
          setOptions(value);
        }}
      />
    </Container>
  );
});
