import React from 'react';
import PropTypes from 'prop-types';
import { Menu } from '../menu/menu';
import { Layer } from './layer';

export const OptionDropdown = ({ name, label, options, onChange }) => {
  return (
    <Menu
      menu={{
        label,
        trigger: 'DropDownButton',
        possibleAnchors: ['TOP_LEFT', 'BOTTOM_LEFT'],
        sections: (
          <Layer
            options={options}
            onChangeOptions={(evt, nextOptions) => {
              evt.stopPropagation();
              evt.target.name = name;
              evt.target.value = nextOptions;
              onChange(evt);
            }}
          />
        ),
      }}
    />
  );
};

OptionDropdown.defaultProps = {
  name: undefined,
};

OptionDropdown.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      selected: PropTypes.bool,
    }),
  ).isRequired,
  onChange: PropTypes.func.isRequired,
};
