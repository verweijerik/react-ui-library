import React from 'react';
import cx from 'classnames';
import { Option } from './option';
import { Divider } from '../divider/divider';
import styles from './option-dropdown.module.less';

export const Layer = ({ options, onChangeOptions }) => {
  return (
    <div className={cx('menu', styles.menu)}>
      <div className={styles.header}>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a
          onClick={(evt) => {
            const next = options.map((option) => ({
              ...option,
              selected: true,
            }));
            onChangeOptions(evt, next);
          }}
        >
          All
        </a>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a
          onClick={(evt) => {
            const next = options.map((option) => ({
              ...option,
              selected: false,
            }));
            onChangeOptions(evt, next);
          }}
        >
          Clear
        </a>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a
          onClick={(evt) => {
            const next = options.map((option) => ({
              ...option,
              selected: !option.selected,
            }));
            onChangeOptions(evt, next);
          }}
        >
          Invert
        </a>
      </div>
      <Divider margin="7px" />
      {options.map((option, index) => (
        <Option
          key={index}
          option={option}
          onChange={(evt) => {
            const { value } = option;
            const next = options.map((option) =>
              option.value !== value
                ? option
                : { ...option, selected: !option.selected },
            );
            onChangeOptions(evt, next);
          }}
        />
      ))}
    </div>
  );
};
