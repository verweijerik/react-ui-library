import React, { useReducer, useEffect } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Accordion } from '../accordion';
import { CheckBox } from '../../check-box/check-box';
import styles from './accordion-with-default-toggle.module.less';

const initialState = (initialExpanded, defaultEnabled) => ({
  accordionExpanded: initialExpanded,
  defaultEnabled,
});

const reducer = (state, action) => {
  switch (action.type) {
    case 'TOGGLE_ACCORDION': {
      return {
        ...state,
        accordionExpanded: !state.accordionExpanded,
      };
    }
    case 'TOGGLE_ENABLE': {
      return {
        accordionExpanded: state.defaultEnabled,
        defaultEnabled: !state.defaultEnabled,
      };
    }
    default:
      return state;
  }
};

export const AccordionWithDefaultToggle = ({
  heading,
  toggleLabel,
  onClickDefaultToggle,
  expanded: initialExpanded,
  defaultEnabled: initialDefaultEnabled,
  children,
  padding,
}) => {
  const [state, dispatch] = useReducer(
    reducer,
    initialState(initialExpanded, initialDefaultEnabled),
  );

  useEffect(() => {
    if (state.accordionExpanded !== initialExpanded) {
      dispatch({ type: 'TOGGLE_ACCORDION' });
    }

    if (state.defaultEnabled !== initialDefaultEnabled) {
      dispatch({ type: 'TOGGLE_ENABLE' });
    }
  }, [initialExpanded, initialDefaultEnabled]);

  const content = (
    <>
      <div className={styles.checkboxWrapper}>
        <CheckBox
          label={toggleLabel}
          onChange={(evt) => {
            dispatch({ type: 'TOGGLE_ENABLE' });
            if (onClickDefaultToggle) {
              onClickDefaultToggle(evt);
            }
          }}
          checked={state.defaultEnabled}
        />
      </div>
      {state.accordionExpanded && (
        <div className={styles.contentWrapper}>{children}</div>
      )}
    </>
  );

  return (
    <>
      <Accordion
        heading={heading}
        bordered
        expanded={state.accordionExpanded}
        squareBottom
        onClick={() => dispatch({ type: 'TOGGLE_ACCORDION' })}
      />
      <div className={cx(styles.bordered, padding ? styles.padding : '')}>
        {content}
      </div>
    </>
  );
};

AccordionWithDefaultToggle.defaultProps = {
  expanded: false,
  defaultEnabled: true,
  onClickDefaultToggle: null,
  padding: true,
};

AccordionWithDefaultToggle.propTypes = {
  heading: PropTypes.node.isRequired,
  toggleLabel: PropTypes.string.isRequired,
  expanded: PropTypes.bool,
  defaultEnabled: PropTypes.bool,
  onClickDefaultToggle: PropTypes.func,
  padding: PropTypes.bool,
};
