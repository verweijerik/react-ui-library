import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaPlus } from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Accordion } from './accordion';
import { AccordionWithDefaultToggle } from './helpers/accordion-with-default-toggle';
import { Heading } from '../heading/heading';
import { Divider } from '../divider/divider';
import { Table } from '../table/table';
import { withFluidWidth, withFluidWidthEditable } from '../table/story-data';

const style = {
  height: '500px',
  width: '400px',
};

const onIconClick = () => {
  console.log('func as prop to child');
};

storiesOf('Layout/Accordion', module)
  .add('collapsed', () => (
    <Container style={style}>
      <Accordion heading={<Heading>Heading</Heading>}>
        Example content
      </Accordion>
    </Container>
  ))
  .add('expanded', () => (
    <Container style={style}>
      <Accordion heading={<Heading>Heading</Heading>} expanded>
        Example content
      </Accordion>
    </Container>
  ))
  .add('bordered', () => (
    <Container style={style}>
      <Accordion heading={<Heading>Heading</Heading>} bordered expanded>
        Example content
      </Accordion>
    </Container>
  ))
  .add('with no padding', () => (
    <Container style={style}>
      <Accordion
        heading={<Heading>Heading</Heading>}
        bordered
        expanded
        padding={false}
      >
        Example content
      </Accordion>
    </Container>
  ))
  .add('managed', () => (
    <Container style={style}>
      <Accordion heading={<Heading>Heading</Heading>} bordered expanded managed>
        Example content
      </Accordion>
    </Container>
  ))
  .add('nested', () => {
    const [firstExpanded, setFirstExpanded] = useState(false);
    const [secondExpanded, setSecondExpanded] = useState(false);
    return (
      <Container style={style}>
        <Accordion
          heading={
            <Heading onClick={() => setFirstExpanded(!firstExpanded)}>
              First level
            </Heading>
          }
          expanded={firstExpanded}
        >
          <div>
            <p>Example content</p>
            <Accordion
              heading={
                <Heading onClick={() => setSecondExpanded(!secondExpanded)}>
                  Second level
                </Heading>
              }
              expanded={secondExpanded}
            >
              Example content
            </Accordion>
          </div>
        </Accordion>
      </Container>
    );
  })
  .add('with help icon', () => (
    <Container style={style}>
      <Accordion
        heading={
          <Heading onClickHelp={() => console.log('help!')}>Heading</Heading>
        }
        bordered
        expanded
        managed
      >
        Example content
      </Accordion>
    </Container>
  ))
  .add('with default toggle & table', () => {
    const [enabled, setEnabled] = useState(true);
    return (
      <Container style={style}>
        <AccordionWithDefaultToggle
          heading={
            <Heading onClickHelp={() => console.log('help!')}>Heading</Heading>
          }
          toggleLabel="Default Settings"
          onClickDefaultToggle={() => setEnabled(!enabled)}
          padding={false}
        >
          <Divider margin={0} />
          <Table table={enabled ? withFluidWidth : withFluidWidthEditable} />
        </AccordionWithDefaultToggle>
      </Container>
    );
  })
  .add('with icon in header', () => {
    return (
      <Container style={style}>
        <Accordion
          heading={
            <Heading onIconClick={() => onIconClick()} icon={<FaPlus />}>
              Heading
            </Heading>
          }
          bordered
          managed
          padding={false}
          rightSideIcon
        >
          Some default settings
        </Accordion>
      </Container>
    );
  });
