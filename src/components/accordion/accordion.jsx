import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Chevron } from './chevron/chevron';
import styles from './accordion.module.less';

export const AccordionManaged = ({
  heading,
  bordered,
  padding,
  expanded: initialExpanded,
  children,
}) => {
  const [expanded, toggle] = React.useState(initialExpanded);

  return (
    <>
      <Accordion
        heading={heading}
        bordered={bordered}
        padding={padding}
        expanded={expanded}
        onClick={() => {
          toggle(!expanded);
        }}
      >
        {children}
      </Accordion>
    </>
  );
};

export const AccordionBase = ({
  heading,
  expanded,
  bordered,
  padding,
  children,
  squareBottom,
  onClick,
}) => {
  return (
    <div
      className={cx(
        styles.accordion,
        bordered ? styles.bordered : '',
        expanded ? styles.expanded : '',
        squareBottom ? styles.squareBottom : '',
      )}
    >
      <div
        className={cx(styles.accordionHeader, onClick ? styles.clickable : '')}
        onClick={onClick}
      >
        <Chevron expanded={expanded} />
        {heading}
      </div>
      {expanded && children && (
        <div
          className={cx(
            styles.accordionContent,
            bordered && padding ? styles.padding : '',
          )}
        >
          {children}
        </div>
      )}
    </div>
  );
};

export const Accordion = ({
  heading,
  expanded,
  managed,
  bordered,
  padding,
  children,
  onClick,
  squareBottom,
}) =>
  managed ? (
    <AccordionManaged
      heading={heading}
      expanded={expanded}
      bordered={bordered}
      padding={padding}
      squareBottom={squareBottom}
      onClick={onClick}
    >
      {children}
    </AccordionManaged>
  ) : (
    <AccordionBase
      heading={heading}
      expanded={expanded}
      bordered={bordered}
      padding={padding}
      squareBottom={squareBottom}
      onClick={onClick}
    >
      {children}
    </AccordionBase>
  );

Accordion.defaultProps = {
  bordered: false,
  padding: true,
  managed: false,
  expanded: false,
  onClick: null,
  squareBottom: false,
};

Accordion.propTypes = {
  heading: PropTypes.node.isRequired,
  bordered: PropTypes.bool,
  padding: PropTypes.bool,
  managed: PropTypes.bool,
  expanded: PropTypes.bool,
  onClick: PropTypes.func,
  squareBottom: PropTypes.bool,
};
