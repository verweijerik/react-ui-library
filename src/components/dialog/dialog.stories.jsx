import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Dialog } from './dialog';
import { Button } from '../button/button';

const style = {
  height: '500px',
  width: '500px',
};

const content = <>This will complete the transaction</>;

const footer = (
  <>
    <Button label="Okay" inverted colored="green" onClick={() => {}} />
    <Button label="Cancel" inverted colored="red" onClick={() => {}} />
  </>
);

const dialog = {
  heading: 'Commit transaction?',
  content,
  footer,
  onClose: () => {},
};

const withFixedWidth = {
  ...dialog,
  width: '240px',
};

const withAutoWidth = {
  ...dialog,
  width: 'auto',
};

const withLongContent = {
  scroll: true,
  heading: 'Long Content With Scrollbar',
  content: (
    <div>{'Example modal content goes here lorem ipsum. '.repeat(1000)}</div>
  ),
  footer,
  onClose: () => {},
};

storiesOf('Modals/Dialog', module)
  .add('default', () => (
    <Container style={style}>
      <Dialog dialog={dialog} />
    </Container>
  ))
  .add('with fixed width', () => (
    <Container style={style}>
      <Dialog dialog={withFixedWidth} />
    </Container>
  ))
  .add('with auto width', () => (
    <Container style={style}>
      <Dialog dialog={withAutoWidth} />
    </Container>
  ))
  .add('with long content and scrollbar', () => (
    <Container style={style}>
      <Dialog dialog={withLongContent} />
    </Container>
  ));
