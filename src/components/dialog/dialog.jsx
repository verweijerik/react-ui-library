import React from 'react';
import PropTypes from 'prop-types';
import { FaTimes } from 'react-icons/fa';
import cx from 'classnames';
import styles from './dialog.module.less';

export const Dialog = ({ dialog }) => {
  const {
    heading,
    content,
    contentPadding,
    footer,
    scroll,
    width,
    onClose,
  } = dialog;
  return (
    <div
      className={cx(
        styles.dialog,
        width ? styles.inline : null,
        scroll ? styles.scroll : null,
      )}
      style={{ width }}
    >
      <div className={styles.header}>
        <h1 className={styles.title}>{heading}</h1>
        <div className={styles.dismiss}>
          <FaTimes onClick={onClose} />
        </div>
      </div>
      <div className={styles.content} style={{ padding: contentPadding || 20 }}>
        {content}
      </div>
      <div className={styles.footer}>{footer}</div>
    </div>
  );
};

const dialogShape = PropTypes.shape({
  heading: PropTypes.string,
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]).isRequired,
  contentPadding: PropTypes.string,
  footer: PropTypes.node,
  width: PropTypes.string,
  bordered: PropTypes.bool,
  onClose: PropTypes.func,
  scroll: PropTypes.bool,
});

Dialog.propTypes = {
  dialog: dialogShape.isRequired,
};
