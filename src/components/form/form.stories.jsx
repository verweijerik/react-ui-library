import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import {
  Field,
  Input,
  Select,
  CheckBox,
  ButtonGroup,
  Button,
  Toggle,
  InputGroup,
  InputGroupAddon,
  Slider,
  TextArea,
  RadioButton,
  Spacer,
} from '../../..';
import { FormRow } from '../layout/form-row/form-row';

const menu = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
];

storiesOf('Forms/Layout', module)
  .add('default (stacked)', () => (
    <Container>
      <Field label="Input" helpText="Some help text">
        <Input value="Value" width="100%" />
      </Field>
      <Field label="TextArea">
        <TextArea value="Value" />
      </Field>
      <Field label="Select (native)">
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={{ label: 'Option 2', value: '2' }}
          width="100%"
          native
        />
      </Field>
      <Field label="Select (custom)">
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={{ label: 'Option 2', value: '2' }}
          width="100%"
        />
      </Field>
      <Field label="Select (multi)">
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          width="100%"
        />
      </Field>
      <Field label="Slider">
        <Slider
          name="example"
          label="Number of aardvarks"
          value={35}
          min={0}
          max={100}
          step={1}
          marks={[
            { value: 20, label: 'S1...', tooltip: 'S1 Production' },
            { value: 40 },
            { value: 60, label: <strong>60 (best)</strong> },
            { value: 80, label: 'S4...', tooltip: 'S4 Shut-in' },
            { value: 100, label: 'S5...', tooltip: 'S5 Circulate' },
          ]}
          onChange={() => {}}
          showArrows
          showTooltip
        />
      </Field>
      <Field label="ButtonGroup">
        <ButtonGroup items={menu} value={menu[0].key} />
      </Field>
      <Field label="InputGroup">
        <InputGroup>
          <InputGroupAddon>$</InputGroupAddon>
          <Input name="example" value="123" onChange={() => {}} size={4} />
          <InputGroupAddon>each</InputGroupAddon>
        </InputGroup>
      </Field>
      <Field label="CheckBox">
        <CheckBox label="CheckBox" checked />
      </Field>
      <Field label="RadioButton">
        <RadioButton
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Monkeys', value: 'bananas' },
          ]}
          value={{ label: 'Monkeys', value: 'bananas' }}
          inline
        />
      </Field>
      <Field label="Toggle">
        <Toggle label="Toggle" checked onChange={() => {}} />
      </Field>
      <Field>
        <Button colored label="Button" onClick={() => {}} />
      </Field>
    </Container>
  ))
  .add('row', () => (
    <Container>
      <FormRow>
        <Field label="Input" helpText="Some help text">
          <Input value="Value" width="200px" />
        </Field>
        <Field label="Select (native)">
          <Select
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="auto"
            native
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (custom)">
          <Select
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="200px"
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (multi)">
          <Select
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            width="250px"
            onChange={() => {}}
          />
        </Field>
        <Field label="Slider">
          <Slider
            width={150}
            name="example"
            value={35}
            min={0}
            max={100}
            step={1}
            onChange={() => {}}
            showArrows
            showTooltip
          />
        </Field>
        <Field label="ButtonGroup">
          <ButtonGroup items={menu} value={menu[0].key} />
        </Field>
        <Field label="InputGroup">
          <InputGroup>
            <InputGroupAddon>$</InputGroupAddon>
            <Input name="example" value="123" onChange={() => {}} size={4} />
            <InputGroupAddon>each</InputGroupAddon>
          </InputGroup>
        </Field>
        <Field label="CheckBox">
          <CheckBox label="CheckBox" checked />
        </Field>
        <Field label="RadioButton">
          <RadioButton
            options={[
              { label: 'Aardvarks', value: 'termites' },
              { label: 'Monkeys', value: 'bananas' },
            ]}
            value={{ label: 'Monkeys', value: 'bananas' }}
            inline
          />
        </Field>
        <Field label="Toggle">
          <Toggle label="Toggle" checked onChange={() => {}} />
        </Field>
        <Field>
          <Button colored label="Button" onClick={() => {}} />
        </Field>
      </FormRow>
      <Spacer />
      <FormRow>
        <Field label="Input" helpText="Some help text">
          <Input small value="Value" width="200px" />
        </Field>
        <Field label="Select (native)">
          <Select
            small
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="auto"
            native
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (custom)">
          <Select
            small
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="200px"
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (multi)">
          <Select
            small
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            width="250px"
            onChange={() => {}}
          />
        </Field>
        <Field label="ButtonGroup">
          <ButtonGroup small items={menu} value={menu[0].key} />
        </Field>
        <Field label="InputGroup">
          <InputGroup small>
            <InputGroupAddon>$</InputGroupAddon>
            <Input name="example" value="123" onChange={() => {}} size={4} />
            <InputGroupAddon>each</InputGroupAddon>
          </InputGroup>
        </Field>
        <Field>
          <CheckBox small label="CheckBox" checked />
        </Field>
        <Field label="RadioButton">
          <RadioButton
            small
            options={[
              { label: 'Aardvarks', value: 'termites' },
              { label: 'Monkeys', value: 'bananas' },
            ]}
            value={{ label: 'Monkeys', value: 'bananas' }}
            inline
          />
        </Field>
        <Field>
          <Toggle small label="Toggle" checked onChange={() => {}} />
        </Field>
        <Field>
          <Button small colored label="Button" onClick={() => {}} />
        </Field>
      </FormRow>
    </Container>
  ))
  .add('labels left (stacked)', () => (
    <Container>
      <Field
        label="Input"
        labelLeft
        labelWidth="100px"
        helpText="Some help text"
      >
        <Input value="Value" width="100%" />
      </Field>
      <Field label="TextArea" labelLeft labelWidth="100px">
        <TextArea value="Value" />
      </Field>
      <Field label="Select (native)" labelLeft labelWidth="100px">
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={{ label: 'Option 1', value: '1' }}
          width="100%"
          native
          onChange={() => {}}
        />
      </Field>
      <Field label="Select (custom)" labelLeft labelWidth="100px">
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={{ label: 'Option 1', value: '1' }}
          width="100%"
          onChange={() => {}}
        />
      </Field>
      <Field label="Select (multi)" labelLeft labelWidth="100px">
        <Select
          options={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          value={[
            { label: 'Option 1', value: '1' },
            { label: 'Option 2', value: '2' },
          ]}
          width="100%"
          onChange={() => {}}
        />
      </Field>
      <Field label="Slider" labelLeft labelWidth="100px">
        <Slider
          name="example"
          label="Number of aardvarks"
          value={35}
          min={0}
          max={100}
          step={1}
          onChange={() => {}}
          showArrows
          showTooltip
        />
      </Field>
      <Field label="ButtonGroup Long Label" labelLeft labelWidth="100px">
        <ButtonGroup items={menu} value={menu[0].key} />
      </Field>
      <Field label="InputGroup" labelLeft labelWidth="100px">
        <InputGroup>
          <InputGroupAddon>$</InputGroupAddon>
          <Input name="example" value="123" onChange={() => {}} size={4} />
          <InputGroupAddon>each</InputGroupAddon>
        </InputGroup>
      </Field>
      <Field label="CheckBox" labelLeft labelWidth="100px">
        <CheckBox label="CheckBox" checked />
      </Field>
      <Field label="RadioButton" labelLeft labelWidth="100px">
        <RadioButton
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Monkeys', value: 'bananas' },
          ]}
          value={{ label: 'Monkeys', value: 'bananas' }}
          inline
        />
      </Field>
      <Field label="Toggle" labelLeft labelWidth="100px">
        <Toggle label="Toggle" checked onChange={() => {}} />
      </Field>
      <Field>
        <Button colored label="Button" onClick={() => {}} />
      </Field>
    </Container>
  ))
  .add('labels left (row)', () => (
    <Container>
      <FormRow>
        <Field label="Input" labelLeft helpText="Some help text">
          <Input value="Value" width="100%" />
        </Field>
        <Field label="Select (native)" labelLeft>
          <Select
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="auto"
            native
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (custom)" labelLeft>
          <Select
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="150px"
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (multi)" labelLeft>
          <Select
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            width="250px"
            onChange={() => {}}
          />
        </Field>
        <Field label="Slider" labelLeft>
          <Slider
            name="example"
            width="100px"
            value={35}
            min={0}
            max={100}
            step={1}
            onChange={() => {}}
            showTooltip
          />
        </Field>
        <Field label="ButtonGroup" labelLeft>
          <ButtonGroup items={menu} value={menu[0].key} />
        </Field>
        <Field label="InputGroup" labelLeft>
          <InputGroup>
            <InputGroupAddon>$</InputGroupAddon>
            <Input name="example" value="123" onChange={() => {}} size={4} />
            <InputGroupAddon>each</InputGroupAddon>
          </InputGroup>
        </Field>
        <Field label="CheckBox" labelLeft>
          <CheckBox label="CheckBox" checked />
        </Field>
        <Field label="RadioButton" labelLeft>
          <RadioButton
            options={[
              { label: 'Aardvarks', value: 'termites' },
              { label: 'Monkeys', value: 'bananas' },
            ]}
            value={{ label: 'Monkeys', value: 'bananas' }}
            inline
          />
        </Field>
        <Field label="Toggle" labelLeft>
          <Toggle label="Toggle" checked onChange={() => {}} />
        </Field>
        <Field>
          <Button colored label="Button" onClick={() => {}} />
        </Field>
      </FormRow>
      <Spacer />
      <FormRow>
        <Field label="Input" labelLeft helpText="Some help text">
          <Input small value="Value" width="200px" />
        </Field>
        <Field label="Select (native)" labelLeft>
          <Select
            small
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="auto"
            native
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (custom)" labelLeft>
          <Select
            small
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={{ label: 'Option 1', value: '1' }}
            width="200px"
            onChange={() => {}}
          />
        </Field>
        <Field label="Select (multi)" labelLeft>
          <Select
            small
            options={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            value={[
              { label: 'Option 1', value: '1' },
              { label: 'Option 2', value: '2' },
            ]}
            width="250px"
            onChange={() => {}}
          />
        </Field>
        <Field label="ButtonGroup" labelLeft>
          <ButtonGroup small items={menu} value={menu[0].key} />
        </Field>
        <Field label="InputGroup" labelLeft>
          <InputGroup small>
            <InputGroupAddon>$</InputGroupAddon>
            <Input name="example" value="123" onChange={() => {}} size={4} />
            <InputGroupAddon>each</InputGroupAddon>
          </InputGroup>
        </Field>
        <Field>
          <CheckBox small label="CheckBox" checked />
        </Field>
        <Field label="RadioButton" labelLeft>
          <RadioButton
            small
            options={[
              { label: 'Aardvarks', value: 'termites' },
              { label: 'Monkeys', value: 'bananas' },
            ]}
            value={{ label: 'Monkeys', value: 'bananas' }}
            inline
          />
        </Field>
        <Field>
          <Toggle small label="Toggle" checked onChange={() => {}} />
        </Field>
        <Field>
          <Button small colored label="Button" onClick={() => {}} />
        </Field>
      </FormRow>
    </Container>
  ))
  .add('in display: grid (test)', () => (
    <Container style={{ width: 300, background: '#eee' }}>
      <div
        style={{
          display: 'grid',
          width: '100%',
          gridTemplateColumns: 'repeat(3, auto)',
          gap: 2,
        }}
      >
        <Field label="Kickoff depth">
          <Input right width="100%" />
        </Field>
        <Field label="Param 1 (DLS1)">
          <Input right width="100%" />
        </Field>
        <Field label="Param 2 (DLS2)">
          <Input right width="100%" />
        </Field>
      </div>
    </Container>
  ));
