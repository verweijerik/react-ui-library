import React from 'react';
import { FaRegQuestionCircle } from 'react-icons/fa';
import { Tooltip } from '../tooltip/tooltip';
import styles from './form.module.less';

export const Label = ({ label, width, helpText }) => {
  return (
    <div className={styles.label}>
      <label style={{ width: width || '' }}>
        {label}
        {helpText && (
          <Tooltip text={helpText}>
            <FaRegQuestionCircle color="#999" />
          </Tooltip>
        )}
      </label>
    </div>
  );
};
