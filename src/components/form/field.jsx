import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { Label } from './label';
import styles from './form.module.less';

export const Field = ({ label, labelLeft, labelWidth, children, helpText }) => {
  return (
    <div className={cx(styles.field, labelLeft ? styles.labelLeft : '')}>
      {label && <Label label={label} width={labelWidth} helpText={helpText} />}
      <div className={styles.fieldInput}>{children}</div>
    </div>
  );
};

Field.defaultProps = {
  label: null,
  helpText: null,
  labelLeft: false,
  labelWidth: 'auto',
};

Field.propTypes = {
  label: PropTypes.string,
  helpText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  labelLeft: PropTypes.bool,
  labelWidth: PropTypes.string,
  children: PropTypes.node.isRequired,
};
