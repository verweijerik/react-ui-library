import React, { Fragment, isValidElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './input-group.module.less';

const isDOMTypeElement = (element) => {
  return isValidElement(element) && typeof element.type === 'string';
};

export const InputGroup = ({ children, small, width }) => {
  const childElements =
    children instanceof Array
      ? children.filter((c) => isValidElement(c))
      : [children];
  const elements = childElements.map((item, index) => {
    const isDOMType = isDOMTypeElement(item);
    const { groupOrder: groupOrderProp } = !isDOMType
      ? item.props
      : { groupOrder: null };
    const count = childElements.length;
    const isFirst = index === 0;
    const isLast = index === count - 1;
    const isOnly = !count || count === 1;
    const groupOrder =
      groupOrderProp ||
      (isOnly ? null : isFirst ? 'first' : isLast ? 'last' : 'middle');
    return isDOMType ? (
      item
    ) : (
      <Fragment key={index}>
        {React.cloneElement(item, { groupOrder, small })}
      </Fragment>
    );
  });
  return (
    <div className={cx(styles.inputGroup)} style={{ width }}>
      {elements}
    </div>
  );
};

InputGroup.defaultProps = {
  small: false,
  width: '100%',
};

InputGroup.propTypes = {
  children: PropTypes.node.isRequired,
  small: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
