import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './input-group-addon.module.less';

export const InputGroupAddon = ({ children, groupOrder, small }) => {
  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case 'first':
          return styles.groupOrderFirst;
        case 'last':
          return styles.groupOrderLast;
        default:
          return styles.groupOrderMiddle;
      }
    }
    return '';
  })();
  return (
    <span className={cx(styles.addon, order, small ? styles.small : '')}>
      {children}
    </span>
  );
};

InputGroupAddon.defaultProps = {
  groupOrder: null,
  small: false,
};

InputGroupAddon.propTypes = {
  children: PropTypes.node.isRequired,
  groupOrder: PropTypes.string,
  small: PropTypes.bool,
};
