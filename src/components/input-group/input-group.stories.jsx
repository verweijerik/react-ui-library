import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { InputGroup } from './input-group';
import { InputGroupAddon } from './input-group-addon/input-group-addon';
import { Input } from '../input/input';
import { Button } from '../button/button';
import { Menu } from '../menu/menu';
import { Select } from '../select/select';

const style = {
  height: '500px',
  width: '500px',
};

const sectionStyle = {
  marginBottom: 40,
};

storiesOf('Forms/InputGroup', module)
  .add('with Addon', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <InputGroup>
          <Input width="100%" name="example" value="123" onChange={() => {}} />
          <InputGroupAddon>km</InputGroupAddon>
        </InputGroup>
      </div>
      <div style={sectionStyle}>
        <InputGroup>
          <InputGroupAddon>$</InputGroupAddon>
          <Input width="100%" name="example" value="123" onChange={() => {}} />
          <InputGroupAddon>each</InputGroupAddon>
        </InputGroup>
      </div>
    </Container>
  ))
  .add('with Button', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <InputGroup>
          <Input
            width="100%"
            name="example"
            value="Example"
            onChange={() => {}}
          />
          <Button name="example" label="Button" onClick={() => {}} />
        </InputGroup>
      </div>
      <div style={sectionStyle}>
        <InputGroup>
          <Input
            width="100%"
            name="example"
            value="Lorem"
            onChange={() => {}}
          />
          <Input
            width="100%"
            name="example"
            value="Ipsum"
            onChange={() => {}}
          />
          <Button name="example" label="Settings" onClick={() => {}} />
          <Button name="example" label="Confirm" colored onClick={() => {}} />
        </InputGroup>
      </div>
    </Container>
  ))
  .add('with Menu', () => (
    <Container style={style}>
      <InputGroup>
        <Input width="100%" name="example" value="123" onChange={() => {}} />
        <Menu
          menu={{
            label: 'E-06/degC',
            trigger: 'DropDownButton',
            sections: [
              {
                type: 'Option',
                label: '403.5433',
                description: 'ft',
                inline: true,
                onClick: () => {},
              },
              {
                type: 'Option',
                label: '0.123',
                description: 'km',
                inline: true,
                onClick: () => {},
              },
              {
                type: 'Option',
                label: '4842.4608',
                description: 'in',
                inline: true,
                onClick: () => {},
              },
            ],
          }}
        />
      </InputGroup>
    </Container>
  ))
  .add('with Select', () => (
    <Container style={style}>
      <InputGroup>
        <InputGroupAddon>addon</InputGroupAddon>
        <Select
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={() => {}}
          value={{ label: 'Monkeys', value: 'bananas' }}
          width="200px"
        />
        <Button name="example" label="Confirm" colored onClick={() => {}} />
      </InputGroup>
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <InputGroup small>
        <InputGroupAddon>$</InputGroupAddon>
        <Input width="100%" name="example" value="123" onChange={() => {}} />
        <Select
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Kangaroos', value: 'grass' },
            { label: 'Monkeys', value: 'bananas' },
            { label: 'Possums', value: 'slugs' },
          ]}
          onChange={() => {}}
          value={{ label: 'Monkeys', value: 'bananas' }}
          width="auto"
        />
        <Button name="example" label="Confirm" colored onClick={() => {}} />
      </InputGroup>
    </Container>
  ))
  .add('fixed width', () => (
    <Container style={style}>
      <InputGroup width="200px">
        <InputGroupAddon>$</InputGroupAddon>
        <Input
          size="1"
          width="100%"
          name="example"
          value="123"
          onChange={() => {}}
        />
        <Button name="example" label="Confirm" colored onClick={() => {}} />
      </InputGroup>
    </Container>
  ))
  .add('with single element', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <InputGroup>
          <Input width="100%" name="example" value="123" onChange={() => {}} />
        </InputGroup>
      </div>
      <div style={sectionStyle}>
        <InputGroup>
          <Button name="example" label="Confirm" colored onClick={() => {}} />
        </InputGroup>
      </div>
      <div style={sectionStyle}>
        <InputGroup>
          <Select
            options={[
              { label: 'Aardvarks', value: 'termites' },
              { label: 'Kangaroos', value: 'grass' },
              { label: 'Monkeys', value: 'bananas' },
              { label: 'Possums', value: 'slugs' },
            ]}
            onChange={() => {}}
            value={{ label: 'Monkeys', value: 'bananas' }}
            width="auto"
          />
        </InputGroup>
      </div>
      <div style={sectionStyle}>
        <InputGroup>
          <Menu
            menu={{
              label: 'E-06/degC',
              trigger: 'DropDownButton',
              sections: [
                {
                  type: 'Option',
                  label: '403.5433',
                  description: 'ft',
                  inline: true,
                  onClick: () => {},
                },
                {
                  type: 'Option',
                  label: '0.123',
                  description: 'km',
                  inline: true,
                  onClick: () => {},
                },
                {
                  type: 'Option',
                  label: '4842.4608',
                  description: 'in',
                  inline: true,
                  onClick: () => {},
                },
              ],
            }}
          />
        </InputGroup>
      </div>
    </Container>
  ))
  .add('Conditional elements & manual groupOrder', () => (
    <Container style={style}>
      <InputGroup>
        {false && (
          <Input width="100%" name="example" value="123" onChange={() => {}} />
        )}
        <Input
          width="100%"
          name="example"
          value="123"
          tooltip="Foo"
          onChange={() => {}}
          groupOrder="first"
        />
        <Input
          width="100%"
          name="example"
          value="123"
          tooltip="Foo"
          onChange={() => {}}
        />
        <Input
          width="100%"
          name="example"
          value="123"
          tooltip="Foo"
          onChange={() => {}}
        />
        {false ? (
          <Input width="100%" name="example" value="123" onChange={() => {}} />
        ) : null}
      </InputGroup>
    </Container>
  ));
