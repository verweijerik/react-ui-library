import React from 'react';
import { storiesOf } from '@storybook/react';
import { FaTrash } from 'react-icons/fa';
import { Button } from './button';
import { Container } from '../../helpers/container';
import svgIcon from '../../images/icons/icons8-junction.svg';

const style = {
  height: '500px',
  width: '200px',
};

const sectionStyle = {
  marginBottom: 40,
};

const deprecatedMessage =
  'This usage of icons is deprecated. Use react-icons instead.';

storiesOf('Forms/Button', module)
  .add('default', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} />
      </div>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} colored />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
        />
      </div>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} basic />
      </div>
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} disabled />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored
          disabled
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          disabled
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          disabled
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          basic
          disabled
        />
      </div>
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} small />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored
          small
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          small
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          small
        />
      </div>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} basic small />
      </div>
    </Container>
  ))
  .add('with title', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          title="button title"
        />
      </div>
    </Container>
  ))
  .add('icon', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          icon={<FaTrash />}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored
          icon={<FaTrash />}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          icon={<FaTrash />}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          icon={<FaTrash />}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          basic
          icon={<FaTrash />}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          basic
          colored="muted"
          icon={<FaTrash />}
        />
      </div>
    </Container>
  ))
  .add('icon only', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button name="example" onClick={() => {}} icon={<FaTrash />} />
      </div>
      <div style={sectionStyle}>
        <Button name="example" onClick={() => {}} colored icon={<FaTrash />} />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          onClick={() => {}}
          colored="red"
          icon={<FaTrash />}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          onClick={() => {}}
          colored="green"
          icon={<FaTrash />}
        />
      </div>
      <div style={sectionStyle}>
        <Button name="example" onClick={() => {}} basic icon={<FaTrash />} />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          onClick={() => {}}
          basic
          colored="muted"
          icon={<FaTrash />}
        />
      </div>
    </Container>
  ))
  .add('pill', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} pill />
      </div>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} colored pill />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          pill
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          pill
        />
      </div>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} basic pill />
      </div>
    </Container>
  ))
  .add('round', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button name="example" label="MM" onClick={() => {}} round />
      </div>
      <div style={sectionStyle}>
        <Button name="example" label="MM" onClick={() => {}} colored round />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="MM"
          onClick={() => {}}
          colored="red"
          round
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="MM"
          onClick={() => {}}
          colored="green"
          round
        />
      </div>
      <div style={sectionStyle}>
        <Button name="example" label="MM" onClick={() => {}} basic round />
      </div>
    </Container>
  ))
  .add('inverted', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} inverted />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored
          inverted
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          inverted
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          inverted
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          basic
          inverted
        />
      </div>
    </Container>
  ))
  .add('with spinner', () => (
    <div style={{ display: 'flex' }}>
      <Container style={{ ...style }}>
        <div style={sectionStyle}>
          <Button name="example" onClick={() => {}} loading />
        </div>
        <div style={sectionStyle}>
          <Button name="example" onClick={() => {}} loading small />
        </div>
        <div style={sectionStyle}>
          <Button name="example" onClick={() => {}} colored loading />
        </div>
        <div style={sectionStyle}>
          <Button
            name="example"
            onClick={() => {}}
            colored="red"
            loading
            width="200px"
          />
        </div>
        <div style={sectionStyle}>
          <Button
            name="example"
            onClick={() => {}}
            colored="green"
            loading
            pill
          />
        </div>
        <div style={sectionStyle}>
          <Button name="example" onClick={() => {}} basic loading />
        </div>
      </Container>
      <Container style={style}>
        <div style={sectionStyle}>
          <Button
            name="example"
            label="Calculating..."
            onClick={() => {}}
            loading
          />
        </div>
        <div style={sectionStyle}>
          <Button
            name="example"
            label="Calculating..."
            onClick={() => {}}
            loading
            small
          />
        </div>
        <div style={sectionStyle}>
          <Button
            name="example"
            label="Calculating..."
            onClick={() => {}}
            colored
            loading
          />
        </div>
        <div style={sectionStyle}>
          <Button
            name="example"
            label="Calculating..."
            onClick={() => {}}
            colored="red"
            loading
          />
        </div>
        <div style={sectionStyle}>
          <Button
            name="example"
            label="Calculating..."
            onClick={() => {}}
            colored="green"
            loading
          />
        </div>
        <div style={sectionStyle}>
          <Button
            name="example"
            label="Calculating..."
            onClick={() => {}}
            basic
            loading
          />
        </div>
      </Container>
    </div>
  ))
  .add('with fixed width', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          width="200px"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored
          width="200px"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          width="200px"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          width="200px"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          basic
          width="200px"
        />
      </div>
    </Container>
  ))
  .add('with custom SVG icons', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          icon={svgIcon}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored
          icon={svgIcon}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          icon={svgIcon}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          icon={svgIcon}
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          basic
          icon={svgIcon}
        />
      </div>
    </Container>
  ))
  .add('with legacy icons (deprecated)', () => (
    <Container style={style} deprecatedMessage={deprecatedMessage}>
      <div style={sectionStyle}>
        <Button name="example" label="Button" onClick={() => {}} icon="trash" />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored
          icon="trash"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="red"
          icon="trash"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          colored="green"
          icon="trash"
        />
      </div>
      <div style={sectionStyle}>
        <Button
          name="example"
          label="Button"
          onClick={() => {}}
          basic
          icon="trash"
        />
      </div>
    </Container>
  ));
