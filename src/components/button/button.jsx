import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import buttonStyles from './button.module.less';
import { Icon } from '../icon/icon';
import { Spinner } from '../spinner/spinner';

export const Button = ({
  active,
  basic,
  colored,
  disabled,
  groupOrder,
  icon,
  inverted,
  label,
  loading,
  name,
  onClick,
  pill,
  round,
  small,
  width,
  title,
  styles,
}) => {
  const color = (() => {
    if (colored) {
      switch (colored) {
        case 'danger':
        case 'red':
          return buttonStyles.red;
        case 'success':
        case 'green':
          return buttonStyles.green;
        case 'muted':
          return buttonStyles.muted;
        default:
          return buttonStyles.orange;
      }
    }
    return '';
  })();

  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case 'first':
          return buttonStyles.groupOrderFirst;
        case 'last':
          return buttonStyles.groupOrderLast;
        default:
          return buttonStyles.groupOrderMiddle;
      }
    }
    return '';
  })();

  return (
    <button
      type="button"
      className={cx(
        buttonStyles.button,
        active ? buttonStyles.active : '',
        basic ? buttonStyles.basic : '',
        color,
        disabled ? buttonStyles.disabled : '',
        (icon || loading) && !label ? buttonStyles.iconOnly : '',
        inverted ? buttonStyles.inverted : '',
        order,
        pill ? buttonStyles.pill : '',
        round ? buttonStyles.round : '',
        small ? buttonStyles.small : '',
        styles,
      )}
      name={name}
      onClick={onClick}
      {...(title ? { title } : {})} // eslint-disable-line react/jsx-props-no-spreading
      style={{ width }}
    >
      {icon && (
        <span className={buttonStyles.icon}>
          <Icon icon={icon} />
        </span>
      )}
      {loading && (
        <span className={buttonStyles.icon}>
          <Spinner
            small={!small /*regular button use small spinner*/}
            tiny={small /*small button use tiny spinner*/}
            dark={!colored && !basic}
            colored={basic}
          />
        </span>
      )}
      {label}
    </button>
  );
};

Button.defaultProps = {
  active: false,
  basic: false,
  colored: false,
  disabled: false,
  groupOrder: null,
  icon: null,
  label: '',
  loading: false,
  name: undefined,
  pill: false,
  round: false,
  small: false,
  styles: '',
  width: '',
  title: '',
  onClick: () => {},
};

Button.propTypes = {
  active: PropTypes.bool,
  basic: PropTypes.bool,
  colored: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  disabled: PropTypes.bool,
  icon: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string, //deprecated
  ]),
  groupOrder: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  title: PropTypes.string,
  loading: PropTypes.bool,
  name: PropTypes.string,
  onClick: PropTypes.func,
  pill: PropTypes.bool,
  round: PropTypes.bool,
  small: PropTypes.bool,
  styles: PropTypes.string,
  width: PropTypes.string,
};
