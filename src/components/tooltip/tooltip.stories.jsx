import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { TooltipLayer } from './tooltip-layer';
import { Tooltip } from './tooltip';
import { Spacer } from '../../..';

const style = {
  height: '500px',
  width: '500px',
};

storiesOf('Basic/Tooltip', module)
  .add('default', () => (
    <Container style={style}>
      <Tooltip text="Tooltip text">
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('with max width (multiline)', () => (
    <Container style={style}>
      <Tooltip
        text="Example of long tooltip with multiple lines"
        maxWidth="100px"
      >
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('with JSX', () => (
    <Container style={style}>
      <Tooltip
        text={
          <div>
            Select from options:
            <ul style={{ padding: 0, margin: 0, listStylePosition: 'inside' }}>
              <li>One</li>
              <li>Two</li>
            </ul>
          </div>
        }
      >
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('error', () => (
    <Container style={style}>
      <Tooltip text="Tooltip text" error>
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('inside overflow:hidden container', () => (
    <Container style={{ overflow: 'hidden', border: '1px solid black' }}>
      <Tooltip text="Tooltip text">Hover over me</Tooltip>
    </Container>
  ))
  .add('disabled', () => (
    <Container>
      <Tooltip enabled={false}>Hover over me</Tooltip>
    </Container>
  ))
  .add('warning', () => (
    <Container>
      <Tooltip text="Tooltip text" warning>
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('custom placement', () => (
    <Container>
      <Tooltip text="Tooltip text" anchor="LEFT_BOTTOM">
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('custom offset', () => (
    <Container>
      <Tooltip text="Tooltip text" triggerOffset={30}>
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('custom font size', () => (
    <Container>
      <Tooltip text="Tooltip text" fontSize={18}>
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('custom padding', () => (
    <Container>
      <Tooltip text="Tooltip text" padding="10px 20px">
        <span>Hover over me</span>
      </Tooltip>
    </Container>
  ))
  .add('display property', () => (
    <Container warningMessage="To avoid layout issues, the tooltip should match the display property of its children (i.e. inline for inline triggers, block for block, etc)">
      <Tooltip text="Tooltip text">Inline (default)</Tooltip>
      <Spacer />
      <Tooltip text="Tooltip text" display="inline-block">
        <div style={{ display: 'inline-block', border: '1px solid #DDD' }}>
          Inline block
        </div>
      </Tooltip>
      <Spacer />
      <Tooltip text="Tooltip text" display="block">
        <div style={{ border: '1px solid #DDD' }}>Block</div>
      </Tooltip>
    </Container>
  ))
  .add('base tooltip layer (internal)', () => (
    <Container>
      <div style={{ height: '50px' }}>
        <TooltipLayer text="Tooltip text" />
      </div>
      <div style={{ height: '50px' }}>
        <TooltipLayer
          text={<div style={{ color: 'orange' }}>Custom tooltip text</div>}
        />
      </div>
    </Container>
  ));
