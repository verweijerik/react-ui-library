import React from 'react';
import PropTypes from 'prop-types';
import { Arrow } from 'react-laag';
import cx from 'classnames';
import styles from './tooltip.module.less';

export const TooltipLayer = ({
  text,
  error,
  warning,
  padding,
  maxWidth,
  fontSize,
  layerProps,
  arrowStyle,
  layerSide,
}) => {
  return (
    <div
      className={cx(
        styles.tooltip,
        error ? styles.error : warning ? styles.warning : null,
      )}
      style={{ padding }}
      {...layerProps} // eslint-disable-line react/jsx-props-no-spreading
    >
      <div style={{ maxWidth, fontSize, padding }}>{text}</div>
      <Arrow
        style={arrowStyle}
        layerSide={layerSide}
        backgroundColor={error ? '#9f3a38' : warning ? '#a07800' : '#333'}
        size={6}
      />
    </div>
  );
};

TooltipLayer.defaultProps = {
  text: '',
  error: false,
  warning: false,
  maxWidth: 'none',
  fontSize: 'inherit',
  padding: '4px 8px',
  layerProps: {
    style: {
      position: 'fixed',
    },
  },
  layerSide: 'top',
  arrowStyle: {
    top: '100%',
    bottom: null,
    left: '50%',
    right: null,
    position: 'absolute',
  },
};

TooltipLayer.propTypes = {
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  error: PropTypes.bool,
  warning: PropTypes.bool,
  fontSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  padding: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  maxWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  layerSide: PropTypes.string,
  layerProps: PropTypes.object,
  arrowStyle: PropTypes.object,
};
