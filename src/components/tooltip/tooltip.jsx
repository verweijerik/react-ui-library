import React, { isValidElement } from 'react';
import PropTypes from 'prop-types';
import ResizeObserver from 'resize-observer-polyfill';
import { useTooltip } from 'react-laag';
import { TooltipLayer } from './tooltip-layer';

export const Tooltip = ({
  children,
  text,
  error,
  anchor,
  possibleAnchors,
  enabled,
  warning,
  maxWidth,
  triggerOffset,
  fontSize,
  padding,
  display,
  flex,
}) => {
  const visible = enabled && (isValidElement(text) || text.length > 0);
  const [element, triggerProps] = visible
    ? useTooltip(
        ({ isOpen, layerProps, arrowStyle, layerSide }) =>
          isOpen && (
            <TooltipLayer
              text={text}
              error={error}
              warning={warning}
              padding={padding}
              maxWidth={maxWidth}
              fontSize={fontSize}
              layerProps={layerProps}
              arrowStyle={arrowStyle}
              layerSide={layerSide}
            />
          ),
        {
          delayEnter: 100,
          delayLeave: 100,
          fixed: true,
          placement: {
            anchor,
            possibleAnchors,
            autoAdjust: true,
            triggerOffset,
          },
          ResizeObserver,
        },
      )
    : [];
  return (
    <>
      {visible && element}
      <span
        {...triggerProps} // eslint-disable-line react/jsx-props-no-spreading
        style={{ display, flex }}
      >
        {children}
      </span>
    </>
  );
};

Tooltip.defaultProps = {
  text: '',
  enabled: true,
  error: false,
  warning: false,
  anchor: 'TOP_CENTER',
  maxWidth: 'none',
  triggerOffset: 4,
  possibleAnchors: [
    'TOP_CENTER',
    'LEFT_CENTER',
    'BOTTOM_CENTER',
    'RIGHT_CENTER',
  ],
  fontSize: 'inherit',
  padding: '4px 8px',
  display: 'inline',
  flex: 'none',
};

Tooltip.propTypes = {
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  error: PropTypes.bool,
  warning: PropTypes.bool,
  anchor: PropTypes.string,
  enabled: PropTypes.bool,
  maxWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  triggerOffset: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  possibleAnchors: PropTypes.array,
  fontSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  padding: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  display: PropTypes.string,
  flex: PropTypes.string,
};
