import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Button } from '../button/button';
import { toast } from './toaster';

const style = {
  height: '500px',
  width: '200px',
};

storiesOf('Modals/Toaster', module)
  .add('managed', () => {
    return (
      <Container style={style}>
        <Button
          name="example"
          label="Show Toast"
          onClick={() =>
            toast({
              message: {
                type: 'Error',
                content: 'An error has happened',
                details: 'Bad input data',
              },
              onClose: () => console.log('toast closed'),
            })
          }
        />
      </Container>
    );
  })
  .add('prevent duplicates', () => {
    //pass unique id to prevent duplicates
    return (
      <Container style={style}>
        <Button
          name="example"
          label="Show Toast"
          onClick={() =>
            toast({
              message: {
                type: 'Error',
                content: 'An error has happened',
                details: 'Bad input data',
              },
              id: 'uniqueId',
              onClose: () => console.log('toast closed'),
            })
          }
        />
      </Container>
    );
  })
  .add('static examples', () => {
    setTimeout(() => {
      toast({
        message: {
          type: 'Success',
          content: 'Great results',
        },
        autoClose: false,
      });
      toast({
        message: {
          heading: 'Info heading',
          content: 'Here is some info',
          type: 'Info',
        },
        autoClose: false,
      });
      toast({
        message: {
          icon: true,
          content: 'Warning message',
          type: 'Warning',
        },
        autoClose: false,
      });
      toast({
        message: {
          heading: 'Error heading',
          content: 'An error has happened',
          details: 'Bad input data',
          icon: true,
          type: 'Error',
        },
        id: 'foobar',
        autoClose: false,
      });
      const id = toast({
        message: {
          heading: 'Error heading',
          content: 'An error has happened',
          details: 'Bad input data',
          icon: true,
          type: 'Error',
        },
        id: 'foobar', //duplicate IDs won't be shown twice
        autoClose: false,
      });
    }, 0);
    return (
      <Container style={style}>
        <div />
      </Container>
    );
  });
