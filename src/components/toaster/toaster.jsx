import React from 'react';
import { toast as toastBase, Slide } from 'react-toastify';
import { Message } from '../message/message';
import { Dismiss } from '../message/dismiss';
import './toaster.less';

export { ToastContainer as Toaster } from 'react-toastify';

export const toast = ({ id, message, autoClose = 5000, onClose }) => {
  const { type } = message;

  const CloseButton = (
    { closeToast }, //Dismiss button that looks like the one in Message
  ) => <Dismiss type={type} onClose={closeToast} isInToast />;

  const content = (
    <Message
      message={{
        ...message,
        visible: true,
        width: '100%',
        withDismiss: false,
      }}
    />
  );

  return toastBase(content, {
    toastId: id,
    autoClose,
    onClose,
    hideProgressBar: true,
    closeOnClick: false,
    pauseOnHover: true,
    draggable: false,
    closeButton: CloseButton,
    transition: Slide,
  });
};
