import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './badge.module.less';

const isDark = (hexColor) => {
  // https://stackoverflow.com/a/12043228/942635
  const c = hexColor.substring(1); // strip #
  const rgb = parseInt(c, 16); // convert RRGGBB to decimal
  /* eslint-disable no-bitwise*/
  const r = (rgb >> 16) & 0xff; // extract red
  const g = (rgb >> 8) & 0xff; // extract green
  const b = (rgb >> 0) & 0xff; // extract blue
  /* eslint-enable no-bitwise*/
  const luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
  return luma < 128;
};

export const Badge = ({ children, color, title, dot }) => {
  const visible = !(title === null && dot === null);
  return (
    <div className={styles.wrapper}>
      {visible && (
        <sup
          className={cx(
            styles.badge,
            isDark(color) ? styles.dark : styles.light,
            title !== null ? styles.label : styles.dot,
            children ? styles.hasChildren : '',
          )}
          style={{ background: color || undefined }}
        >
          {title}
        </sup>
      )}
      {children}
    </div>
  );
};

Badge.defaultProps = {
  children: null,
  color: '',
  title: null,
  dot: null,
};

Badge.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  dot: PropTypes.bool,
};
