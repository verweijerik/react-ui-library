import React from 'react';
import { storiesOf } from '@storybook/react';
import { FaBell } from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Badge } from './badge.jsx';
import { Icon } from '../icon/icon.jsx';
import { Button } from '../button/button.jsx';

const sectionStyle = {
  marginBottom: 40,
};

storiesOf('Basic/Badge', module)
  .add('count', () => (
    <Container>
      <div style={sectionStyle}>
        <Badge title="3">
          <Icon size={24} icon={<FaBell />} clickable />
        </Badge>
      </div>
      <div style={sectionStyle}>
        <Badge title={0}>
          <Button name="example" label="Button" onClick={() => {}} />
        </Badge>
      </div>
    </Container>
  ))
  .add('dot', () => (
    <Container>
      <div style={sectionStyle}>
        <Badge dot>
          <Icon size={24} icon={<FaBell />} clickable />
        </Badge>
      </div>
      <div style={sectionStyle}>
        <Badge dot>
          <Button name="example" label="Button" onClick={() => {}} />
        </Badge>
      </div>
    </Container>
  ))
  .add('standalone', () => (
    <Container>
      <div style={sectionStyle}>
        <Badge title="3" />
      </div>
    </Container>
  ))
  .add('not visible', () => (
    <Container>
      <div style={sectionStyle}>
        <Badge>
          <Icon size={24} icon={<FaBell />} clickable />
        </Badge>
      </div>
    </Container>
  ));
