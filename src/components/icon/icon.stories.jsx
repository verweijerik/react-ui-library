import React from 'react';
import { storiesOf } from '@storybook/react';
import { FaAngleDoubleRight } from 'react-icons/fa';
import svgIcon from '../../images/icons/icons8-junction.svg';
import pngIcon from '../../images/icons/icon-drop.png';
import { Container } from '../../helpers/container';
import { Icon } from './icon';
import styles from './icons.example.module.less';

const style = {
  height: '500px',
  width: '200px',
};

const sectionStyle = {
  marginBottom: 20,
};

storiesOf('Basic/Icon', module)
  .add('default', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <h4>
          <a href="https://react-icons.netlify.com/">React Icons</a>{' '}
          (recommended):
        </h4>
        <Icon icon={<FaAngleDoubleRight />} />
      </div>
      <div style={sectionStyle}>
        <h4>SVG (for custom icons):</h4>
        <Icon icon={svgIcon} />
      </div>
      <div style={sectionStyle}>
        <h4>PNG icon (not recommended):</h4>
        <Icon icon={pngIcon} />
      </div>
      <div style={sectionStyle}>
        <h4>String names (deprecated):</h4>
        <Icon icon="arrow left" />
      </div>
    </Container>
  ))
  .add('clickable', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <h4>
          <a href="https://react-icons.netlify.com/">React Icons</a>{' '}
          (recommended):
        </h4>
        <Icon icon={<FaAngleDoubleRight />} clickable />
      </div>
      <div style={sectionStyle}>
        <h4>SVG (for custom icons):</h4>
        <Icon icon={svgIcon} clickable />
      </div>
      <div style={sectionStyle}>
        <h4>PNG icon (not recommended):</h4>
        <Icon icon={pngIcon} clickable />
      </div>
      <div style={sectionStyle}>
        <h4>String names (deprecated):</h4>
        <Icon icon="arrow left" clickable />
      </div>
    </Container>
  ))
  .add('inline', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <h4>
          <a href="https://react-icons.netlify.com/">React Icons</a>{' '}
          (recommended):
        </h4>
        <span className={styles.middleAlignedInline}>
          Do you need help? <Icon icon={<FaAngleDoubleRight />} />
        </span>
      </div>
      <div style={sectionStyle}>
        <h4>SVG (for custom icons):</h4>
        <span className={styles.middleAlignedInline}>
          Do you need help? <Icon icon={svgIcon} />
        </span>
      </div>
      <div style={sectionStyle}>
        <h4>SVG (for custom icons):</h4>
        <span className={styles.middleAlignedInline}>
          Do you need help? <Icon icon={pngIcon} />
        </span>
      </div>
      <div style={sectionStyle}>
        <h4>String names (deprecated):</h4>
        <span className={styles.middleAlignedInline}>
          Do you need help? <Icon icon="arrow left" />
        </span>
      </div>
    </Container>
  ))
  .add('colored', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <h4>
          <a href="https://react-icons.netlify.com/">React Icons</a>{' '}
          (recommended):
        </h4>
        <Icon icon={<FaAngleDoubleRight />} color="purple" />
      </div>
      <div style={sectionStyle}>
        <h4>SVG (for custom icons):</h4>
        <Icon icon={svgIcon} color="purple" />
      </div>
      <div style={sectionStyle}>
        <h4>String names (deprecated):</h4>
        <Icon icon="arrow left" color="purple" />
      </div>
    </Container>
  ))
  .add('sized', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <h4>
          <a href="https://react-icons.netlify.com/">React Icons</a>{' '}
          (recommended):
        </h4>
        <Icon icon={<FaAngleDoubleRight />} size="100px" />
      </div>
      <div style={sectionStyle}>
        <h4>SVG (for custom icons):</h4>
        <Icon icon={svgIcon} size="100px" />
      </div>
      <div style={sectionStyle}>
        <h4>PNG icon (not recommended):</h4>
        <Icon icon={pngIcon} size="100px" />
      </div>
      <div style={sectionStyle}>
        <h4>String names (deprecated):</h4>
        <Icon icon="arrow left" size="100px" />
      </div>
    </Container>
  ));
