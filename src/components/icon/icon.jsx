import React, { isValidElement } from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import { ReactSVG } from 'react-svg';
import { DeprecatedIcon } from './deprecated-icon';
import styles from './icon.module.less';

/*
  Icon wrapper component to support different icon formats:
    - react-icons (default recommendation)
    - SVG (for special cases)
    - deprecated string names (for backwards compatibility with Semantic UI)
 */

export const Icon = ({ icon, clickable, color, size }) => {
  return (
    <div
      className={cx(styles.wrapper, clickable ? styles.clickable : '')}
      style={{
        color,
        fill: color,
        fontSize: size || 'inherit',
        width: size || 'auto',
        height: size || 'auto',
      }}
    >
      {isValidElement(icon) ? (
        icon //react-icon
      ) : typeof icon === 'string' && icon.includes('.svg') ? (
        <ReactSVG
          className={styles.customSvg}
          beforeInjection={(svg) => {
            if (size) {
              svg.setAttribute('width', size);
              svg.setAttribute('height', size);
            }
          }}
          src={icon}
        /> //SVG
      ) : typeof icon === 'string' && icon.includes('.png') ? (
        <img alt="icon" className={styles.customPng} src={icon} />
      ) : typeof icon === 'string' ? (
        <DeprecatedIcon icon={icon} /> //deprecated Semantic UI string name
      ) : null}
    </div>
  );
};

Icon.defaultProps = {
  clickable: false,
};

Icon.propTypes = {
  icon: PropTypes.oneOfType([
    PropTypes.node, //JSX (react-icons)
    PropTypes.string, //SVG (or deprecated string name)
  ]).isRequired,
  clickable: PropTypes.bool,
};
