import React from 'react';
import {
  FaArrowDown,
  FaArrowLeft,
  FaArrowRight,
  FaArrowUp,
  FaChartLine,
  FaCheck,
  FaChevronDown,
  FaChevronUp,
  FaClone,
  FaEdit,
  FaEye,
  FaEyeSlash,
  FaFileImport, // vertically flipped reply (used in import context)
  FaMinus,
  FaPencilAlt, //pencil
  FaPlus,
  FaQuestion,
  FaRegCircle, // for not found
  FaRegCopy, // copy outline
  FaSortAmountDown,
  FaSortAmountUp,
  FaStar,
  FaSync,
  FaTable,
  FaTimes,
  FaTrash,
  FaUndo,
} from 'react-icons/fa';

export const DeprecatedIcon = ({ icon }) => {
  switch (icon) {
    case 'arrow down':
      return <FaArrowDown />;
    case 'arrow left':
      return <FaArrowLeft />;
    case 'arrow right':
      return <FaArrowRight />;
    case 'arrow up':
      return <FaArrowUp />;
    case 'chart line':
      return <FaChartLine />;
    case 'check':
    case 'checkmark':
      return <FaCheck />;
    case 'close':
    case 'times':
      return <FaTimes />;
    case 'question':
      return <FaQuestion />;
    case 'sort amount down':
      return <FaSortAmountDown />;
    case 'table':
      return <FaTable />;
    case 'trash':
      return <FaTrash />;
    case 'minus':
      return <FaMinus />;
    case 'add':
    case 'plus':
      return <FaPlus />;
    case 'pencil':
      return <FaPencilAlt />;
    case 'edit':
    case 'edit outline':
      return <FaEdit />;
    case 'chevron up':
      return <FaChevronUp />;
    case 'chevron down':
      return <FaChevronDown />;
    case 'undo':
      return <FaUndo />;
    case 'eye':
      return <FaEye />;
    case 'eye slash':
      return <FaEyeSlash />;
    case 'star':
      return <FaStar />;
    case 'clone':
    case 'orange clone':
      return <FaClone />;
    case 'sort amount up':
      return <FaSortAmountUp />;
    case 'copy':
    case 'copy outline':
      return <FaRegCopy />;
    case 'vertically flipped reply':
      return <FaFileImport />;
    case 'refresh':
      return <FaSync />;
    case '':
      return null;
    default:
      return <FaRegCircle />; //just a default icon so it's obvious when missing
  }
};
