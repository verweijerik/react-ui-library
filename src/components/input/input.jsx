import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './input.module.less';
import { Tooltip } from '../tooltip/tooltip';
import { isStringNumberOrNode } from '../../helpers/types';

/*
  Gets the width when you want to set it based on the number of characters
  Replacement for input size property which is hard to style
*/
export const widthOfCharacters = (characterCount) => {
  const padding = '15px';
  const multiplier = '0.675em';
  return `calc((${characterCount} * ${multiplier}) + (2 * ${padding}))`;
};

export const Input = ({
  error,
  tooltip,
  isInTable,
  width: propWidth,
  small,
  onChange,
  size,
  placeholder,
  value,
  onKeyPress,
  onFocus,
  onBlur,
  name,
  type,
  tabIndex,
  disabled,
  right,
  warning,
  groupOrder,
  maxTooltipWidth,
}) => {
  const order = (() => {
    if (groupOrder) {
      switch (groupOrder) {
        case 'first':
          return styles.groupOrderFirst;
        case 'last':
          return styles.groupOrderLast;
        default:
          return styles.groupOrderMiddle;
      }
    }
    return '';
  })();

  const width = propWidth || (size ? 'auto' : '100%');

  return (
    <div style={{ width }} className={order}>
      <Tooltip
        error={!!error}
        warning={!!warning}
        text={tooltip || error || warning}
        enabled={
          (tooltip && isStringNumberOrNode(tooltip)) ||
          (error && isStringNumberOrNode(error)) ||
          (warning && isStringNumberOrNode(warning)) ||
          false
        }
        maxWidth={maxTooltipWidth}
        display="block"
      >
        <input
          type={type}
          size={size || 20}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          onKeyPress={onKeyPress}
          onFocus={onFocus}
          onBlur={onBlur}
          name={name}
          tabIndex={tabIndex}
          autoComplete="off"
          disabled={disabled}
          className={cx(
            styles.input,
            error ? styles.error : warning ? styles.warning : '',
            right ? styles.right : '',
            small ? styles.small : '',
            isInTable ? styles.isInTable : '',
          )}
          style={{ width }}
        />
      </Tooltip>
    </div>
  );
};

Input.defaultProps = {
  error: null,
  warning: null,
  tooltip: null,
  name: undefined,
  type: 'text',
  onChange: () => {},
  onKeyPress: () => {},
  onFocus: () => {},
  onBlur: () => {},
  placeholder: '',
  small: false,
  tabIndex: 0,
  value: '',
  disabled: false,
  right: false,
  groupOrder: null,
  maxTooltipWidth: undefined,
  width: undefined,
  //deprecated props
  size: null, //use width instead
};

Input.propTypes = {
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  warning: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  tooltip: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  name: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  small: PropTypes.bool,
  placeholder: PropTypes.string,
  tabIndex: PropTypes.number,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  disabled: PropTypes.bool,
  right: PropTypes.bool,
  groupOrder: PropTypes.string,
  maxTooltipWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  //deprecated props:
  size: PropTypes.number, //use width instead
};
