import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Input, widthOfCharacters } from './input.jsx';
import { Spacer } from '../layout/spacer/spacer';
import { Message } from '../message/message';

const isNumeric = (v) => !isNaN(parseFloat(v)) && isFinite(v);

const style = {
  height: '500px',
  width: '200px',
};

storiesOf('Forms/Input', module)
  .add('default', () => (
    <Container style={style}>
      <Input name="example" onChange={() => {}} />
    </Container>
  ))
  .add('with value', () => (
    <Container style={style}>
      <Input name="example" value="Example" onChange={() => {}} />
    </Container>
  ))
  .add('with placeholder', () => (
    <Container style={style}>
      <Input
        name="example"
        placeholder="Example placeholder"
        onChange={() => {}}
      />
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <Input name="example" value="Example" small onChange={() => {}} />
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style}>
      <Input name="example" value="Example" disabled onChange={() => {}} />
    </Container>
  ))
  .add('with different types', () => (
    <Container style={style}>
      <Input name="example" value="Text" type="text" onChange={() => {}} />
      <Spacer />
      <Message
        message={{
          visible: true,
          content:
            'onChange always returns string type according to HTML spec (not a bug)',
          type: 'Warning',
        }}
      />
      <Spacer />
      <Input name="example" value="123" type="number" onChange={() => {}} />
      <Spacer />
      <Input
        name="example"
        value="2020-05-22"
        type="date"
        onChange={() => {}}
      />
    </Container>
  ))
  .add('width', () => (
    <Container style={style}>
      <Input name="example" value={12} onChange={() => {}} width="50px" />
    </Container>
  ))
  .add('size (deprecated)', () => {
    return (
      <Container
        style={{ ...style }}
        deprecatedMessage="Deprecated - use width instead"
      >
        <Input size={3} name="example" value="123" onChange={() => {}} />
        <Spacer />
        <p>Replace with:</p>
        <Input
          width={widthOfCharacters(3)}
          name="example"
          value="123"
          onChange={() => {}}
        />
      </Container>
    );
  })
  .add('with tooltip', () => (
    <Container style={style}>
      <Input
        name="example"
        value="Example"
        tooltip="Some info"
        onChange={() => {}}
      />
    </Container>
  ))
  .add('with error', () => (
    <Container style={style}>
      <Input
        name="example"
        value="Bad Input"
        error="Error message goes here"
        onChange={() => {}}
      />
    </Container>
  ))
  .add('with warning', () => (
    <Container style={{ ...style }}>
      <Input
        name="example"
        value="Warned Input"
        warning="Warning message goes here"
        onChange={() => {}}
      />
    </Container>
  ))
  .add('right', () => (
    <Container style={{ ...style }}>
      <Input right name="example" value="Value" onChange={() => {}} />
    </Container>
  ))
  .add('managed', () => {
    const [value, setValue] = useState('type here');
    return (
      <Container style={style}>
        <Input
          error={isNumeric(value) ? "Can't be numeric" : null}
          name="example"
          value={value}
          onChange={(evt) => setValue(evt.target.value)}
        />
      </Container>
    );
  });
