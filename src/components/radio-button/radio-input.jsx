import React from 'react';
import cx from 'classnames';
import styles from './radio-button.module.less';

export const RadioInput = ({
  name,
  label,
  value,
  selected,
  disabled,
  small,
  onChange,
  noMargin,
}) => {
  return (
    <div
      className={cx(
        styles.radio,
        disabled ? styles.disabled : null,
        small ? styles.small : null,
        noMargin ? styles.noMargin : null,
      )}
      onClick={onChange}
    >
      <input
        type="radio"
        /*
          Don't set the name property. In certain circumstances (including
          duplicated names), it seems confuse React into behaving like an
          uncontrolled input. We've seen a bug where sometimes the input state
          wouldn't update visually upon click. If we change this decision, we
          need to understand why this happens.
          https://stackoverflow.com/questions/63032125
        */
        //name={name} //name must be unique
        value={value}
        checked={selected}
        onChange={() => {}}
        disabled={disabled}
      />
      <label
        //data attributes are for deprecated variant
        data-name={name}
        data-value={value}
      >
        {label}
      </label>
    </div>
  );
};
