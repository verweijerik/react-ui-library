import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { standardizeInputs } from '../select/select.input';
import { RadioInput } from './radio-input';
import styles from './radio-button.module.less';

export const RadioButton = ({
  name,
  label: rawLabel,
  options: rawOptions,
  value: rawValue,
  onChange,
  inline,
  disabled,
  small,
  //deprecated props:
  mainLabel,
  onClick,
  radioButtonsData,
  classForContainer,
  noMargin,
}) => {
  //----------------------------------------------------------------------------
  // Deprecation / backwards-compatibility
  const isDeprecated = radioButtonsData !== undefined;
  const { simpleInputs, options, selectedOptions: value } = isDeprecated
    ? {
        simpleInputs: false,
        options: radioButtonsData,
        selectedOptions: undefined,
      }
    : standardizeInputs(rawOptions, rawValue);
  const selectedValue =
    isDeprecated || value === undefined || value === null
      ? undefined
      : simpleInputs
      ? value
      : value.value;
  const label = isDeprecated ? mainLabel : rawLabel;
  //----------------------------------------------------------------------------

  return (
    <div
      className={cx(
        styles.wrapper,
        inline ? styles.inline : null,
        classForContainer === 'inline fields' ? styles.inline : null, //deprecated
      )}
    >
      {mainLabel && (
        <label className={cx(inline && styles.labelMargin)}>{label}</label>
      )}
      {options.map((option, index) => {
        const selected =
          option.checked || option.value === selectedValue || false;
        return (
          <RadioInput
            key={index}
            name={option.name || option.value} //name must be unique
            label={option.label}
            value={option.value}
            selected={selected}
            disabled={disabled || option.disabled}
            small={small}
            noMargin={noMargin}
            onChange={
              !option.disabled
                ? (evt) => {
                    if (isDeprecated) {
                      onClick(evt.target);
                    } else {
                      evt.target.name = name;
                      evt.target.value = option.value;
                      evt.target.label = option.label;
                      onChange(evt);
                    }
                  }
                : null
            }
          />
        );
      })}
    </div>
  );
};

RadioButton.defaultProps = {
  name: undefined,
  noMargin: false,
  inline: false,
  mainLabel: '',
  label: '',
  disabled: false,
  small: false,
  onClick: () => {},
  onChange: () => {},
  classForContainer: 'grouped fields', // Deprecated
};

const RadioButtonShapeDeprecated = {
  radioButtonsData: PropTypes.arrayOf(PropTypes.object).isRequired,
  inline: PropTypes.bool,
  mainLabel: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  classForContainer: PropTypes.string, // Deprecated
};

export const radioOptionShape = PropTypes.shape({
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
});

const RadioButtonShape = {
  name: PropTypes.string,
  noMargin: PropTypes.bool,
  label: PropTypes.string,
  options: PropTypes.arrayOf(radioOptionShape).isRequired,
  value: radioOptionShape,
  inline: PropTypes.bool,
  disabled: PropTypes.bool,
  small: PropTypes.bool,
  onChange: PropTypes.func,
};

RadioButton.propTypes = PropTypes.oneOfType([
  PropTypes.shape(RadioButtonShape), //only use this for new usages
  PropTypes.shape(RadioButtonShapeDeprecated), //deprecated Select interface
]).isRequired;
