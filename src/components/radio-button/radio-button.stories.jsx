import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { RadioButton } from './radio-button';
import { Field, Input, Spacer } from '../../..';

const style = {
  height: '500px',
  width: '500px',
};

storiesOf('Forms/RadioButton', module)
  .add('vertical', () => (
    <Container style={style}>
      <RadioButton
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
        value={{ label: 'Monkeys', value: 'bananas' }}
        onChange={() => {}}
      />
    </Container>
  ))
  .add('horizontal', () => (
    <Container style={style}>
      <RadioButton
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
        value={{ label: 'Monkeys', value: 'bananas' }}
        onChange={() => {}}
        inline
        label="Main Label"
      />
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <RadioButton
        name="example"
        small
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
        value={{ label: 'Monkeys', value: 'bananas' }}
        onChange={() => {}}
        inline
        label="Main Label"
      />
    </Container>
  ))
  .add('disabled (all)', () => (
    <Container style={style}>
      <RadioButton
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
        value={{ label: 'Monkeys', value: 'bananas' }}
        onChange={() => {}}
        disabled
      />
    </Container>
  ))
  .add('disabled (individual)', () => (
    <Container style={style}>
      <RadioButton
        name="example"
        options={[
          { value: 'Aardvarks', label: 'termites' },
          { label: 'Kangaroos', value: 'grass' },
          { label: 'Monkeys', value: 'bananas', disabled: true },
        ]}
        value={{ label: 'Monkeys', value: 'bananas' }}
        onChange={() => {}}
      />
    </Container>
  ))
  .add('unselected', () => (
    <Container style={style}>
      <RadioButton
        name="example"
        options={[
          { label: 'Aardvarks', value: 'termites' },
          { label: 'Monkeys', value: 'bananas' },
        ]}
        value={null}
        onChange={() => {}}
      />
    </Container>
  ))
  .add('managed', () => {
    const [value, setValue] = useState({ value: 'termites' });
    return (
      <Container style={style}>
        <RadioButton
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Monkeys', value: 'bananas' },
          ]}
          value={value}
          onChange={(evt) => {
            const { name, value, label } = evt.target;
            console.log(name);
            console.log(value);
            console.log(label);
            setValue({ value });
          }}
        />
      </Container>
    );
  })
  .add('managed (deprecated)', () => {
    const [options, setOptions] = useState([
      {
        name: 'animals',
        label: 'Aardvarks',
        value: 'termites',
        checked: 'checked',
      },
      { name: 'animals', label: 'Monkeys', value: 'bananas' },
    ]);
    return (
      <Container style={style}>
        <RadioButton
          radioButtonsData={options}
          onClick={(evt) => {
            const { name, value } = evt.dataset;
            console.log(name);
            console.log(value);
            setOptions(
              options.map((o) => ({ ...o, checked: o.value === value })),
            );
          }}
        />
      </Container>
    );
  })
  .add('noMargin', () => (
    <Container
      style={style}
      warningMessage={`
        RadioButtons have top and bottom margins by default to line up with other form elements. When these margins are unwanted - for example when they're combined with another input in the same field – you can use the noMargin prop to remove them.
      `}
    >
      <Field label="Label">
        <RadioButton
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Monkeys', value: 'bananas' },
          ]}
          value={{ label: 'Monkeys', value: 'bananas' }}
          onChange={() => {}}
          inline
          noMargin
          label="Main Label"
        />
      </Field>
      <Spacer />
      <Field label="Label">
        <RadioButton
          name="example"
          options={[
            { label: 'Aardvarks', value: 'termites' },
            { label: 'Monkeys', value: 'bananas' },
          ]}
          value={{ label: 'Monkeys', value: 'bananas' }}
          onChange={() => {}}
          inline
          noMargin
          label="Main Label"
        />
        <Spacer height="10px" />
        <Input />
      </Field>
    </Container>
  ))
  .add('horizontal (deprecated)', () => (
    <Container style={style}>
      <RadioButton
        name="example"
        radioButtonsData={[
          {
            name: 'frequency',
            value: 'once_a_week',
            label: 'Once a week',
            checked: 'checked',
          },
          { name: 'frequency', value: 'twice_a_week', label: 'Twice a week' },
        ]}
        onClick={() => {}}
        classForContainer="inline fields"
      />
    </Container>
  ));
