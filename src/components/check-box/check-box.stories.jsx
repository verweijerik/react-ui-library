import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { CheckBox } from './check-box';
import { Field, Input, Spacer } from '../../..';

const style = {
  height: '500px',
  width: '200px',
};

const sectionStyle = {
  marginBottom: 30,
};

storiesOf('Forms/CheckBox', module)
  .add('default', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <CheckBox name="example" label="Label" onChange={() => {}} />
      </div>
      <div style={sectionStyle}>
        <CheckBox name="example" label="Label" checked onChange={() => {}} />
      </div>
    </Container>
  ))
  .add('no label', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <CheckBox name="example" onChange={() => {}} />
      </div>
      <div style={sectionStyle}>
        <CheckBox name="example" checked onChange={() => {}} />
      </div>
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <CheckBox name="example" label="Label" onChange={() => {}} disabled />
      </div>
      <div style={sectionStyle}>
        <CheckBox
          name="example"
          label="Label"
          checked
          onChange={() => {}}
          disabled
        />
      </div>
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <CheckBox small name="example" label="Label" onChange={() => {}} />
      </div>
      <div style={sectionStyle}>
        <CheckBox
          small
          name="example"
          label="Label"
          checked
          onChange={() => {}}
        />
      </div>
    </Container>
  ))
  .add('noMargin', () => (
    <Container
      style={style}
      warningMessage={`
        CheckBox has top and bottom margins by default to line up with other form elements. When these margins are unwanted - for example when it's combined with another input in the same field – you can use the noMargin prop to remove them.
      `}
    >
      <Field label="Label">
        <CheckBox name="example" label="Label" onChange={() => {}} noMargin />
      </Field>
      <Spacer />
      <Field label="Label">
        <CheckBox name="example" label="Label" onChange={() => {}} noMargin />
        <Spacer height="10px" />
        <Input />
      </Field>
    </Container>
  ))
  .add('managed', () => {
    const [value, setValue] = useState(false);
    return (
      <Container style={style}>
        <CheckBox
          name="example"
          label="Click label"
          checked={value}
          onChange={() => setValue(!value)}
        />
      </Container>
    );
  });
