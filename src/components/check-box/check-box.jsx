import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { FaCheck } from 'react-icons/fa';
import styles from './check-box.module.less';

export const CheckBox = ({
  noMargin,
  dataix,
  isInTable,
  tabIndex,
  checked,
  name,
  key,
  label,
  disabled,
  small,
  onChange,
}) => {
  return (
    <div
      className={cx(
        styles.checkbox,
        noMargin ? styles.noMargin : null,
        isInTable ? styles.isInTable : null,
        disabled ? styles.disabled : null,
        small ? styles.small : null,
        !label ? styles.noLabel : null,
      )}
      data-ix={dataix}
      onClick={(evt) => {
        evt.target.name = name;
        evt.target.value = !checked;
        evt.target.checked = !checked;
        onChange(evt);
      }}
    >
      <input
        type="checkbox"
        tabIndex={tabIndex}
        checked={checked}
        name={name}
        key={key}
        onChange={() => {}}
        disabled={disabled}
      />
      <label htmlFor={name}>
        <FaCheck className={styles.checkmark} />
        {label}
      </label>
    </div>
  );
};

CheckBox.defaultProps = {
  checked: false,
  isInTable: false,
  label: '',
  name: undefined,
  noMargin: false,
  onChange: () => {},
  tabIndex: 0,
  disabled: false,
  small: false,
  key: '',
  dataix: 0,
};

CheckBox.propTypes = {
  checked: PropTypes.bool,
  isInTable: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  noMargin: PropTypes.bool,
  onChange: PropTypes.func,
  tabIndex: PropTypes.number,
  disabled: PropTypes.bool,
  small: PropTypes.bool,
  key: PropTypes.string, //deprecated
  dataix: PropTypes.number, //deprecated (used in GenericTable)
};
