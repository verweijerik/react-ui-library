import React, { isValidElement, useState } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa';
import { Button } from '../button/button';
import styles from './drawer.module.less';

export const Drawer = ({
  background,
  children,
  fixed,
  open: openProp,
  right,
  shadow,
  top,
  width,
  closedWidth,
  button,
  buttonAnimate,
  buttonPosition,
}) => {
  const isStandardButton = button === true;
  const isCustomButton = !isStandardButton && isValidElement(button);
  const [open, setOpen] = isStandardButton
    ? useState(openProp)
    : [openProp, null];
  return (
    <div
      className={cx(
        styles.drawer,
        shadow ? styles.shadow : '',
        fixed ? styles.fixed : styles.inline,
        right ? styles.right : styles.left,
      )}
      style={{
        width: open ? width : closedWidth,
        top,
      }}
    >
      <div className={styles.drawerContent} style={{ background }}>
        <div style={{ width }}>{children}</div>
      </div>

      {button && (
        <span
          className={cx(
            styles.toggleButton,
            open && (isStandardButton || (isCustomButton && buttonAnimate))
              ? styles.toggleButtonOpen
              : '',
            buttonPosition === 'top' ? styles.top : styles.bottom,
          )}
        >
          {isCustomButton ? (
            button
          ) : (
            <Button
              onClick={setOpen ? () => setOpen(!open) : null}
              round
              icon={right ? <FaChevronRight /> : <FaChevronLeft />}
            />
          )}
        </span>
      )}
    </div>
  );
};

Drawer.defaultProps = {
  background: 'transparent',
  fixed: false,
  open: false,
  right: false,
  width: 400,
  closedWidth: 0,
  shadow: false,
  top: 0,
  button: null,
  buttonAnimate: true,
  buttonPosition: 'bottom',
};

Drawer.propTypes = {
  background: PropTypes.string,
  fixed: PropTypes.bool,
  open: PropTypes.bool,
  right: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  closedWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.node.isRequired,
  shadow: PropTypes.bool,
  top: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  //button
  button: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
  buttonAnimate: PropTypes.bool,
  buttonPosition: PropTypes.oneOf(['top', 'bottom']),
};
