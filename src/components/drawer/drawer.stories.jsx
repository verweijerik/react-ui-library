import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import {
  FaArrowLeft,
  FaTimes,
  FaQuestion,
  FaChevronLeft,
} from 'react-icons/fa';
import { Drawer } from './drawer';
import { Button } from '../button/button';
import { List } from '../list/list';
import { Menu } from '../menu/menu';
import { list, labelledList, withHeadings } from '../list/list.stories';
import { Container } from '../../helpers/container';
import { withSubMenus } from '../menu/menu.stories';

const inlineContainerStyle = {
  height: '500px',
  background: '#f5f5f5',
  border: '1px solid #ddd',
  display: 'flex',
  position: 'relative',
};

const inlineContentStyle = {
  flexGrow: 1,
  padding: 20,
};

storiesOf('Layout/Drawer', module)
  .add('default', () => {
    return (
      <Container style={inlineContainerStyle}>
        <Drawer background="#eee" button>
          <p>Example content</p>
        </Drawer>
        <div style={inlineContentStyle}>Placeholder page content</div>
      </Container>
    );
  })
  .add('with shadow', () => {
    return (
      <Container style={inlineContainerStyle}>
        <Drawer background="#eee" button shadow>
          <p>Example content</p>
        </Drawer>
        <div style={inlineContentStyle}>Placeholder page content</div>
      </Container>
    );
  })
  .add('start open', () => {
    return (
      <Container style={inlineContainerStyle}>
        <Drawer background="#eee" button open>
          <p>Example content</p>
        </Drawer>
        <div style={inlineContentStyle}>Placeholder page content</div>
      </Container>
    );
  })
  .add('right-aligned', () => {
    return (
      <Container style={inlineContainerStyle}>
        <div style={inlineContentStyle}>Placeholder page content</div>
        <Drawer right button background="#eee">
          <p>Example content</p>
        </Drawer>
      </Container>
    );
  })
  .add('button position', () => {
    return (
      <Container style={inlineContainerStyle}>
        <div style={inlineContentStyle}>Placeholder page content</div>
        <Drawer button background="#eee" buttonPosition="top">
          <p>Example content</p>
        </Drawer>
      </Container>
    );
  })
  .add('with width', () => {
    return (
      <Container style={inlineContainerStyle}>
        <Drawer background="#eee" width="50vw" button>
          <p>Example content</p>
        </Drawer>
        <div style={inlineContentStyle}>Placeholder page content</div>
      </Container>
    );
  })
  .add('with closed width', () => {
    return (
      <Container style={inlineContainerStyle}>
        <Drawer background="#eee" closedWidth={100} button>
          <p>Example content</p>
        </Drawer>
        <div style={inlineContentStyle}>Placeholder page content</div>
      </Container>
    );
  })
  .add('with custom button', () => {
    const [openLeft, setOpenLeft] = useState(false);
    const [openRight, setOpenRight] = useState(false);
    return (
      <Container style={inlineContainerStyle}>
        <div style={inlineContentStyle}>Placeholder page content</div>
        <Drawer
          open={openLeft}
          background="#eee"
          buttonType="custom"
          button={
            <Button
              onClick={() => setOpenLeft(!openLeft)}
              colored
              round
              icon={<FaArrowLeft />}
            />
          }
          buttonAnimate
        >
          <p>Example content</p>
        </Drawer>
        <Drawer
          right
          open={openRight}
          background="#eee"
          button={
            <Button
              onClick={() => setOpenRight(!openRight)}
              colored
              round
              icon={<FaTimes />}
            />
          }
          buttonAnimate={false}
        >
          <p>Example content</p>
        </Drawer>
      </Container>
    );
  })
  .add('wrapping multiple lists', () => {
    const [open, setOpen] = useState(true);
    return (
      <Container style={inlineContainerStyle}>
        <Drawer
          button={
            <Button
              onClick={setOpen ? () => setOpen(!open) : null}
              round
              icon={<FaChevronLeft />}
            />
          }
          open={open}
          background="#f5f7f9"
          closedWidth={50}
        >
          <List drawer list={labelledList} narrow={!open} marginBottom={10} />
          <List drawer list={withHeadings} narrow={!open} marginBottom={10} />
          <List drawer list={list} narrow={!open} marginBottom={10} />
        </Drawer>
        <div style={{ borderLeft: '1px solid #ddd' }}></div>
      </Container>
    );
  })
  .add('inline', () => {
    const [openLeft, setOpenLeft] = useState(false);
    const [openRight, setOpenRight] = useState(false);
    return (
      <Container style={inlineContainerStyle}>
        <div style={inlineContentStyle}>
          <Button label="Left Drawer" onClick={() => setOpenLeft(!openLeft)} />
          <Button
            label="Right Drawer"
            onClick={() => setOpenRight(!openRight)}
          />
        </div>
        <Drawer open={openLeft} background="#eee">
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenLeft(false)} />
        </Drawer>
        <Drawer right open={openRight} background="#eee">
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenRight(false)} />
        </Drawer>
      </Container>
    );
  })
  .add('inline with closed width', () => {
    const [openLeft, setOpenLeft] = useState(false);
    const [openRight, setOpenRight] = useState(false);
    return (
      <Container style={inlineContainerStyle}>
        <div style={inlineContentStyle}>
          <Button label="Left Drawer" onClick={() => setOpenLeft(!openLeft)} />
          <Button
            label="Right Drawer"
            onClick={() => setOpenRight(!openRight)}
          />
        </div>
        <Drawer open={openLeft} background="#eee" closedWidth={50}>
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenLeft(false)} />
        </Drawer>
        <Drawer right open={openRight} background="#eee" closedWidth={50}>
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenRight(false)} />
        </Drawer>
      </Container>
    );
  })
  .add('fixed', () => {
    const [openLeft, setOpenLeft] = useState(false);
    const [openRight, setOpenRight] = useState(false);
    return (
      <Container>
        <Button label="Left Drawer" onClick={() => setOpenLeft(!openLeft)} />
        <Button label="Right Drawer" onClick={() => setOpenRight(!openRight)} />
        <Drawer fixed open={openLeft} background="#eee">
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenLeft(false)} />
        </Drawer>
        <Drawer fixed right open={openRight} background="#eee">
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenRight(false)} />
        </Drawer>
      </Container>
    );
  })
  .add('fixed with top margin', () => {
    const [openLeft, setOpenLeft] = useState(false);
    const [openRight, setOpenRight] = useState(false);
    return (
      <Container>
        <Button label="Left Drawer" onClick={() => setOpenLeft(!openLeft)} />
        <Button label="Right Drawer" onClick={() => setOpenRight(!openRight)} />
        <Drawer fixed open={openLeft} background="#eee" top={50}>
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenLeft(false)} />
        </Drawer>
        <Drawer fixed right open={openRight} background="#eee" top={50}>
          <p>Example content</p>
          <Button label="Close" onClick={() => setOpenRight(false)} />
        </Drawer>
      </Container>
    );
  })
  .add('help guide', () => {
    const [open, setOpen] = useState(false);
    return (
      <Container>
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Button onClick={() => setOpen(!open)} round icon={<FaQuestion />} />
        </div>

        <Drawer shadow fixed right open={open} background="white">
          {open && (
            <div style={{ position: 'absolute', left: -19, top: 50 }}>
              <Button onClick={() => setOpen(false)} round icon={<FaTimes />} />
            </div>
          )}
        </Drawer>
      </Container>
    );
  })
  .add('menu test', () => {
    const [open, setOpen] = useState(false);
    return (
      <Container style={{ ...inlineContainerStyle, width: 600 }}>
        <Drawer button open={open} background="white" />
        <div
          style={{
            ...inlineContentStyle,
            overflowX: 'hidden',
            overflowY: 'auto',
            position: 'relative',
          }}
        >
          <Menu menu={withSubMenus} />
        </div>
      </Container>
    );
  });
