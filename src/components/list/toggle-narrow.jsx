import React from 'react';
import { FaChevronLeft } from 'react-icons/fa';
import { Icon } from '../icon/icon';
import styles from './list.module.less';

export const ToggleNarrow = ({ toggleNarrow, onClickToggleNarrow }) => {
  return toggleNarrow ? (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a className={styles.toggleNarrow} onClick={onClickToggleNarrow}>
      <Icon icon={<FaChevronLeft />} />
    </a>
  ) : null;
};
