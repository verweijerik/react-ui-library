import React from 'react';
import { Badge } from '../../badge/badge';
import styles from '../list.module.less';

export const Label = ({ label }) => {
  return label ? (
    <span className={styles.label}>
      <Badge color={label.color} title={label.value} />
    </span>
  ) : null;
};
