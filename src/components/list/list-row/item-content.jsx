import React from 'react';
import styles from '../list.module.less';

export const ItemContent = ({ item, expanding }) => {
  return expanding && item.active && item.content ? (
    <div
      className={styles.itemContent}
      onClick={(evt) => evt.stopPropagation()}
    >
      {item.content}
    </div>
  ) : null;
};
