import React, { useEffect, useState } from 'react';
import cx from 'classnames';
import { GrDrag } from 'react-icons/gr';
import { Actions } from '../../actions/actions';
import { Label } from './label';
import { MetaContent } from './meta-content';
import { MetaCount } from './meta-count';
import { ItemContent } from './item-content';
import { Icon } from '../../icon/icon';
import { dragEnd, dragStart, onMouseDragDown } from '../draggable';
import styles from '../list.module.less';
import iconStyles from '../../table/icon/icon.module.less';

export const ListRow = ({
  items,
  expanding,
  invokeEditOnRowClick,
  noHeader,
  allWhite,
  onListReorder,
}) => {
  return (
    <>
      {items.map((item, index) => {
        const edit =
          item.actions &&
          item.actions.find((a) => a.label && a.label.toLowerCase() === 'edit');
        return (
          <div
            className={cx(
              styles.item,
              item.active ? styles.active : '',
              item.type === 'Heading' ? styles.heading : '',
              item.disabled ? styles.disabled : '',
              noHeader && index === 0 ? styles.borderTopNone : '',
              allWhite ? styles.backgroundWhite : '',
            )}
            onClick={(evt) => {
              if (invokeEditOnRowClick && edit) {
                //If invokeEditOnRowClick (default) and an edit action exists
                //Then clicking the list row will also invoke the edit action
                edit.onClick(evt, item.id);
              }
              if (item.onClick) {
                item.onClick(evt);
              }
            }}
            key={index}
            data-id={index}
            onDragEnd={(ev) => dragEnd(ev, item, onListReorder)}
            onDragStart={(ev) => dragStart(ev, item)}
          >
            <div className={styles.itemHeader}>
              {item.draggable && (
                <div
                  className={styles.drag}
                  onMouseDown={(e) => {
                    onMouseDragDown(e);
                  }}
                >
                  <Icon icon={<GrDrag />} />
                </div>
              )}

              <Label label={item.label} />
              <MetaContent item={item} />
              {!item.disabled && <Actions actions={item.actions} right />}
              <MetaCount item={item} />
            </div>
            <ItemContent item={item} expanding={expanding} />
          </div>
        );
      })}
    </>
  );
};
