import React from 'react';
import cx from 'classnames';
import { FaExclamationTriangle } from 'react-icons/fa';
import { Icon } from '../../icon/icon';
import styles from '../list.module.less';
import { Tooltip } from '../../tooltip/tooltip';

export const MetaContent = ({ item }) => {
  return (
    <span className={styles.title}>
      <span className={cx(styles.name, styles.bold)}>
        {item.name}

        {item.icon && item.icon.tooltip && item.icon.tooltip.text ? (
          <span className={styles.iconTooltipMargin}>
            {item.icon.tooltip.text && (
              <Tooltip text={item.icon.tooltip.text} maxWidth="350px">
                <Icon
                  icon={item.icon.icon}
                  color={item.icon.color || '#db2828'}
                  size={12}
                />
              </Tooltip>
            )}
          </span>
        ) : item.icon && item.icon.icon ? (
          <span className={styles.iconTooltipMargin}>
            <Icon
              icon={item.icon.icon}
              color={item.icon.color || '#db2828'}
              size={12}
            />
          </span>
        ) : null}
      </span>

      {item.invalid && (
        <span className={styles.invalidIcon}>
          <Icon icon={<FaExclamationTriangle />} color="#db2828" size={12} />
        </span>
      )}
      {item.details ? (
        <span className={styles.details}>{item.details}</span>
      ) : null}
      {item.metadata ? (
        <span className={styles.metadata}>{item.metadata}</span>
      ) : null}
    </span>
  );
};
