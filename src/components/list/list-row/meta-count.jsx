import React from 'react';
import styles from '../list.module.less';

export const MetaCount = ({ item }) => {
  return item.metaCount ? (
    <span className={styles.metaCount}>{item.metaCount}</span>
  ) : null;
};
