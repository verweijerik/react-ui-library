import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Actions, actionsShape } from '../actions/actions';
import { Card } from '../card/card';
import { Heading } from '../heading/heading';
import { ListRow } from './list-row/list-row';
import { ToggleNarrow } from './toggle-narrow';
import { dragOver } from './draggable';
import styles from './list.module.less';

export const List = ({
  list,
  bordered,
  expanding,
  narrow,
  toggleNarrow,
  onToggleNarrow,
  invokeEditOnRowClick,
  noHeader,
  draggable,
  allWhite,
  onListReorder,
  drawer,
  marginBottom,
}) => {
  return (
    <div
      className={cx(narrow ? styles.narrow : '', drawer ? styles.drawer : '')}
      style={{ marginBottom }}
      onDragOver={(ev) => dragOver(ev, list.items)}
    >
      <Card bordered={bordered} padding={false}>
        {!noHeader && (
          <div className={styles.header}>
            <ToggleNarrow
              toggleNarrow={toggleNarrow}
              onClickToggleNarrow={onToggleNarrow}
            />
            {list.name && (
              <div className={styles.headerTitle}>
                <Heading top marginBottom={0}>
                  {list.name}
                </Heading>
              </div>
            )}
            <Actions actions={list.actions} right />
          </div>
        )}
        <div className={cx(styles.list, bordered ? styles.bordered : '')}>
          <ListRow
            noHeader={noHeader}
            draggable={draggable}
            items={list.items}
            expanding={expanding}
            invokeEditOnRowClick={invokeEditOnRowClick}
            allWhite={allWhite}
            onListReorder={onListReorder}
          />
        </div>
      </Card>
    </div>
  );
};

List.defaultProps = {
  bordered: false,
  expanding: false,
  narrow: false,
  toggleNarrow: false,
  onToggleNarrow: () => {},
  invokeEditOnRowClick: true,
  noHeader: false,
  draggable: false,
  allWhite: false,
  onListReorder: null,
  drawer: false,
  marginBottom: 0,
};

List.propTypes = {
  list: PropTypes.shape({
    name: PropTypes.string,
    actions: actionsShape,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
          .isRequired,
        name: PropTypes.string.isRequired,
        active: PropTypes.bool,
        details: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
        metadata: PropTypes.string,
        metaCount: PropTypes.number,
        invalid: PropTypes.bool,
        content: PropTypes.element,
        actions: actionsShape,
        icon: PropTypes.shape({
          icon: PropTypes.node.isRequired,
          color: PropTypes.string.isRequired,
          tooltip: {
            text: PropTypes.oneOfType([
              PropTypes.string,
              PropTypes.number,
              PropTypes.node,
            ]),
            error: PropTypes.bool,
            warning: PropTypes.bool,
            anchor: PropTypes.string,
            enabled: PropTypes.bool,
            maxWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            triggerOffset: PropTypes.oneOfType([
              PropTypes.string,
              PropTypes.number,
            ]),
            possibleAnchors: PropTypes.array,
            fontSize: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            padding: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
          },
        }),
      }),
    ).isRequired,
  }).isRequired,
  bordered: PropTypes.bool,
  expanding: PropTypes.bool,
  narrow: PropTypes.bool,
  toggleNarrow: PropTypes.bool,
  onToggleNarrow: PropTypes.func,
  invokeEditOnRowClick: PropTypes.bool, //by default clicking row invokes edit action
  noHeader: PropTypes.bool,
  draggable: PropTypes.bool,
  allWhite: PropTypes.bool,
  onListReorder: PropTypes.func,
  drawer: PropTypes.bool,
  marginBottom: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
