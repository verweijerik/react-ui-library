import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import {
  FaChartBar,
  FaArrowAltCircleDown,
  FaPencilAlt,
  FaTrash,
  FaPlus,
  FaArrowDown,
  FaArrowUp,
  FaCog,
  FaRegCopy,
  FaExclamationTriangle,
} from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { List } from './list';
import { Menu } from '../menu/menu';
import { Drawer } from '../drawer/drawer';

const style = {
  height: '500px',
  width: '360px',
};

export const list = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

export const withHeadings = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Main element',
      type: 'Heading',
    },
    {
      id: 2,
      name: 'Aardvark',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Other elements',
      type: 'Heading',
    },
    {
      id: 4,
      name: 'Kangaroo',
      active: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 5,
      name: 'Panda',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const withRowEventHandler = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      onClick: () => {
        console.log('Row clicked');
      },
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      onClick: () => {
        console.log('Row clicked');
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {
            console.log('Pencil (edit action) clicked');
          },
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {
            console.log('Trash clicked');
          },
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      onClick: () => {
        console.log('Row clicked');
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {
            console.log('Pencil (edit action) clicked');
          },
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {
            console.log('Trash (edit action) clicked');
          },
        },
      ],
    },
  ],
};

const invalidItemsList = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      invalid: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const metaCountList = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      invalid: true,
      metaCount: 4,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const subMenuList = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
    {
      label: 'Clone',
      icon: <FaCog />,
      subActions: [
        {
          label: 'Clone',
          icon: <FaRegCopy />,
          onClick: () => {},
        },
      ],
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
      ],
    },
  ],
};

export const labelledList = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      label: {
        value: 1,
        color: '#69779b',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      label: {
        value: 2,
        color: '#acdeaa',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      label: {
        value: 'Foo',
        color: '#acdbdf',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Platypus (dark label)',
      label: {
        value: 'Bar',
        color: '#444',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const labelledListMetaCount = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      metaCount: 3,
      label: {
        value: 1,
        color: '#69779b',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      metaCount: 1,
      active: true,
      label: {
        value: 2,
        color: '#acdeaa',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      metaCount: 4,
      label: {
        value: 'Foo',
        color: '#acdbdf',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 4,
      name: 'Platypus (dark label)',
      metaCount: 10,
      label: {
        value: 'Bar',
        color: '#444',
      },
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const subtitledList = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      details: 'Eats termites',
      metadata: '2019-05-03 09:03:51',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      details: 'Eats grass',
      metadata: '2019-05-03 09:03:51',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      details: 'Eats bamboo',
      metadata: '2019-05-03 09:03:51.405',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const expandableContent = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      content: <div>Example content element (for show/hide)</div>,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const withJsxDetails = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      details: <div>Example details element 1</div>,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      details: <div>Example details element 2</div>,
      metadata: '2019-05-03 09:03:51',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      details: <div>Example details element 3</div>,
      metadata: '2019-05-03 09:03:51',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const withoutHeader = {
  name: 'Title',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      invalid: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      active: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const disabled = {
  name: 'List Name',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
  ],
  items: [
    {
      id: 1,
      name: 'Disabled element',
      invalid: false,
      disabled: true,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 2,
      name: 'Enabled element',
      active: false,
      disabled: false,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
  ],
};

const kitchenSinkList = {
  name: 'Animals',
  actions: [
    {
      label: 'Add',
      icon: <FaPlus />,
      onClick: () => {},
    },
    {
      label: 'Clone',
      icon: <FaCog />,
      subActions: [
        {
          label: 'Clone',
          icon: <FaRegCopy />,
          onClick: () => {},
        },
      ],
    },
  ],
  items: [
    {
      id: 1,
      name: 'Aardvark',
      label: {
        value: 1,
        color: '#69779b',
      },
      details: 'Eats termites',
      metadata: '2019-05-03 09:03:51',
      invalid: true,
      metaCount: 3,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
      ],
    },
    {
      id: 2,
      name: 'Kangaroo',
      label: {
        value: 2,
        color: '#acdeaa',
      },
      active: true,
      details: <div>Example details element</div>,
      metadata: '2019-05-03 09:03:51',
      content: <div>Example content element (for show/hide)</div>,
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
      ],
    },
    {
      id: 3,
      name: 'Panda',
      label: {
        value: 'Foo',
        color: '#acdbdf',
      },
      details: 'Eats bamboo',
      metadata: '2019-05-03 09:03:51.405',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
        {
          label: 'More',
          subActions: [
            {
              label: 'Move Up',
              icon: <FaArrowUp />,
              onClick: () => {},
            },
            {
              label: 'Move Down',
              icon: <FaArrowDown />,
              onClick: () => {},
            },
          ],
        },
      ],
    },
  ],
};
const allWhite = {
  name: 'All white elements',
  allWhite: true,
  items: [
    {
      id: 1,
      name: 'Enabled element 1',
      active: false,
    },
    {
      id: 2,
      name: 'Enabled element 2',
      active: false,
    },
  ],
};

const withTooltip = {
  name: 'List Name',
  items: [
    {
      id: 1,
      name: 'Element 1 - icon with tooltip',
      icon: {
        icon: <FaExclamationTriangle />,
        color: '#db2828',
        tooltip: {
          text: (
            <div>
              <ul>
                <li>test2 list</li>
                <li>test2 list</li>
                <li>test2 list</li>
              </ul>
            </div>
          ),
        },
      },
    },
    {
      id: 2,
      name: 'Element 2 - only icon',
      icon: {
        icon: <FaExclamationTriangle />,
        color: '#db2828',
      },
    },
  ],
};

const draggable = {
  items: [
    {
      id: 1,
      name: 'Main element',
      type: 'Heading',
    },
    {
      id: 2,
      name: 'Item no 1',
      draggable: true,
    },
    {
      id: 3,
      name: 'Item no 2',
      draggable: true,
    },
    {
      id: 4,
      name: 'Item no 3',
      draggable: true,
      details: <div>Example details element 2</div>,
      metadata: '2019-05-03 09:03:51',
      actions: [
        {
          label: 'Edit',
          icon: <FaPencilAlt />,
          onClick: () => {},
        },
        {
          label: 'Delete',
          icon: <FaTrash />,
          onClick: () => {},
        },
      ],
    },
    {
      id: 5,
      name: 'Item no 4',
      draggable: true,
    },
    {
      id: 6,
      name: 'Item no 5',
      draggable: true,
    },
  ],
};
const menu = (
  <Menu
    menu={{
      trigger: 'Component',
      component: <FaArrowAltCircleDown color="#ff6600" />,
      sections: [
        { type: 'Heading', label: 'Options' },
        {
          type: 'Option',
          label: 'Hydrated',
          icon: <FaChartBar color="#ff6600" />,
          onClick: () => {},
        },
      ],
    }}
  />
);

storiesOf('Basic/List', module)
  .add('default', () => (
    <Container style={style}>
      <List list={list} bordered />
    </Container>
  ))
  .add('without borders', () => (
    <Container style={style}>
      <List list={list} />
    </Container>
  ))
  .add('with headings', () => (
    <Container style={style}>
      <List list={withHeadings} bordered />
    </Container>
  ))
  .add('with row onClick event', () => (
    <Container style={style}>
      <h2>Results in console after click</h2>
      <List list={withRowEventHandler} bordered invokeEditOnRowClick={false} />
    </Container>
  ))
  .add('labelled', () => (
    <Container style={style}>
      <List list={labelledList} bordered />
    </Container>
  ))
  .add('with details', () => (
    <Container style={style}>
      <List list={subtitledList} bordered />
    </Container>
  ))
  .add('with JSX details', () => (
    <Container style={style}>
      <List list={withJsxDetails} bordered />
    </Container>
  ))
  .add('with embedded menu component', () => (
    <Container style={style}>
      <List
        list={{
          name: 'Animals',
          actions: [
            {
              label: 'Add',
              icon: <FaPlus />,
              onClick: () => {},
            },
            { childComponent: menu },
          ],
          items: [
            {
              id: 1,
              name: 'Aardvark',
              actions: [
                { childComponent: menu },
                {
                  label: 'Edit',
                  icon: <FaPencilAlt />,
                  onClick: () => {},
                },
                {
                  label: 'Delete',
                  icon: <FaTrash />,
                  onClick: () => {},
                },
              ],
            },
          ],
        }}
        bordered
      />
    </Container>
  ))
  .add('with sub-menus', () => (
    <Container style={style}>
      <List list={subMenuList} bordered />
    </Container>
  ))
  .add('with expandable content', () => (
    <Container style={style}>
      <List list={expandableContent} bordered expanding />
    </Container>
  ))
  .add('invalid/expired items', () => (
    <Container style={style}>
      <List list={invalidItemsList} bordered />
    </Container>
  ))
  .add('with metaCount', () => (
    <Container style={style}>
      <List list={metaCountList} bordered />
    </Container>
  ))
  .add('inline drawer button (toggleNarrow)', () => {
    const [open, setOpen] = useState(true);
    return (
      <Container style={style}>
        <Drawer open={open} background="white" closedWidth={50}>
          <List
            list={labelledListMetaCount}
            narrow={!open}
            toggleNarrow
            onToggleNarrow={() => setOpen(!open)}
            bordered
          />
        </Drawer>
        <div style={{ borderLeft: '1px solid #ddd' }}></div>
      </Container>
    );
  })
  .add('full height', () => {
    const [open, setOpen] = useState(true);
    return (
      <Container
        style={{
          height: '500px',
          background: '#f5f5f5',
          border: '1px solid #ddd',
          display: 'flex',
        }}
      >
        <Drawer open={open} background="#f5f7f9" closedWidth={50}>
          <List
            list={labelledListMetaCount}
            narrow={!open}
            toggleNarrow
            onToggleNarrow={() => setOpen(!open)}
            drawer
          />
        </Drawer>
        <div style={{ borderLeft: '1px solid #ddd' }}></div>
      </Container>
    );
  })
  .add('without header', () => (
    <Container style={style}>
      <List list={withoutHeader} bordered noHeader />
    </Container>
  ))
  .add('all list elements white', () => (
    <Container style={style}>
      <List list={withoutHeader} bordered allWhite />
    </Container>
  ))
  .add('drag and drop', () => {
    const [dragList, setDraglist] = useState(draggable);
    const onListReorder = (list) => {
      const { to, from, data } = list;
      dragList.items.splice(to, 0, dragList.items.splice(from, 1)[0]);
      setDraglist({ ...dragList });
    };
    return (
      <Container style={style}>
        <List
          list={dragList}
          bordered
          draggable
          noHeader
          onListReorder={onListReorder}
        />
      </Container>
    );
  })
  .add('kitchen sink', () => {
    const [open, setOpen] = useState(true);
    return (
      <Container style={style}>
        <Drawer open={open} background="white" closedWidth={50}>
          <List
            list={kitchenSinkList}
            narrow={!open}
            toggleNarrow
            onToggleNarrow={() => setOpen(!open)}
            bordered
            expanding
          />
        </Drawer>
      </Container>
    );
  })
  .add('disabled elements', () => (
    <Container style={style}>
      <List list={disabled} bordered />
    </Container>
  ))
  .add('all list with white background', () => (
    <Container style={style}>
      <List list={allWhite} bordered allWhite />
    </Container>
  ))
  .add('list with tooltip info', () => (
    <Container style={style}>
      <List list={withTooltip} bordered />
    </Container>
  ));
