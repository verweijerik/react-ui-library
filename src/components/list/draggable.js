// //  DRAG AND DROP METHODS
let lastLowerThanElemHalfHeight = null;
let lastPosition = null;
let dragged = null;
let over = null;
let dragPlaceholder = document.createElement('div');

export const dragStart = (ev) => {
  dragged = ev.currentTarget;
  ev.dataTransfer.effectAllowed = 'move';
  ev.dataTransfer.setData('text/plain', ev.target.id);
  dragPlaceholder.style.width = dragged.offsetWidth + 'px';
  dragPlaceholder.style.height = dragged.offsetHeight + 'px';
  dragPlaceholder.style.maxHeight = dragged.offsetHeight + 'px';
  dragPlaceholder.style.backgroundColor = '#0000000d';
  dragPlaceholder.style.transition = 'max-height 250ms ease-in-out';
  dragPlaceholder.style.overflow = 'hidden';
  dragPlaceholder.id = 'dragPlaceholder';
};

const generateDragPlaceholderClone = () => {
  let dragPlaceholderClone = dragPlaceholder.cloneNode();
  dragPlaceholderClone.id = '';
  dragPlaceholder.parentNode.replaceChild(
    dragPlaceholderClone,
    dragPlaceholder,
  );
  return dragPlaceholderClone;
};

const whichTransitionEvent = () => {
  const el = document.createElement('fakeelement');
  const transitions = {
    WebkitTransition: 'webkitTransitionEnd',
    MozTransition: 'transitionend',
    MSTransition: 'msTransitionEnd',
    OTransition: 'oTransitionEnd',
    transition: 'transitionEnd',
  };

  for (let t in transitions) {
    if (el.style[t] !== undefined) {
      return transitions[t];
    }
  }
};

const animateDragPlaceholder = (dragPlaceholderClone) => {
  dragPlaceholder.style.maxHeight = '0px';
  // eslint-disable-next-line no-unused-expressions
  dragPlaceholderClone.offsetHeight;
  dragPlaceholderClone.style.maxHeight = '0px';

  dragPlaceholderClone.addEventListener(whichTransitionEvent(), () => {
    dragPlaceholderClone.parentNode.removeChild(dragPlaceholderClone);
  });

  // eslint-disable-next-line no-unused-expressions
  dragPlaceholder.offsetHeight;
  dragPlaceholder.style.maxHeight = dragPlaceholder.style.height;
};

export const dragOver = (ev) => {
  ev.preventDefault();
  const firstTimeDragOver = !over;

  if (firstTimeDragOver) {
    dragged.parentNode.insertBefore(dragPlaceholder, dragged.nextSibling);
    dragged.style.maxHeight = '0';
    dragged.style.overflow = 'hidden';
  }

  const newOver = ev.target.closest('[draggable]');

  if (!newOver) return;

  const { top, height } = newOver.getBoundingClientRect();

  const halfYElement = top + height / 2;

  if (
    lastLowerThanElemHalfHeight === ev.clientY > halfYElement &&
    newOver === over
  ) {
    return;
  }

  over = newOver;
  lastLowerThanElemHalfHeight = ev.clientY > halfYElement;

  let dragPlaceholderClone = null;

  if (!firstTimeDragOver) {
    dragPlaceholderClone = generateDragPlaceholderClone();
  }

  if (lastLowerThanElemHalfHeight) {
    if (over.nextSibling) {
      over.parentNode.insertBefore(dragPlaceholder, over.nextSibling);
    } else {
      over.parentNode.append(dragPlaceholder);
    }
  } else {
    over.parentNode.insertBefore(dragPlaceholder, over);
  }

  if (
    !firstTimeDragOver &&
    dragPlaceholderClone.nextSibling !== dragPlaceholder &&
    dragPlaceholderClone.previousSibling !== dragPlaceholder
  ) {
    animateDragPlaceholder(dragPlaceholderClone);
  } else if (!firstTimeDragOver) {
    dragPlaceholderClone.parentNode.removeChild(dragPlaceholderClone);
  }

  const allDraggables = Array.from(
    over.parentNode.querySelectorAll('[draggable], #dragPlaceholder'),
  ).filter((item) => item !== dragged);

  lastPosition = allDraggables.findIndex((item) => item === dragPlaceholder);
};

export const dragEnd = (ev, list, setListDragState) => {
  ev.preventDefault();

  dragged.style.display = 'block';
  dragged.style.maxHeight = '';
  dragged.style.overflow = '';
  dragged.parentNode.removeChild(dragPlaceholder);

  let data = [].concat(list);
  let from = Number(dragged.dataset.id);
  let to = Number(lastPosition);
  data.splice(to, 0, data.splice(from, 1)[0]);
  setListDragState({
    to,
    from,
    data: data[0],
  });

  lastLowerThanElemHalfHeight = null;
  lastPosition = null;
  dragged = null;
  over = null;

  const listNodes = ev.target.parentNode.children;
  for (const node of listNodes) {
    node.setAttribute('draggable', 'false');
  }
};

export const onMouseDragDown = (e) => {
  const item = e.target.closest('[data-id]');
  const listNodes = item.parentNode.childNodes;
  for (let node of listNodes) {
    node.setAttribute('draggable', 'true');
  }
};
