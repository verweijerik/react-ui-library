import React, { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Portal } from './portal';

const style = {
  height: '500px',
  width: '500px',
};

storiesOf('Modals/Portal', module)
  .add('Portal', () => {
    return (
      <Container
        style={style}
        warningMessage={`
        For most use-cases, use the Portal component. 
      `}
      >
        <div id="portalContainer" />
        <Portal id="portalContainer">
          <div>Portal content</div>
        </Portal>
      </Container>
    );
  })
  .add('React createPortal()', () => {
    const parent = document.querySelector('#root');
    return (
      <Container
        style={style}
        warningMessage={`
        This story shows how to use React createPortal() directly
      `}
      >
        <>
          <div id="portalContainer" />
          {parent && createPortal(<div>Portal content</div>, parent)}
        </>
      </Container>
    );
  })
  .add('React createPortal() with dynamic domNode', () => {
    const parent = document.querySelector('#portalContainer');
    const [visible, setVisible] = useState(false);
    useEffect(() => {
      if (!visible) setVisible(true);
    }, []);
    return (
      <Container
        style={style}
        warningMessage={`
        This story shows how to use React createPortal() directly. The domNode 
        for createPortal() must exist. For a dynamic container, this is not 
        the case on the first render. 
      `}
      >
        <>
          <div id="portalContainer" />
          {visible &&
            parent &&
            createPortal(
              <div>Portal content</div>,
              document.querySelector(`#portalContainer`),
            )}
        </>
      </Container>
    );
  });
