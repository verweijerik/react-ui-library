import { useLayoutEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';

export const Portal = ({ id, children }) => {
  const parent = document.querySelector(`#${id}`);
  const [mounted, setMounted] = useState(false);
  useLayoutEffect(() => {
    if (!mounted) setMounted(true);
  }, [id]);
  return parent ? createPortal(children, parent) : null;
};

Portal.propTypes = {
  id: PropTypes.string.isRequired,
};
