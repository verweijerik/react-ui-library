import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Toggle } from './toggle';

const style = {
  height: '500px',
  width: '200px',
};

const warningMessage = 'For most use-cases, prefer CheckBox instead.';

storiesOf('Forms/Toggle', module)
  .add('unchecked', () => (
    <Container style={style} warningMessage={warningMessage}>
      <Toggle name="example" label="Label" onChange={() => {}} />
    </Container>
  ))
  .add('checked', () => (
    <Container style={style} warningMessage={warningMessage}>
      <Toggle name="example" label="Label" checked onChange={() => {}} />
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style} warningMessage={warningMessage}>
      <div>
        <Toggle name="example" label="Label" disabled onChange={() => {}} />
      </div>
      <div>
        <Toggle
          name="example"
          label="Label"
          disabled
          checked
          onChange={() => {}}
        />
      </div>
    </Container>
  ))
  .add('small', () => (
    <Container style={style} warningMessage={warningMessage}>
      <Toggle small name="example" label="Label" checked onChange={() => {}} />
    </Container>
  ))
  .add('no label', () => (
    <Container style={style} warningMessage={warningMessage}>
      <Toggle name="example" checked onChange={() => {}} />
    </Container>
  ))
  .add('noMargin', () => (
    <Container
      style={style}
      warningMessage={`
        Toggle has top and bottom margins by default to line up with other form elements. When these margins are unwanted - for example when it's combined with another input in the same field – you can use the noMargin prop to remove them.
      `}
    >
      <div style={{ background: '#fafafa' }}>
        <Toggle
          name="example"
          noMargin
          label="Label"
          checked
          onChange={() => {}}
        />
      </div>
    </Container>
  ))
  .add('managed', () => {
    const [value, setValue] = useState(false);
    return (
      <Container style={style} warningMessage={warningMessage}>
        <Toggle
          name="example"
          label="Click label"
          checked={value}
          onChange={() => setValue(!value)}
        />
      </Container>
    );
  });
