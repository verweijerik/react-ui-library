import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './toggle.module.less';

export const Toggle = ({
  name,
  label,
  checked,
  disabled,
  small,
  onChange,
  noMargin,
}) => {
  return (
    <div
      className={cx(
        styles.toggle,
        disabled ? styles.disabled : null,
        small ? styles.small : null,
        noMargin ? styles.noMargin : null,
      )}
      onClick={(evt) => {
        evt.target.name = name;
        evt.target.checked = !checked;
        evt.target.value = !checked;
        onChange(evt);
      }}
    >
      <input
        type="checkbox"
        name={name}
        value={checked}
        checked={checked}
        onChange={() => {}}
      />
      <label>{label}</label>
    </div>
  );
};

Toggle.defaultProps = {
  label: null,
  name: undefined,
  checked: false,
  small: false,
  noMargin: false,
};

Toggle.propTypes = {
  name: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  small: PropTypes.bool,
  noMargin: PropTypes.bool,
};
