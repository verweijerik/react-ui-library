import React from 'react';
import { storiesOf } from '@storybook/react';
import { FaCheckCircle, FaPlus } from 'react-icons/fa';
import { Heading } from './heading';
import { Container } from '../../helpers/container';

const style = {
  height: '500px',
  width: '400px',
};

const customTitleStyles = {
  display: 'flex',
  alignItems: 'center',
};

storiesOf('Layout/Heading', module)
  .add('default', () => (
    <Container style={style}>
      <Heading top>Top heading</Heading>
      <Heading>Subheading</Heading>
    </Container>
  ))
  .add('custom margin', () => (
    <Container style={style}>
      <Heading top marginBottom={5}>
        Top heading
      </Heading>
      <Heading marginBottom={5}>Subheading</Heading>
    </Container>
  ))
  .add('show help', () => (
    <Container style={style}>
      <Heading top onClickHelp={() => console.log('help!')}>
        Top heading
      </Heading>
      <Heading onClickHelp={() => console.log('help!')}>Subheading</Heading>
    </Container>
  ))
  .add('with custom icon', () => (
    <Container style={style}>
      <Heading
        top
        onIconClick={() => console.log('clicked icon!')}
        icon={<FaPlus />}
      >
        Top heading
      </Heading>
      <Heading
        onIconClick={() => console.log('clicked icon!')}
        icon={<FaPlus />}
      >
        Subheading
      </Heading>
    </Container>
  ))
  .add('with custom JSX content', () => (
    <Container style={style}>
      <Heading top>
        <div style={customTitleStyles}>
          <span style={{ marginRight: 10 }}>Top heading</span>
          <FaCheckCircle />
        </div>
      </Heading>
      <Heading>
        <div style={customTitleStyles}>
          <span style={{ marginRight: 10 }}>Subheading</span>
          <FaCheckCircle />
        </div>
      </Heading>
    </Container>
  ));
