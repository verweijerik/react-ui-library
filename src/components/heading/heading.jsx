import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { FaQuestionCircle } from 'react-icons/fa';
import { Icon } from '../icon/icon';
import styles from './heading.module.less';

const IconWrapper = ({ onClick, icon, isHelp }) => {
  return (
    onClick && (
      <span
        onClick={(evt) => {
          evt.stopPropagation();
          if (onClick) {
            onClick(evt);
          }
        }}
        className={cx(styles.icon, isHelp ? styles.help : '')}
      >
        <Icon icon={icon} />
      </span>
    )
  );
};

export const Heading = ({
  children,
  onClickHelp,
  onClick,
  onIconClick,
  icon,
  marginBottom,
  top,
}) => {
  return (
    <div
      className={cx(styles.heading, top ? styles.top : '')}
      style={{ marginBottom }}
      onClick={(evt) => {
        if (onClick) {
          onClick(evt);
        }
      }}
    >
      {children}
      {icon && <IconWrapper onClick={onIconClick} icon={icon} />}
      {onClickHelp && (
        <IconWrapper onClick={onClickHelp} icon={<FaQuestionCircle />} isHelp />
      )}
    </div>
  );
};

Heading.defaultProps = {
  onClick: null,
  onClickHelp: null,
  onIconClick: null,
  icon: null,
  marginBottom: null,
  top: false,
};

Heading.propTypes = {
  onClick: PropTypes.func,
  onClickHelp: PropTypes.func,
  onIconClick: PropTypes.func,
  icon: PropTypes.node,
  marginBottom: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  top: PropTypes.bool,
};
