import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Spacer, Heading } from '../../..';
import { Tabs } from './tabs';

const style = {
  height: '500px',
  width: '500px',
};

const onChange = (evt) => {
  const { name, value, label } = evt.target;
  console.log(name);
  console.log(value);
  console.log(label);
};

/*
  You can use the Tabs component two ways:
  i) pass the children elements
  ii) don't pass children elements but instead pass a config object (the key is optional)
 */

storiesOf('Navigation/Tabs', module)
  .add('default', () => (
    <Container style={style}>
      <Tabs
        name="example"
        onChange={onChange}
        options={[
          { label: 'Tab 0', value: 0 },
          { label: 'Tab 1', value: 1 },
          { label: 'Tab 2', value: 2 },
          { label: 'Tab 3', value: 3 },
          { label: 'Tab 4', value: 4 },
        ]}
      />
    </Container>
  ))
  .add('with selected tab', () => (
    <Container style={style}>
      <Tabs
        name="example"
        onChange={onChange}
        value={3}
        options={[
          { label: 'Tab 0', value: 0 },
          { label: 'Tab 1', value: 1 },
          { label: 'Tab 2', value: 2 },
          { label: 'Tab 3', value: 3 },
          { label: 'Tab 4', value: 4 },
        ]}
      />
    </Container>
  ))
  .add('with hidden tab', () => (
    <Container style={style}>
      <Tabs
        name="example"
        onChange={onChange}
        options={[
          { label: 'Tab 0', value: 0 },
          { label: 'Tab 1', value: 1 },
          { label: 'Tab 2', value: 2 },
          { label: 'Tab 3', value: 3, hidden: true },
          { label: 'Tab 4', value: 4 },
        ]}
      />
    </Container>
  ))
  .add('with disabled tab', () => (
    <Container style={style}>
      <Tabs
        name="example"
        onChange={onChange}
        options={[
          { label: 'Tab 0', value: 0 },
          { label: 'Tab 1', value: 1 },
          { label: 'Tab 2', value: 2 },
          { label: 'Tab 3', value: 3, disabled: true },
          { label: 'Tab 4', value: 4 },
        ]}
      />
    </Container>
  ))
  .add('with horizontal padding', () => (
    <Container style={style}>
      <Tabs
        name="example"
        onChange={onChange}
        padding
        options={[
          { label: 'Tab 0', value: 0 },
          { label: 'Tab 1', value: 1 },
          { label: 'Tab 2', value: 2 },
          { label: 'Tab 3', value: 3 },
          { label: 'Tab 4', value: 4 },
        ]}
      />
    </Container>
  ))
  .add('right aligned', () => (
    <Container>
      <Heading top>Right only</Heading>
      <Tabs
        name="example"
        onChange={onChange}
        padding
        options={[
          { label: 'Tab 0', value: 0, right: true },
          { label: 'Tab 1', value: 1, right: true },
          { label: 'Tab 2', value: 2, right: true },
          { label: 'Tab 3', value: 3, right: true },
          { label: 'Tab 4', value: 4, right: true },
        ]}
      />
      <Spacer />
      <Heading top>Left &amp; right</Heading>
      <Tabs
        name="example"
        onChange={onChange}
        padding
        options={[
          { label: 'Tab 0', value: 0 },
          { label: 'Tab 1', value: 1 },
          { label: 'Tab 2', value: 2 },
          { label: 'Tab 3', value: 3, right: true },
          { label: 'Tab 4', value: 4, right: true },
        ]}
      />
      <Spacer />
      <Heading top>Test: wrapping</Heading>
      <Tabs
        name="example"
        onChange={onChange}
        padding
        options={[
          { label: 'Tab Name 0', value: 0 },
          { label: 'Tab Name 1', value: 1 },
          { label: 'Tab Name 2', value: 2 },
          { label: 'Tab Name 3', value: 3 },
          { label: 'Tab Name 4', value: 4 },
          { label: 'Tab Name 5', value: 5 },
          { label: 'Tab Name 6', value: 6 },
          { label: 'Tab Name 7', value: 6 },
          { label: 'Right Tab 1', value: 7, right: true },
          { label: 'Right Tab 2', value: 7, right: true },
        ]}
      />
    </Container>
  ))
  .add('with children', () => (
    <Container style={style}>
      <Tabs name="example" onChange={onChange} value={4}>
        <div label="Tab 0">
          <h3>Tab 0 Content</h3>
        </div>
        <div label="Tab 1" hidden>
          <h3>Tab 1 Content</h3>
        </div>
        <div label="Tab 2" disabled>
          <h3>Tab 2 Content</h3>
        </div>
        <div label="Tab 3">
          <h3>Tab 3 Content</h3>
        </div>
        <div label="Tab 4">
          <h3>Tab 4 Content</h3>
        </div>
      </Tabs>
    </Container>
  ))
  .add('managed', () => {
    const options = [
      {
        label: 'Monkeys',
        value: 'monkeys',
      },
      {
        label: 'Bananas',
        value: 'bananas',
      },
      {
        label: 'Squirrels',
        value: 'squirrels',
        disabled: true,
      },
      {
        label: 'Pomegranates',
        value: 'pomegranates',
      },
    ];
    const [selectedTab, setSelectedTab] = useState(options[0]);
    return (
      <Container style={style}>
        <Tabs
          name="example"
          value={selectedTab}
          options={options}
          onChange={(evt) => {
            const { value, label } = evt.target;
            setSelectedTab({ value, label });
          }}
        />
      </Container>
    );
  })
  .add('deprecated event handler', () => {
    const [selectedTab, setSelectedTab] = useState(0);
    return (
      <Container style={style}>
        <Tabs
          onChangeTab={(tabIndex) => {
            setSelectedTab(tabIndex);
          }}
          activeTabIndex={selectedTab}
          tabs={[
            {
              label: 'Monkeys',
              key: 'monkeys',
            },
            {
              label: 'Bananas',
              key: 'bananas',
            },
            {
              label: 'Squirrels',
              key: 'squirrels',
              disabled: true,
            },
            {
              label: 'Pomegranates',
              key: 'pomegranates',
            },
          ]}
        />
      </Container>
    );
  })
  .add('without vertical padding / margins', () => (
    <Container style={style}>
      <Tabs onChange={onChange} margin={false} contentPadding={false}>
        <div label="Tab 0">
          <h3>Tab 0 Content</h3>
        </div>
        <div label="Tab 1">
          <h3>Tab 1 Content</h3>
        </div>
        <div label="Tab 2">
          <h3>Tab 2 Content</h3>
        </div>
        <div label="Tab 3">
          <h3>Tab 3 Content</h3>
        </div>
        <div label="Tab 4">
          <h3>Tab 4 Content</h3>
        </div>
      </Tabs>
    </Container>
  ));
