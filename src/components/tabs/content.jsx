import React from 'react';
import cx from 'classnames';
import styles from './tabs.module.less';

export const Content = ({ children, activeTabIndex, contentPadding }) => {
  //The tab content is only rendered if the children props are provided, otherwise tab content is handled by the parent
  return (
    children && (
      <div className={cx(contentPadding ? styles.contentPadding : '')}>
        {children.map((child, index) => activeTabIndex === index && child)}
      </div>
    )
  );
};
