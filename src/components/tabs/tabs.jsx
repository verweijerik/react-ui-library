import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Label } from './label';
import { Content } from './content';
import styles from './tabs.module.less';
import { standardizeInputs } from '../select/select.input';

export const Tabs = ({
  name,
  options: rawOptions,
  children,
  value: rawValue,
  onChange,
  padding,
  margin,
  contentPadding,
  //deprecated:
  onChangeTab,
  activeTabIndex: rawActiveTabIndex,
  tabs: tabsConfig,
}) => {
  //----------------------------------------------------------------------------
  // Deprecation / backwards-compatibility
  const isDeprecated = onChangeTab !== undefined;

  const rawTabs = children
    ? children.map((c, i) => ({
        value: i,
        label: c.props.label,
        hidden: c.props.hidden,
        disabled: c.props.disabled,
      }))
    : rawOptions || tabsConfig;
  const { simpleInputs, options: tabs, selectedOptions: value } = isDeprecated
    ? {
        simpleInputs: false,
        options: rawTabs,
        selectedOptions: undefined,
      }
    : standardizeInputs(rawTabs, rawValue);
  const selectedValue =
    isDeprecated || value === undefined || value === null
      ? undefined
      : simpleInputs
      ? value
      : value.value;
  const activeTabIndexBase =
    rawActiveTabIndex !== undefined
      ? rawActiveTabIndex
      : tabs.findIndex((t) => t.value === selectedValue);
  const activeTabIndex = activeTabIndexBase === -1 ? 0 : activeTabIndexBase;
  //----------------------------------------------------------------------------

  const renderTab = (tab, index) => {
    const { label, hidden, disabled, value, right } = tab;
    return (
      <Label
        label={label}
        key={index}
        hidden={hidden}
        disabled={disabled}
        right={right}
        active={
          isDeprecated ? index === activeTabIndex : value === selectedValue
        }
        onClick={
          isDeprecated
            ? () => {
                //deprecated
                const value = tab.key || label || null; //return key if it exists, otherwise use label
                onChangeTab(index, value);
              }
            : (evt) => {
                evt.target.name = name;
                evt.target.value = tab.value;
                evt.target.label = label;
                onChange(evt);
              }
        }
      />
    );
  };

  if (tabs) {
    return (
      <div>
        <div
          className={cx(
            styles.tabs,
            padding ? styles.padding : '',
            margin ? styles.margin : '',
          )}
        >
          {tabs.map((tab, index) => renderTab(tab, index))}
        </div>
        <Content
          activeTabIndex={activeTabIndex}
          contentPadding={contentPadding}
        >
          {children}
        </Content>
      </div>
    );
  }
  return null;
};

Tabs.defaultProps = {
  name: undefined,
  options: null,
  children: null,
  onChange: () => {},
  padding: false,
  margin: true,
  contentPadding: true,
  //deprecated:
  activeTabIndex: undefined,
  tabs: undefined,
  onChangeTab: undefined,
};

export const tabOptionShape = PropTypes.shape({
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  hidden: PropTypes.bool,
  disabled: PropTypes.bool,
  right: PropTypes.bool,
});

export const tabSelectedOptionShape = PropTypes.shape({
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
});

const TabsShapeDeprecated = {
  tabs: PropTypes.arrayOf(tabOptionShape),
  children: PropTypes.node,
  activeTabIndex: PropTypes.number,
  onChangeTab: PropTypes.func,
  padding: PropTypes.bool,
  margin: PropTypes.bool,
  contentPadding: PropTypes.bool,
};

const TabsShape = {
  name: PropTypes.string,
  options: PropTypes.arrayOf(tabOptionShape),
  children: PropTypes.node,
  value: tabSelectedOptionShape,
  onChange: PropTypes.func,
  padding: PropTypes.bool,
  margin: PropTypes.bool,
  contentPadding: PropTypes.bool,
};

Tabs.propTypes = PropTypes.oneOfType([
  PropTypes.shape(TabsShape), //only use this for new usages
  PropTypes.shape(TabsShapeDeprecated), //deprecated Select interface
]).isRequired;
