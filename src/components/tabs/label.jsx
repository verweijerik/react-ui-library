import React from 'react';
import cx from 'classnames';
import styles from './tabs.module.less';

export const Label = ({ label, hidden, disabled, active, onClick, right }) => {
  return (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a
      id={label}
      className={cx(
        styles.item,
        active ? styles.active : '',
        disabled ? styles.disabled : '',
        hidden && !active ? styles.hidden : '',
        right ? styles.right : styles.left,
      )}
      onClick={onClick}
    >
      {label}
    </a>
  );
};
