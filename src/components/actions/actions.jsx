import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { FaEllipsisV } from 'react-icons/fa';
import { Icon } from '../icon/icon';
import { Menu } from '../menu/menu';
import styles from './actions.module.less';
import iconStyles from '../table/icon/icon.module.less';

const SubmenuActions = (props) => {
  const { actions, subMenuIcon } = props;
  const menuButton = subMenuIcon ? (
    <span className={styles.action}>
      <Icon icon={subMenuIcon} size={14} />
    </span>
  ) : (
    <span className={styles.action}>
      <FaEllipsisV className={iconStyles.icon} />
    </span>
  );

  if (actions.length) {
    const subMenu = {
      trigger: 'Component',
      component: menuButton,
      sections: actions.map((a) => {
        return {
          type: 'Option',
          label: a.label,
          icon: a.icon,
          onClick: (evt) => {
            evt.stopPropagation();
            a.onClick(evt);
          },
        };
      }),
    };

    return <Menu menu={subMenu} />;
  }
  return null;
};

export const Actions = (props) => {
  const { actions, right } = props;
  return (
    <div className={cx(styles.actions)}>
      {actions.map((action, index) => {
        if (
          action.childComponent &&
          React.isValidElement(action.childComponent)
        ) {
          return (
            <div
              data-identifier={action.identifier}
              className={styles.actionComponentContainer}
              key={index}
            >
              {action.childComponent}
            </div>
          );
        } else if (action.subActions) {
          return (
            <SubmenuActions
              actions={action.subActions}
              subMenuIcon={action.icon}
              key={index}
              right={right}
            />
          );
        } else {
          return (
            <span
              key={index}
              data-identifier={action.identifier}
              className={cx(
                styles.action,
                action.disabled ? styles.disabled : '',
              )}
              onClick={(evt) => {
                evt.stopPropagation();
                if (!action.disabled) {
                  action.onClick(evt);
                }
              }}
            >
              <Icon icon={action.icon} size={14} />
            </span>
          );
        }
      })}
    </div>
  );
};

const actionShape = PropTypes.shape({
  label: PropTypes.string.isRequired,
  icon: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  //optional, added as data-identifier, e.g. for GUI tests
  identifier: PropTypes.string,
});

const subActionShape = PropTypes.shape({
  icon: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  subActions: PropTypes.arrayOf(actionShape).isRequired,
});

const childComponentShape = PropTypes.shape({
  childComponent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node, //React component
  ]).isRequired,
  //optional, added as data-identifier, e.g. for GUI tests
  identifier: PropTypes.string,
});

export const actionsShape = PropTypes.arrayOf(
  PropTypes.oneOfType([actionShape, subActionShape, childComponentShape]),
);

Actions.defaultProps = {
  actions: [],
  right: false,
};

Actions.propTypes = {
  actions: actionsShape,
  right: PropTypes.bool,
};
