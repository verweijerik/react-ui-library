import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  FaDownload,
  FaQuestionCircle,
  FaArrowUp,
  FaArrowDown,
  FaAngleDown,
  FaHeartbeat,
  FaEllipsisH,
} from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Actions } from './actions.jsx';
import svgIcon from '../../images/icons/icons8-junction.svg';
import pngIcon from '../../images/icons/icon-drop.png';

const style = {
  display: 'inline-block',
};

storiesOf('Basic/Actions', module)
  .add('single', () => (
    <Container style={style}>
      <Actions
        actions={[
          { label: 'Download', icon: <FaDownload />, onClick: () => {} },
        ]}
      />
    </Container>
  ))
  .add('multiple', () => (
    <Container style={style}>
      <Actions
        actions={[
          { label: 'Download', icon: <FaDownload />, onClick: () => {} },
          { label: 'Help', icon: <FaQuestionCircle />, onClick: () => {} },
        ]}
      />
    </Container>
  ))
  .add('with child component', () => (
    <Container style={style}>
      <Actions
        actions={[
          {
            childComponent: (
              <div>
                <FaHeartbeat />
              </div>
            ),
          },
        ]}
      />
    </Container>
  ))
  .add('with sub actions', () => (
    <Container style={style}>
      <Actions
        actions={[
          {
            label: 'More',
            subActions: [
              {
                label: 'Move Up',
                icon: <FaArrowUp />,
                onClick: () => {},
              },
              {
                label: 'Move Down',
                icon: <FaArrowDown />,
                onClick: () => {},
              },
            ],
          },
        ]}
      />
    </Container>
  ))
  .add('with icon types', () => (
    <Container style={style}>
      <Actions
        actions={[
          { label: 'React-icons', icon: <FaDownload />, onClick: () => {} },
          { label: 'SVG icon', icon: svgIcon, onClick: () => {} },
          { label: 'PNG icon', icon: pngIcon, onClick: () => {} },
          {
            label: 'String name (deprecated)',
            icon: 'arrow down',
            onClick: () => {},
          },
          {
            label: 'More',
            icon: <FaEllipsisH />,
            subActions: [
              {
                label: 'React-icons',
                icon: <FaArrowUp />,
                onClick: () => {},
              },
              {
                label: 'SVG icon',
                icon: svgIcon,
                onClick: () => {},
              },
              {
                label: 'PNG icon',
                icon: pngIcon,
                onClick: () => {},
              },
              {
                label: 'String name (deprecated)',
                icon: 'arrow down',
                onClick: () => {},
              },
            ],
          },
        ]}
      />
    </Container>
  ))
  .add('with disabled state', () => (
    <Container style={style}>
      <Actions
        actions={[
          {
            label: 'Download',
            icon: <FaDownload />,
            onClick: () => {},
            disabled: true,
          },
        ]}
      />
    </Container>
  ))
  .add('kitchen sink', () => (
    <Container style={style}>
      <Actions
        actions={[
          { label: 'Download', icon: <FaDownload />, onClick: () => {} },
          {
            label: 'Help',
            icon: <FaQuestionCircle />,
            onClick: () => {},
            disabled: true,
          },
          {
            childComponent: (
              <div>
                <FaHeartbeat />
              </div>
            ),
          },
          {
            label: 'More',
            icon: <FaAngleDown />,
            subActions: [
              {
                label: 'Move Up',
                icon: <FaArrowUp />,
                onClick: () => {},
              },
              {
                label: 'Move Down',
                icon: <FaArrowDown />,
                onClick: () => {},
              },
            ],
          },
        ]}
      />
    </Container>
  ));
