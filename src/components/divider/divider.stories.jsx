import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Divider } from './divider.jsx';

const style = {
  height: '500px',
  width: '200px',
};

storiesOf('Layout/Divider', module)
  .add('default', () => (
    <Container style={style}>
      content
      <Divider />
      more content
    </Container>
  ))
  .add('custom margin', () => (
    <Container style={style}>
      content
      <Divider margin="0" />
      more content
    </Container>
  ));
