import React from 'react';
import PropTypes from 'prop-types';
import styles from './divider.module.less';

export const Divider = ({ margin }) => (
  <hr
    className={styles.divider}
    style={{
      marginTop: margin,
      marginBottom: margin,
    }}
  />
);

Divider.defaultProps = {
  margin: 20,
};

Divider.propTypes = {
  margin: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};
