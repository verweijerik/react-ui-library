import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaMailBulk } from 'react-icons/fa';
import { Container } from '../../helpers/container';
import { Loader } from './loader';
import { Spinner } from '../spinner/spinner';
import { ProgressBar } from '../progress-bar/progress-bar';
import { Button } from '../button/button';

const containerStyle = {
  display: 'flex',
};

const style = {
  height: '400px',
  width: '400px',
};

storiesOf('Progress/Loader', module)
  .add('default', () => (
    <Container style={containerStyle}>
      <div style={style}>
        <Loader>
          <Spinner />
        </Loader>
      </div>
      <div style={style}>
        <Loader>
          <ProgressBar width="300px" inverted percentage={40} />
        </Loader>
      </div>
    </Container>
  ))
  .add('with text', () => (
    <Container style={containerStyle}>
      <div style={style}>
        <Loader text="Calculating...">
          <Spinner />
        </Loader>
      </div>
      <div style={style}>
        <Loader text="Calculating...">
          <ProgressBar width="300px" inverted percentage={40} />
        </Loader>
      </div>
    </Container>
  ))
  .add('with extra details (string)', () => (
    <Container style={containerStyle}>
      <div style={style}>
        <Loader text="Calculating..." details="Some extra info">
          <Spinner />
        </Loader>
      </div>
      <div style={style}>
        <Loader text="Calculating..." details="Some extra info">
          <ProgressBar width="300px" inverted percentage={40} />
        </Loader>
      </div>
    </Container>
  ))
  .add('with extra details (element)', () => (
    <Container style={containerStyle}>
      <div style={style}>
        <Loader
          text="Calculating..."
          details={<Button colored label="Cancel" onClick={() => {}} />}
        >
          <Spinner />
        </Loader>
      </div>
      <div style={style}>
        <Loader
          text="Calculating..."
          details={<Button colored label="Cancel" onClick={() => {}} />}
        >
          <ProgressBar width="300px" inverted percentage={40} />
        </Loader>
      </div>
    </Container>
  ))
  .add('full size (spinner)', () => (
    <Container style={containerStyle}>
      <div style={style}>
        <Loader fullViewPortSize>
          <Spinner />
        </Loader>
      </div>
    </Container>
  ))
  .add('full size (progress bar)', () => (
    <Container style={containerStyle}>
      <div style={style}>
        <Loader fullViewPortSize>
          <ProgressBar width="300px" inverted percentage={40} />
        </Loader>
      </div>
    </Container>
  ))
  .add('custom size', () => (
    <Container style={containerStyle}>
      <div style={style}>
        <Loader text="Calculating..." width="100px" height="100px">
          <Spinner />
        </Loader>
      </div>
      <div style={style}>
        <Loader text="Calculating..." width="100px" height="100px">
          <ProgressBar width="80px" inverted percentage={40} />
        </Loader>
      </div>
    </Container>
  ))
  .add('with custom content (deprecated)', () => (
    <Container style={style}>
      <Loader text="Fetching messages...">
        <FaMailBulk color="white" size="80px" />
      </Loader>
    </Container>
  ))
  .add('managed (progress bar)', () => {
    const [percentage, setPercentage] = useState(5);
    const set = (dir) =>
      setPercentage(
        (percentage + ((10 * (dir === '+' ? 1 : -1)) % 100) + 100) % 100,
      );
    return (
      <Container style={style}>
        <Button label="-" onClick={() => set('-')} small />
        <Button label="+" onClick={() => set('+')} small />
        <div
          style={{ marginTop: '20px', marginBottom: '50px', height: '100%' }}
        >
          <Loader text="Calculating...">
            <ProgressBar width="300px" inverted percentage={percentage} />
          </Loader>
        </div>
      </Container>
    );
  });
