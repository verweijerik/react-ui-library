import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './loader.module.less';

export const Loader = ({
  width,
  height,
  text,
  details,
  fullViewPortSize,
  children,
}) => {
  return (
    <div
      className={cx(styles.loader, {
        [styles.fullViewPortSize]: fullViewPortSize,
      })}
      style={{ width, height }}
    >
      <div className={styles.dimmer}>
        <div className={styles.content}>
          {children}
          {text !== '' && <div className={styles.text}>{text}</div>}
          {details !== '' && <div className={styles.details}>{details}</div>}
        </div>
      </div>
    </div>
  );
};

Loader.defaultProps = {
  fullViewPortSize: false,
  width: null,
  height: null,
  text: '',
  details: '',
  children: null,
};

Loader.propTypes = {
  fullViewPortSize: PropTypes.bool,
  width: PropTypes.string,
  height: PropTypes.string,
  text: PropTypes.string,
  details: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.node,
};
