import _ from 'lodash';
import React from 'react';
import RcSlider, { Handle } from 'rc-slider';
import memoizeOne from 'memoize-one';
import isEqual from 'react-fast-compare';
import { FaFastBackward, FaFastForward } from 'react-icons/fa';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { SliderTooltip } from './slider-tooltip';
import './rc-slider.less';
import styles from './slider.module.less';
import { Tooltip } from '../tooltip/tooltip';
import { Button } from '../button/button';

const formatMarkers = (marks) => {
  const formatted = marks.reduce((acc, { label, tooltip, value }) => {
    const isTooltip = tooltip !== undefined;
    const formattedValue = isTooltip ? (
      <Tooltip text={tooltip}>{label}</Tooltip>
    ) : (
      label || value
    );
    acc[value] = formattedValue;
    return acc;
  }, {});
  return formatted;
};

const memoizedFormatMarkers = memoizeOne(formatMarkers, isEqual);

export const Slider = ({
  name,
  label,
  width,
  labelWidth,
  value,
  min,
  max,
  step,
  marks,
  showArrows,
  showTooltip,
  tooltipFormatter,
  disabled,
  vertical,
  onChange,
}) => {
  const formattedMarks = memoizedFormatMarkers(marks);

  const onChangeValue = (value) => {
    //rc-slider doesn't pass the Synthetic event
    //but we can still follow the standard event format
    onChange({
      target: {
        name,
        value,
        label,
      },
    });
  };

  const ButtonMin = () => (
    <div className={styles.button}>
      <Button
        basic
        small
        onClick={() => onChangeValue(min)}
        disabled={disabled || value === min}
        icon={<FaFastBackward />}
      />
    </div>
  );

  const ButtonMax = () => (
    <div className={styles.button}>
      <Button
        basic
        small
        onClick={() => onChangeValue(max)}
        disabled={disabled || value === max}
        icon={<FaFastForward />}
      />
    </div>
  );

  return (
    <div
      className={cx(
        styles.container,
        vertical.enabled ? styles.vertical : '',
        !_.isEmpty(marks) ? styles.hasMarks : '',
      )}
      style={
        vertical.enabled
          ? { width: vertical.width, height: vertical.height }
          : { width }
      }
    >
      {showArrows && (vertical.enabled ? <ButtonMax /> : <ButtonMin />)}
      <RcSlider
        className={cx(
          disabled && styles.rcSliderDisabled,
          showArrows && styles.hasArrows,
        )}
        value={value}
        max={max}
        min={min}
        step={step}
        marks={formattedMarks}
        onChange={(v) => onChangeValue(v)}
        disabled={disabled}
        vertical={vertical.enabled}
        handle={
          showTooltip
            ? ({ index, value, dragging, ...handleProps }) => {
                return (
                  <Handle
                    key={index}
                    value={value}
                    {...handleProps} // eslint-disable-line react/jsx-props-no-spreading
                  >
                    {dragging && (
                      <SliderTooltip text={tooltipFormatter(value)} />
                    )}
                  </Handle>
                );
              }
            : undefined
        }
      />
      {showArrows && (vertical.enabled ? <ButtonMin /> : <ButtonMax />)}
      {label && (
        <label className={styles.label} style={{ width: labelWidth }}>
          {label}
        </label>
      )}
    </div>
  );
};

Slider.defaultProps = {
  name: undefined,
  label: null,
  width: '100%',
  labelWidth: 'auto',
  step: 1,
  marks: [],
  showArrows: false,
  showTooltip: false,
  tooltipFormatter: (v) => v,
  disabled: false,
  vertical: {
    enabled: false,
    width: '100px',
    height: '400px',
  },
};

Slider.propTypes = {
  name: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  width: PropTypes.string,
  labelWidth: PropTypes.string,
  value: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number,
  marks: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired,
      label: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
        PropTypes.node,
      ]),
      tooltip: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
        PropTypes.node,
      ]),
    }),
  ),
  onChange: PropTypes.func.isRequired,
  showArrows: PropTypes.bool,
  showTooltip: PropTypes.bool,
  tooltipFormatter: PropTypes.func,
  disabled: PropTypes.bool,
  //for vertical variant:
  vertical: PropTypes.object,
};
