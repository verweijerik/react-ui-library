import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Slider } from './slider.jsx';

const style = {
  height: '500px',
  width: '600px',
};

storiesOf('Progress/Slider', module)
  .add('basic', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          value={value}
          min={0}
          max={100}
          step={1}
          onChange={(evt) => setValue(evt.target.value)}
        />
      </Container>
    );
  })
  .add('with label', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          label="Number of aardvarks"
          value={value}
          min={0}
          max={100}
          step={1}
          onChange={(evt) => setValue(evt.target.value)}
        />
      </Container>
    );
  })
  .add('with JSX label', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          label={
            <span>
              Auto width label (default): <strong>{value}</strong>
            </span>
          }
          value={value}
          min={0}
          max={100}
          step={1}
          onChange={(evt) => setValue(evt.target.value)}
        />
        <Slider
          name="example"
          label={
            <span>
              Fixed width label: <strong>{value * Math.random()}</strong>
            </span>
          }
          value={value}
          min={0}
          max={100}
          step={1}
          labelWidth="200px"
          onChange={(evt) => setValue(evt.target.value)}
        />
      </Container>
    );
  })
  .add('with arrows', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          value={value}
          min={0}
          max={100}
          step={1}
          onChange={(evt) => setValue(evt.target.value)}
          showArrows
        />
      </Container>
    );
  })
  .add('with tooltip', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          value={value}
          min={0}
          max={100}
          step={1}
          onChange={(evt) => setValue(evt.target.value)}
          showTooltip
        />
      </Container>
    );
  })
  .add('with formatted tooltip', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          value={value}
          min={0}
          max={100}
          step={1}
          onChange={(evt) => setValue(evt.target.value)}
          showTooltip
          tooltipFormatter={(v) => (
            <div style={{ color: 'red' }}>{`${v} extra info lorem ipsum`}</div>
          )}
        />
      </Container>
    );
  })
  .add('with marks', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          value={value}
          min={0}
          max={100}
          step={1}
          marks={[
            { value: 20 },
            { value: 40 },
            { value: 60, label: <strong>60 (best)</strong> },
            { value: 80 },
            { value: 100, label: <span style={{ color: 'red' }}>100</span> },
          ]}
          onChange={(evt) => setValue(evt.target.value)}
        />
      </Container>
    );
  })
  .add('with tooltip marks', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          value={value}
          min={0}
          max={100}
          step={1}
          marks={[
            { label: 'S1...', tooltip: 'S1 Production', value: 20 },
            { label: 'S2...', tooltip: 'S2 Cementing', value: 40 },
            {
              label: 'S3...',
              tooltip: (
                <span>
                  <strong>S3</strong> Drilling
                </span>
              ),
              value: 60,
            },
            { label: 'S4...', tooltip: 'S4 Shut in', value: 80 },
            { label: 'S5...', tooltip: 'S5 Circulate', value: 100 },
          ]}
          onChange={(evt) => setValue(evt.target.value)}
        />
      </Container>
    );
  })
  .add('disabled', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          label="Number of aardvarks"
          value={value}
          min={0}
          max={100}
          step={1}
          marks={[
            { label: 'S1...', tooltip: 'S1 Production', value: 20 },
            { value: 40 },
            { value: 60, label: <strong>60 (best)</strong> },
            { value: 80, label: 'S4...', tooltip: 'S4 Shut-in' },
            { value: 100, label: 'S5...', tooltip: 'S4 Circulate' },
          ]}
          onChange={(evt) => setValue(evt.target.value)}
          showArrows
          showTooltip
          disabled
        />
      </Container>
    );
  })
  .add('kitchen sink', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          label="Number of aardvarks"
          value={value}
          min={0}
          max={100}
          step={1}
          marks={[
            { value: 20, label: 'S1...', tooltip: 'S1 Production' },
            { value: 40 },
            { value: 60, label: <strong>60 (best)</strong> },
            { value: 80, label: 'S4...', tooltip: 'S4 Shut-in' },
            { value: 100, label: 'S5...', tooltip: 'S5 Circulate' },
          ]}
          onChange={(evt) => setValue(evt.target.value)}
          showArrows
          showTooltip
        />
      </Container>
    );
  })
  .add('vertical', () => {
    const [value, setValue] = useState(42);
    return (
      <Container style={style}>
        <Slider
          name="example"
          label="Number of aardvarks"
          value={value}
          min={0}
          max={100}
          step={1}
          marks={[
            { value: 20, label: 'S1...', tooltip: 'S1 Production' },
            { value: 40 },
            { value: 60, label: <strong>60 (best)</strong> },
            { value: 80, label: 'S4...', tooltip: 'S4 Shut-in' },
            { value: 100, label: 'S5...', tooltip: 'S5 Circulate' },
          ]}
          onChange={(evt) => setValue(evt.target.value)}
          showArrows
          showTooltip
          vertical={{
            enabled: true,
            width: '100px',
            height: '400px',
          }}
        />
      </Container>
    );
  });
