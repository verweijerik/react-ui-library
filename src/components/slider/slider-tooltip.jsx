import React from 'react';
import { TooltipLayer } from '../tooltip/tooltip-layer';

//See https://github.com/react-component/slider/issues/409#issuecomment-606750232
export const SliderTooltip = ({ text }) => {
  return (
    <TooltipLayer
      text={text}
      layerProps={{
        style: {
          whiteSpace: 'nowrap',
          position: 'fixed',
          transform: 'translate(-50%, -100%)',
          marginTop: -15,
          marginLeft: 5,
        },
      }}
    />
  );
};
