import React from 'react';
import PropTypes from 'prop-types';

export const Spacer = ({ height, width, flex }) => (
  <div
    style={{
      height,
      width,
      flexGrow: flex ? 1 : 0,
      flexShrink: flex ? 1 : 0,
    }}
  ></div>
);

Spacer.defaultProps = {
  height: '20px',
  width: 'auto',
  flex: false,
};

Spacer.propTypes = {
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  flex: PropTypes.bool,
};
