import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  TopBar,
  SideBar,
  Heading,
  Row,
  Column,
  Page,
  Divider,
  Spacer,
} from '../../..';
import { placeholder } from './placeholder';

const ImagePlaceholder = ({ width, height }) => (
  <div
    style={{
      background: '#ddd',
      color: '#aaa',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width,
      height,
    }}
  >
    PLACEHOLDER
  </div>
);

storiesOf('Layout/Columns & Rows', module)
  .add('Default', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page>
        <Row>
          <Column>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
        </Row>
      </Page>
    </>
  ))
  .add('Proportional', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page>
        <Row>
          <Column width="50%">
            <Heading top>width=&quot;50%&quot;</Heading>
            {placeholder}
          </Column>
          <Column width="30%">
            <Heading top>width=&quot;30%&quot;</Heading>
            {placeholder}
          </Column>
          <Column width="20%">
            <Heading top>width=&quot;20%&quot;</Heading>
            {placeholder}
          </Column>
        </Row>
      </Page>
    </>
  ))
  .add('Fixed width', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page>
        <Row>
          <Column width="300px">
            <Heading top>width=&quot;300px&quot;</Heading>
            {placeholder}
          </Column>
          <Column>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
        </Row>
      </Page>
    </>
  ))
  .add('Scroll & border', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={false} scroll={false}>
        <Row spacing={0} height="100%">
          <Column width="33.333%" scroll padding>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column width="33.333%" scroll padding borderLeft>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column width="33.333%" borderLeft>
            <ImagePlaceholder width="100%" height="100%" />
          </Column>
        </Row>
      </Page>
    </>
  ))
  .add('Custom spacing', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page>
        <Row spacing={0}>
          <Column>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column>
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
        </Row>
      </Page>
    </>
  ))
  .add('Custom padding', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page>
        <Row>
          <Column padding="30px">
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column padding="0">
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
          <Column padding="50px">
            <Heading top>Default</Heading>
            {placeholder}
          </Column>
        </Row>
      </Page>
    </>
  ))
  .add('Flex rows', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={0}>
        <Row spacing={0}>
          <Column width="50%" scroll padding>
            <Heading top>Default Row (natural height)</Heading>
            Content goes here
          </Column>
          <Column width="50%" borderLeft>
            <ImagePlaceholder width="100%" height="100%" />
          </Column>
        </Row>
        <Divider margin={0} />
        <Row spacing={0} flex>
          <Column width="50%" scroll padding>
            <Heading top>Flex Row (fills height)</Heading>
            {placeholder}
          </Column>
          <Column width="50%" borderLeft>
            <ImagePlaceholder width="100%" height="100%" />
          </Column>
        </Row>
      </Page>
    </>
  ))
  .add('Responsive (width)', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={0}>
        <Row wrap spacing={0}>
          <Column
            background="#eeeeff"
            width="300px"
            widthTablet="50%"
            widthMobile="100%"
            padding
          >
            <Heading top>width=&quot;300px&quot;</Heading>
            <Heading top>widthTablet=&quot;50%&quot;</Heading>
            <Heading top>widthMobile=&quot;100%&quot;</Heading>
            <Spacer />
          </Column>
          <Column
            background="#eeffee"
            width="300px"
            widthTablet="50%"
            widthMobile="100%"
            padding
          >
            <Heading top>width=&quot;300px&quot;</Heading>
            <Heading top>widthTablet=&quot;50%&quot;</Heading>
            <Heading top>widthMobile=&quot;100%&quot;</Heading>
          </Column>
          <Column background="#ffeeee" padding>
            <Heading top>Default (fill remaining width)</Heading>
            <Spacer />
          </Column>
        </Row>
      </Page>
    </>
  ));
