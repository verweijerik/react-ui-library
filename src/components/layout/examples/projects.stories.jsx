import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { TopBar, List, Tabs, Page, Row, Column } from '../../../..';

const tabs = [
  {
    label: 'Browse',
    value: 'Browse',
  },
  {
    label: 'Recent',
    value: 'Recent',
  },
  {
    label: 'My Projects',
    value: 'My Projects',
  },
];

const items = [
  { id: 1, name: 'Lorem', active: true },
  { id: 2, name: 'Ipsum' },
  { id: 3, name: 'Dolor' },
  { id: 4, name: 'Long name goes here lorem ipsum' },
  { id: 5, name: 'Lorem' },
  { id: 6, name: 'Ipsum' },
  { id: 7, name: 'Dolor' },
  { id: 8, name: 'Long name goes here lorem ipsum' },
  { id: 9, name: 'Lorem' },
  { id: 10, name: 'Ipsum' },
  { id: 11, name: 'Dolor' },
  { id: 12, name: 'Long name goes here lorem ipsum' },
  { id: 13, name: 'Lorem' },
  { id: 14, name: 'Ipsum' },
  { id: 15, name: 'Dolor' },
  { id: 16, name: 'Long name goes here lorem ipsum' },
];

storiesOf('Layout/Layout Examples', module).add('Projects', () => {
  const [open, setOpen] = useState(true);
  return (
    <>
      <TopBar />
      <Page padding={false} left={0} scroll={false}>
        <Tabs margin={false} padding options={tabs} value={tabs[0]} />
        <Row spacing={0} flex height="100%">
          <Column borderRight scroll showScrollbar={false}>
            <List
              list={{
                name: 'Country',
                items,
              }}
            />
          </Column>
          <Column borderRight scroll showScrollbar={false}>
            <List
              list={{
                name: 'Field',
                items,
              }}
            />
          </Column>
          <Column borderRight scroll showScrollbar={false}>
            <List
              list={{
                name: 'Site',
                items,
              }}
            />
          </Column>
          <Column borderRight scroll showScrollbar={false}>
            <List
              list={{
                name: 'Well',
                items,
              }}
            />
          </Column>
          <Column borderRight scroll showScrollbar={false}>
            <List
              list={{
                name: 'Wellbore',
                items,
              }}
            />
          </Column>
          <Column scroll showScrollbar={false}>
            <List
              list={{
                name: 'Design',
                items,
              }}
            />
          </Column>
        </Row>
      </Page>
    </>
  );
});
