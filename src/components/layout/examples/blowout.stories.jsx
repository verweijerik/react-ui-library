import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaQuestion } from 'react-icons/fa';
import {
  TopBar,
  SideBar,
  Heading,
  Button,
  Card,
  Column,
  Row,
  Page,
  Spacer,
} from '../../../..';

const ImagePlaceholder = ({ width, height }) => (
  <div style={{ background: '#ddd', width, height }}></div>
);

storiesOf('Layout/Layout Examples', module).add('Blowout Simulation', () => (
  <>
    <TopBar />
    <SideBar options={{ title: '', sections: [] }} />
    <Page padding="16px 20px">
      <Row>
        <Column width="80px">
          <ImagePlaceholder width={60} height={80} />
        </Column>
        <Column>
          <Heading top>Blowout Simulation</Heading>
          <Button colored label="Save & run simulation" />
        </Column>
        <Column flex={false}>
          <Button onClick={() => {}} round icon={<FaQuestion />} />
        </Column>
      </Row>
      <Spacer />
      <Row>
        <Column>
          <Card heading={<Heading>Reservoir Zones</Heading>}>Content</Card>
          <Spacer />
          <Card heading={<Heading>Simulation Models</Heading>}>Content</Card>
          <Spacer />
          <Card heading={<Heading>Simulation Options</Heading>}>Content</Card>
        </Column>
        <Column>
          <Card heading={<Heading>Drill String</Heading>}>Content</Card>
          <Spacer />
          <Card heading={<Heading>Surface Roughness</Heading>}>Content</Card>
          <Spacer />
          <Card heading={<Heading>Weighted Rates</Heading>}>Content</Card>
        </Column>
      </Row>
    </Page>
  </>
));
