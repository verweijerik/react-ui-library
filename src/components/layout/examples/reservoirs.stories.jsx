import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaQuestion, FaChevronLeft } from 'react-icons/fa';
import {
  TopBar,
  SideBar,
  Heading,
  Button,
  Card,
  Drawer,
  List,
  Field,
  Input,
  RadioButton,
  Select,
  CheckBox,
  Spacer,
  Page,
  Row,
  Column,
  FormRow,
} from '../../../..';

const ImagePlaceholder = ({ width, height }) => (
  <div style={{ background: '#ddd', width, height }}></div>
);

storiesOf('Layout/Layout Examples', module).add('Reservoirs', () => {
  const [open, setOpen] = useState(true);
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={false} scroll={false}>
        <Row spacing={0} height="100%">
          <Drawer
            button={
              <Button
                onClick={() => setOpen(!open)}
                round
                icon={<FaChevronLeft />}
              />
            }
            open={open}
            width={300}
            closedWidth={50}
          >
            <List
              drawer
              narrow={!open}
              list={{
                name: 'Reservoirs',
                items: [
                  {
                    id: 1,
                    name: 'Lorem',
                    active: true,
                  },
                  {
                    id: 2,
                    name: 'Ipsum',
                  },
                ],
              }}
            />
          </Drawer>
          <Column scroll padding borderLeft>
            <Row>
              <Column>
                <Heading top>Reservoir name here</Heading>
                <FormRow>
                  <Field label="Name">
                    <Input value="Reservoir name here" width="200px" />
                  </Field>
                  <Field label="Lorem">
                    <RadioButton
                      name="example"
                      options={[
                        { label: 'Aardvarks', value: 'termites' },
                        { label: 'Monkeys', value: 'bananas' },
                      ]}
                      value={{ label: 'Monkeys', value: 'bananas' }}
                      inline
                    />
                  </Field>
                  <Field label="Ipsum">
                    <Select
                      name="example"
                      options={[
                        { label: 'Monkeys', value: 'bananas' },
                        { label: 'Possums', value: 'slugs' },
                      ]}
                      value={{ label: 'Monkeys', value: 'bananas' }}
                      native
                    />
                  </Field>
                  <Field>
                    <CheckBox label="Dolor" />
                  </Field>
                </FormRow>
              </Column>
              <Column flex={false}>
                <Button onClick={() => {}} round icon={<FaQuestion />} />
              </Column>
            </Row>
            <Spacer height={10} />
            <Row>
              <Column width="50%">
                <Card
                  heading={
                    <Heading onClickHelp={() => console.log('help!')}>
                      Reservoir Zone
                    </Heading>
                  }
                >
                  <Row>
                    <Column width="50%">
                      <Field label="Label" helpText="Lorem ipsum">
                        <RadioButton
                          options={[
                            { label: 'Aardvarks', value: 'termites' },
                            { label: 'Monkeys', value: 'bananas' },
                          ]}
                          value={{ label: 'Monkeys', value: 'bananas' }}
                          inline
                        />
                        <Input width="100%" />
                      </Field>
                      <Field label="Label" helpText="Lorem ipsum">
                        <CheckBox label="Lorem ipsum" />
                        <Input width="100%" disabled />
                      </Field>

                      <Field label="Label" helpText="Lorem ipsum">
                        <Input width="100%" />
                      </Field>
                      <Field label="Label" helpText="Lorem ipsum">
                        <Input width="100%" />
                      </Field>
                    </Column>
                    <Column width="50%">
                      <Field label="Label" helpText="Lorem ipsum">
                        <Input width="100%" />
                      </Field>
                      <Field label="Label" helpText="Lorem ipsum">
                        <Input width="100%" />
                      </Field>
                      <Field label="Label" helpText="Lorem ipsum">
                        <Input width="100%" />
                      </Field>
                      <Field label="Label" helpText="Lorem ipsum">
                        <Input width="100%" />
                      </Field>
                    </Column>
                  </Row>
                </Card>
              </Column>
              <Column width="50%">
                <Card heading={<Heading>Reservoir Size</Heading>}>
                  <Row>
                    <Column width="160px">
                      <Field label="Length">
                        <Input width="100%" />
                      </Field>
                      <Field label="Width">
                        <Input width="100%" />
                      </Field>
                    </Column>
                    <Column>
                      <ImagePlaceholder width="100%" height="100%" />
                    </Column>
                  </Row>
                </Card>
                <Spacer />
                <Card heading={<Heading>Reservoir Properties</Heading>}>
                  <Row>
                    <Column>
                      <Field label="Label">
                        <Input width="100%" />
                      </Field>
                    </Column>
                    <Column>
                      <Field label="Label">
                        <Input width="100%" />
                      </Field>
                    </Column>
                    <Column>
                      <Field label="Label">
                        <Input width="100%" />
                      </Field>
                    </Column>
                  </Row>
                </Card>
              </Column>
            </Row>
          </Column>
        </Row>
      </Page>
    </>
  );
});
