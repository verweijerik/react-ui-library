import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaChevronLeft } from 'react-icons/fa';
import {
  Accordion,
  TopBar,
  SideBar,
  Heading,
  Button,
  Drawer,
  List,
  Field,
  Input,
  InputGroup,
  CheckBox,
  Page,
  Row,
  Column,
  Spacer,
  Tabs,
} from '../../../..';

const options = [
  {
    label: 'Import',
    value: 'import',
    right: true,
  },
  {
    label: 'Settings',
    value: 'settings',
    right: true,
  },
];

const MapPlaceholder = () => (
  <div
    style={{
      background: '#AAD3DF',
      borderLeft: '1px solid #8AB3BF',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: 'white',
      height: '100%',
    }}
  >
    MAP
  </div>
);

storiesOf('Layout/Layout Examples', module).add('Site', () => {
  const [open, setOpen] = useState(true);
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={false} scroll={false}>
        <Row spacing={0} flex>
          <Drawer
            button={
              <Button
                onClick={() => setOpen(!open)}
                round
                icon={<FaChevronLeft />}
              />
            }
            open={open}
            width={300}
            closedWidth={50}
          >
            <List
              drawer
              narrow={!open}
              list={{
                name: 'Sites',
                items: [
                  {
                    id: 1,
                    name: 'Main Site',
                    type: 'Heading',
                  },
                  {
                    id: 2,
                    name: 'Site name here',
                    active: true,
                    label: {
                      value: 1,
                      color: '#69779b',
                    },
                  },
                  {
                    id: 3,
                    name: 'Locations',
                    type: 'Heading',
                  },
                  {
                    id: 4,
                    name: 'Ipsum',
                    label: {
                      value: 2,
                      color: '#69779b',
                    },
                  },
                  {
                    id: 5,
                    name: 'Dolor',
                    label: {
                      value: 3,
                      color: '#69779b',
                    },
                  },
                ],
              }}
            />
            <List
              drawer
              narrow={!open}
              list={{
                name: 'Wells',
                items: [
                  {
                    id: 1,
                    name: 'Main Wellbore',
                    type: 'Heading',
                  },
                  {
                    id: 2,
                    name: 'Lorem',
                    label: {
                      value: 1,
                      color: '#77699b',
                    },
                  },
                ],
              }}
            />
            <List
              drawer
              narrow={!open}
              list={{
                name: 'Targets',
                items: [
                  {
                    id: 1,
                    name: 'Lorem',
                    label: {
                      value: 1,
                      color: '#9b6977',
                    },
                  },
                ],
              }}
            />
          </Drawer>
          <Column borderLeft flexbox>
            <Tabs padding margin={false} options={options} />
            <Row spacing={0} flex>
              <Column scroll padding width={550}>
                <Heading top>Site name here</Heading>
                <Field label="Name">
                  <Input value="Site name here" width="200px" />
                </Field>
                <Spacer />
                <Accordion
                  heading={<Heading>Coordinate Reference System</Heading>}
                  managed
                >
                  <Field label="Label" labelLeft labelWidth="50px">
                    <Input value="Value" width="100%" />
                  </Field>
                  <Field label="Label" labelLeft labelWidth="50px">
                    <Input value="Value" width="100%" />
                  </Field>
                  <Field label="Label" labelLeft labelWidth="50px">
                    <Input value="Value" width="100%" />
                  </Field>
                  <Field>
                    <CheckBox label="Dolor" />
                  </Field>
                </Accordion>
                <Spacer />
                <Heading marginBottom={10}>Location</Heading>
                <Row spacing={10}>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                </Row>
                <Spacer />
                <Heading marginBottom={10}>Model</Heading>
                <Row spacing={10}>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                  <Column>
                    <Field label="Label">
                      <Input value="Value" width="100%" />
                    </Field>
                  </Column>
                </Row>
                <Spacer />
                <Accordion heading={<Heading>Settings</Heading>} managed>
                  <Field>
                    <CheckBox label="Dolor" />
                  </Field>
                  <Row spacing={10}>
                    <Column>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                    </Column>
                    <Column>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                      <Field label="Label">
                        <Input value="Value" width="100%" />
                      </Field>
                    </Column>
                  </Row>
                </Accordion>
              </Column>
              <Column>
                <MapPlaceholder />
              </Column>
            </Row>
          </Column>
        </Row>
      </Page>
    </>
  );
});
