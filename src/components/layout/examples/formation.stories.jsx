import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import {
  TopBar,
  SideBar,
  Heading,
  Table,
  Tabs,
  Page,
  Row,
  Spacer,
  Column,
} from '../../../..';

const ChartPlaceholder = () => (
  <div
    style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      background: 'rgba(0,0,0,0.05)',
      height: '100%',
      minHeight: 400,
      textAlign: 'center',
    }}
  >
    CHART <br />
    (fills height, minHeight 400px)
  </div>
);

storiesOf('Layout/Layout Examples', module).add('Formation', () => {
  const options = [
    {
      label: 'Monkeys',
      value: 'monkeys',
    },
    {
      label: 'Bananas',
      value: 'bananas',
    },
    {
      label: 'Squirrels',
      value: 'squirrels',
    },
    {
      label: 'Pomegranates',
      value: 'pomegranates',
    },
  ];
  const [selectedTab, setSelectedTab] = useState(options[0]);
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding="16px 20px" scroll>
        <Heading top marginBottom={0}>
          Formation Inputs
        </Heading>
        <Tabs
          margin={false}
          options={options}
          value={selectedTab}
          onChange={(evt) => {
            const { value, label } = evt.target;
            setSelectedTab({ value, label });
          }}
        />
        <Spacer height={20} />
        <Row flex>
          <Column width={400}>
            <Table
              table={{
                headers: [
                  {
                    cells: [{ value: 'Name' }, { value: 'Origin' }],
                  },
                ],
                rows: [
                  {
                    cells: [{ value: 'Brown rice' }, { value: 'Vietnam' }],
                  },
                  {
                    cells: [{ value: 'Buckwheat' }, { value: 'Poland' }],
                  },
                  {
                    cells: [{ value: 'Couscous' }, { value: 'France' }],
                  },
                ],
              }}
            />
          </Column>
          <Column>
            <ChartPlaceholder />
          </Column>
        </Row>
      </Page>
    </>
  );
});
