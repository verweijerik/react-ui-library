import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { FaChevronLeft } from 'react-icons/fa';
import {
  Page,
  Column,
  Row,
  FormRow,
  TopBar,
  SideBar,
  Heading,
  Button,
  Drawer,
  List,
  Field,
  Input,
  Select,
  CheckBox,
  Tabs,
  Accordion,
  InputGroup,
  InputGroupAddon,
  Spacer,
} from '../../../..';

const options = [
  {
    label: 'Settings',
    value: 'settings',
  },
  {
    label: 'Simulation Results',
    value: 'results',
  },
];
const casingOptions = [
  '30, Conductor, Casing',
  '20, Surface, Casing',
  '13 3/8, Intermediate, Casing',
  '11 3/4, Surface, Casing',
  '9 5/8, Production, Casing',
];

storiesOf('Layout/Layout Examples', module).add('AFE Simulation', () => {
  const [open, setOpen] = useState(true);
  const [selectedTab, setSelectedTab] = useState(options[0]);
  const [selectedCasingOption, setSelectedCasingOption] = useState(
    casingOptions[0],
  );
  return (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={false} scroll={false}>
        <Row spacing={0} height="100%">
          <Drawer
            button={
              <Button
                onClick={() => setOpen(!open)}
                round
                icon={<FaChevronLeft />}
              />
            }
            open={open}
            width={300}
            closedWidth={50}
          >
            <List
              drawer
              narrow={!open}
              list={{
                name: 'AFE Simulation',
                items: [
                  {
                    id: 1,
                    name: 'Simulation name here',
                    active: true,
                  },
                  {
                    id: 2,
                    name: 'Ipsum',
                  },
                ],
              }}
            />
          </Drawer>
          <Column scroll padding borderLeft>
            <Heading top>Simulation name here</Heading>
            <Tabs
              options={options}
              value={selectedTab}
              onChange={(evt) => {
                const { value, label } = evt.target;
                setSelectedTab({ value, label });
              }}
            />
            <FormRow>
              <Field label="Name">
                <Input value="Simulation name here" width="200px" />
              </Field>
              <Field label="Initial condition">
                <Select
                  options={[
                    {
                      label: 'E3 - Circulation through drillpipe',
                      value: 'e3',
                    },
                  ]}
                  value={{
                    label: 'E3 - Circulation through drillpipe',
                    value: 'e3',
                  }}
                  native
                />
              </Field>
            </FormRow>
            <Spacer />
            {casingOptions.map((option) => (
              <div key={option}>
                <Accordion
                  heading={<Heading>{option}</Heading>}
                  managed
                  bordered
                >
                  <FormRow>
                    <Field label="Leak off @TVD 425">
                      <CheckBox label="Use formation data" checked />
                    </Field>
                    <Field>
                      <InputGroup>
                        <Input
                          width="200px"
                          disabled
                          value="1.532142857142857"
                        />
                        <InputGroupAddon>m</InputGroupAddon>
                      </InputGroup>
                    </Field>
                    <Field>
                      <CheckBox label="Gas cap" />
                    </Field>
                    <Field>
                      <CheckBox label="Max allowable pressure buildup" />
                    </Field>
                  </FormRow>
                </Accordion>
                <Spacer height={5} />
              </div>
            ))}
            <Spacer />
            <Field label="Final condition">
              <Select
                options={[
                  {
                    label: 'Undisturbed',
                    value: 'Undisturbed',
                  },
                ]}
                value={{
                  label: 'Undisturbed',
                  value: 'Undisturbed',
                }}
                native
                width="auto"
              />
            </Field>
            <Button label="Simulate" colored />
          </Column>
        </Row>
      </Page>
    </>
  );
});
