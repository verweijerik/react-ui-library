import React, { Fragment, isValidElement } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import styles from './row.module.less';

export const Row = ({
  alignItems,
  children,
  flex,
  height,
  marginBottom,
  marginTop,
  spacing,
  wrap,
}) => {
  const childElements =
    children instanceof Array
      ? children.filter((c) => isValidElement(c))
      : [children];
  const columns = childElements.map((item, index) => (
    <Fragment key={index}>{React.cloneElement(item, { spacing })}</Fragment>
  ));

  return (
    <div
      className={cx(styles.row)}
      style={{
        alignItems,
        flexGrow: flex ? 1 : 0,
        flexShrink: flex ? 1 : 0,
        flexWrap: wrap ? 'wrap' : 'nowrap',
        height,
        marginLeft: spacing * -0.5,
        marginRight: spacing * -0.5,
        marginBottom,
        marginTop,
      }}
    >
      {columns}
    </div>
  );
};

Row.defaultProps = {
  alignItems: 'initial',
  children: null,
  flex: false,
  height: 'auto',
  marginBottom: '0',
  marginTop: '0',
  spacing: 20,
  wrap: false,
};

Row.propTypes = {
  alignItems: PropTypes.string,
  children: PropTypes.node,
  flex: PropTypes.bool,
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  marginBottom: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  marginTop: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  spacing: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  wrap: PropTypes.bool,
};
