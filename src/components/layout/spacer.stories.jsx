import React from 'react';
import { storiesOf } from '@storybook/react';
import { Spacer, Divider } from '../../..';
import { Container } from '../../helpers/container';

storiesOf('Layout/Spacer', module)
  .add('Default', () => (
    <Container>
      <strong>Default (20px)</strong>
      <Spacer />
      Lorem ipsum
    </Container>
  ))
  .add('Custom height', () => (
    <Container>
      <strong>height=&quot;100px&quot;</strong>
      <Spacer height="100px" />
      Lorem ipsum
    </Container>
  ))
  .add('Horizontal', () => (
    <Container>
      <div style={{ display: 'flex' }}>
        <strong>width=&quot;100px&quot;</strong>
        <Spacer width="100px" />
        Lorem ipsum
      </div>
      <Divider />
      <div style={{ display: 'flex' }}>
        <strong>flex</strong>
        <Spacer flex />
        Lorem ipsum
      </div>
    </Container>
  ));
