import styled, { css } from 'styled-components';

const noScrollbars = css`
  scrollbar-width: none;
  -ms-overflow-style: none;
  &::-webkit-scrollbar {
    display: none;
  }
`;

export const StyledColumn = styled.div`
  position: relative; //for react-laag
  display: flex;
  flex-direction: column;
  min-width: 0;
  flex-basis: ${(p) => p.width || 'auto'};
  flex-grow: ${(p) => (!p.width && p.flex ? '1' : '0')};
  flex-shrink: ${(p) => (p.width || !p.flex ? '0' : '1')};
  overflow: ${(p) => (p.scroll ? 'auto' : 'initial')};
  background: ${(p) => p.background};
  border-left: ${(p) =>
    p.borderLeft === true ? '1px solid #ddd' : p.borderLeft};
  border-right: ${(p) =>
    p.borderRight === true ? '1px solid #ddd' : p.borderRight};
  padding: ${(p) => (p.spacing ? `0 ${p.spacing / 2}px` : '0')};
  ${(p) => !p.showScrollbar && noScrollbars}

  @media (max-width: 992px) {
    ${(p) => p.widthTablet && `flex-basis: ${p.widthTablet};`}
  }

  @media (max-width: 575px) {
    ${(p) => p.widthMobile && `flex-basis: ${p.widthMobile};`}
  }

  > .inner {
    flex-basis: 100%;
    position: relative;
    min-height: 0;
    display: ${(p) => (p.flexbox && !p.scroll ? 'flex' : 'block')};
    ${(p) => p.flexbox && `flex-direction: column;`}
    padding: ${(p) => (p.padding === true ? '16px 20px' : p.padding)};
  }
`;
