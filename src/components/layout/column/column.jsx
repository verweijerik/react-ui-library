import React from 'react';
import PropTypes from 'prop-types';
import { StyledColumn } from './styles.js';

export const Column = ({
  background,
  borderLeft,
  borderRight,
  children,
  flex,
  flexbox,
  padding,
  scroll,
  showScrollbar,
  spacing,
  width,
  widthMobile,
  widthTablet,
}) => {
  const getWidthString = (w) => {
    return typeof w === 'string'
      ? w
      : typeof w === 'number'
      ? `${w}px`
      : undefined;
  };

  return (
    <StyledColumn
      background={background}
      borderLeft={borderLeft}
      borderRight={borderRight}
      flex={flex}
      flexbox={flexbox}
      padding={padding}
      scroll={scroll}
      showScrollbar={showScrollbar}
      spacing={spacing}
      width={getWidthString(width)}
      widthTablet={getWidthString(widthTablet)}
      widthMobile={getWidthString(widthMobile)}
    >
      <div className="inner">{children}</div>
    </StyledColumn>
  );
};

Column.defaultProps = {
  background: 'transparent',
  borderLeft: null,
  borderRight: null,
  children: null,
  flex: true,
  flexbox: false,
  padding: false,
  scroll: false,
  showScrollbar: true,
  spacing: 20,
  width: null,
  widthMobile: null,
  widthTablet: null,
};

Column.propTypes = {
  background: PropTypes.string,
  borderLeft: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  borderRight: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  children: PropTypes.node,
  flex: PropTypes.bool,
  flexbox: PropTypes.bool,
  padding: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  scroll: PropTypes.bool,
  showScrollbar: PropTypes.bool,
  spacing: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  widthMobile: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  widthTablet: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
