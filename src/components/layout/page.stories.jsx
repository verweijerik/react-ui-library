import React from 'react';
import { storiesOf } from '@storybook/react';
import { TopBar, SideBar, Heading, Page, PrintHeader } from '../../..';
import { placeholder } from './placeholder';
import LogoSVG from '../../images/logo.svg';

storiesOf('Layout/Page', module)
  .add('Default', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page>
        <Heading top>Heading</Heading>
        {placeholder}
      </Page>
    </>
  ))
  .add('Custom padding', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page padding={false}>
        <Heading top>Heading</Heading>
        {placeholder}
      </Page>
    </>
  ))
  .add('No scroll', () => (
    <>
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page scroll={false}>
        <Heading top>Heading</Heading>
        {placeholder}
      </Page>
    </>
  ))
  .add('With print header', () => (
    <>
      <PrintHeader logo={LogoSVG} logoWidth="60px" alt="company logo" />
      <TopBar />
      <SideBar options={{ title: '', sections: [] }} />
      <Page>
        <Heading top>Heading</Heading>
        {placeholder}
      </Page>
    </>
  ))
  .add('No sidebar', () => (
    <>
      <TopBar />
      <Page left={0} padding>
        <Heading top>Heading</Heading>
        {placeholder}
      </Page>
    </>
  ))
  .add('No topbar', () => (
    <>
      <SideBar options={{ title: '', sections: [] }} />
      <Page top={0} padding>
        <Heading top>Heading</Heading>
        {placeholder}
      </Page>
    </>
  ));
