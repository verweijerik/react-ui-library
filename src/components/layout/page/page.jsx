import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import styles from './page.module.less';

export const Page = ({ children, left, padding: paddingProp, scroll, top }) => {
  const padding =
    typeof paddingProp === 'string'
      ? paddingProp
      : paddingProp === true
      ? '20px'
      : '0';
  return (
    <div
      className={cx(styles.page, scroll ? styles.scroll : '')}
      style={{ left, padding, top }}
    >
      {children}
    </div>
  );
};

Page.defaultProps = {
  children: null,
  padding: true,
  left: '70px',
  scroll: true,
  top: '60px',
};

Page.propTypes = {
  children: PropTypes.node,
  padding: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.number,
  ]),
  left: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  scroll: PropTypes.bool,
  top: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};
