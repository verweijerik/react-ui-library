import React from 'react';
import PropTypes from 'prop-types';
import styles from './form-row.module.less';

export const FormRow = ({ children }) => {
  return <div className={styles.formRow}>{children}</div>;
};

FormRow.defaultProps = {
  children: null,
};

FormRow.propTypes = {
  children: PropTypes.node,
};
