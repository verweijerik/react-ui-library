import React from 'react';
import PropTypes from 'prop-types';
import styles from './print-header.module.less';

export const PrintHeader = ({ logo, logoWidth, alt }) => (
  <img
    src={logo}
    alt={alt}
    className={styles.printHeader}
    style={{ width: logoWidth }}
  />
);

PrintHeader.defaultProps = {
  logoWidth: '100px',
};

PrintHeader.defaultProps = {
  logo: PropTypes.node.isRequired,
  logoWidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  alt: PropTypes.string.isRequired,
};
