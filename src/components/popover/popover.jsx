import React from 'react';
import PropTypes from 'prop-types';
import ResizeObserver from 'resize-observer-polyfill';
import cx from 'classnames';
import { Arrow, useToggleLayer } from 'react-laag';
import styles from './popover.module.less';

export const Popover = ({
  children,
  content,
  placement,
  closeOnOutsideClick,
  fullWidth,
}) => {
  const [element, toggleLayerProps] = useToggleLayer(
    ({ isOpen, layerProps, arrowStyle, layerSide, close }) =>
      isOpen && (
        <div
          className={
            fullWidth
              ? cx(styles.toggleBox, styles.fullWidthStyleFix)
              : cx(styles.toggleBox)
          }
          {...layerProps} // eslint-disable-line react/jsx-props-no-spreading
        >
          {React.cloneElement(content, { close })}
          <Arrow
            style={arrowStyle}
            layerSide={layerSide}
            backgroundColor="whitesmoke"
            borderColor="#ccc"
            borderWidth={1}
            size={6}
          />
        </div>
      ),
    {
      placement: {
        anchor: placement,
        autoAdjust: true,
        triggerOffset: 6,
      },
      closeOnOutsideClick,
      ResizeObserver,
    },
  );

  const { isOpen, close, openFromMouseEvent } = toggleLayerProps;

  return (
    <>
      {element}
      <div
        onClick={isOpen ? close : openFromMouseEvent}
        className={cx(styles.childrenContainer)}
      >
        {children}
      </div>
    </>
  );
};

Popover.propTypes = {
  content: PropTypes.node,
  closeOnOutsideClick: PropTypes.bool,
  placement: PropTypes.string, //https://www.react-laag.com/docs/togglelayer/
  fullWidth: PropTypes.bool,
};

Popover.defaultProps = {
  content: null,
  closeOnOutsideClick: true,
  placement: 'TOP_CENTER',
  fullWidth: false,
};
