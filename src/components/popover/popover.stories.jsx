import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Button, Input, Popover, Select, InputGroup } from '../../..';

const style = {
  display: 'flex',
  flex: 1,
  width: '100%',
  height: '100vh',
  alignItems: 'center',
  justifyContent: 'center',
};

const options = [
  { label: 'Option 1', value: '1' },
  { label: 'Option 2', value: '2' },
];

const InputForm = ({ close }) => (
  <>
    <InputGroup small>
      <Input value="Value" width="150px" />
      <Button colored label="Save" onClick={() => {}} />
      <Button label="Cancel" onClick={close} />
    </InputGroup>
  </>
);

const SelectForm = ({ close }) => (
  <InputGroup small>
    <Select menu={options} value={options[0].value} width="150px" />
    <Button colored label="Save" onClick={() => {}} />
    <Button label="Cancel" onClick={close} />
  </InputGroup>
);

storiesOf('Basic/Popover', module)
  .add('triggered by Span', () => (
    <Container style={style}>
      <Popover content={<SelectForm />}>
        <span>Toggle me</span>
      </Popover>
    </Container>
  ))
  .add('triggered by Button', () => (
    <Container style={style}>
      <Popover content={<SelectForm />}>
        <Button colored label="Toggle me" onClick={() => {}} />
      </Popover>
    </Container>
  ))
  .add('triggered by Input', () => (
    <Container style={style}>
      <Popover content={<InputForm />}>
        <Input value="Value" width="100%" />
      </Popover>
    </Container>
  ))
  .add('triggered by Input with error', () => (
    <Container style={style}>
      <Popover content={<InputForm />}>
        <Input value="Value" width="100%" error="Error" />
      </Popover>
    </Container>
  ));
