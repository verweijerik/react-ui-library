import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Card } from './card';
import { Heading } from '../heading/heading';

const style = {
  height: '500px',
  width: '400px',
};

storiesOf('Layout/Card', module)
  .add('default', () => (
    <Container style={style}>
      <Card>Example content</Card>
    </Container>
  ))
  .add('raised', () => (
    <Container style={style}>
      <Card raised>Example content</Card>
    </Container>
  ))
  .add('with heading', () => (
    <Container style={style}>
      <Card heading={<Heading>Heading</Heading>}>Example content</Card>
    </Container>
  ))
  .add('with help heading', () => (
    <Container style={style}>
      <Card
        heading={
          <Heading onClickHelp={() => console.log('help!')}>Heading</Heading>
        }
      >
        Example content
      </Card>
    </Container>
  ))
  .add('with no padding', () => (
    <Container style={style}>
      <Card heading={<Heading>Heading</Heading>} padding={false}>
        Example content
      </Card>
    </Container>
  ))
  .add('without border', () => (
    <Container style={style}>
      <Card
        heading={<Heading>Heading</Heading>}
        padding={false}
        bordered={false}
      >
        Example content
      </Card>
    </Container>
  ));
