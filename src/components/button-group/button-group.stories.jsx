import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Container } from '../../helpers/container';
import { ButtonGroup } from './button-group';

const style = {
  height: '500px',
  width: '500px',
};

//Prefer this way of using ButtonGroup:
const buttonsArrayOfObjects = [
  { key: 0, label: 'Aardvarks', value: 'termites' },
  { key: 1, label: 'Kangaroos', value: 'grass' },
  { key: 2, label: 'Monkeys', value: 'bananas' },
  { key: 3, label: 'Pandas', value: 'bamboo', disabled: true },
  { key: 4, label: 'Possums', value: 'slugs', hidden: true },
];

//For simple cases when button key, label and value are the same, it's okay to use this:
const buttonsArrayOfStrings = ['One', 'Two', 'Three'];

//Avoid this deprecated legacy usage (still supported, but may be removed in future):
const buttonsArrayOfArrays = [
  ['first_item', 'One'],
  ['second_item', 'Two'],
  ['third_item', 'Three'],
];

const buttonsWithIcons = [
  {
    key: 0,
    label: 'Sort',
    value: 'upload',
    icon: 'sort amount down',
  },
  { key: 1, label: 'Delete', value: 'delete', icon: 'trash' },
  { key: 2, label: 'Help', value: 'help', icon: 'question' },
];

const onItemSelected = (key, value, label) => {
  action('Menu item selected')(key);
};

storiesOf('Forms/ButtonGroup', module)
  .add('default', () => (
    <Container style={style}>
      <div style={{ marginBottom: '20px' }}>
        <ButtonGroup
          items={buttonsArrayOfObjects}
          header="Array of objects (preferred)"
          onSelected={onItemSelected}
          value={buttonsArrayOfObjects[1].key}
        />
      </div>
      <div style={{ marginBottom: '20px' }}>
        <ButtonGroup
          items={buttonsArrayOfStrings}
          header="Array of strings"
          onSelected={onItemSelected}
          value={buttonsArrayOfStrings[1]}
        />
      </div>
      <div>
        <ButtonGroup
          items={buttonsArrayOfArrays}
          header="Array of arrays (deprecated)"
          onSelected={onItemSelected}
          value={buttonsArrayOfArrays[1][0]}
        />
      </div>
    </Container>
  ))
  .add('with icons', () => (
    <Container style={style}>
      <ButtonGroup
        items={buttonsWithIcons}
        onSelected={onItemSelected}
        value={buttonsWithIcons[2].key}
      />
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style}>
      <ButtonGroup
        items={buttonsArrayOfStrings}
        onSelected={onItemSelected}
        value={buttonsArrayOfStrings[1]}
        disabled
      />
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <ButtonGroup
        items={buttonsArrayOfStrings}
        onSelected={onItemSelected}
        value={buttonsArrayOfStrings[1]}
        small
      />
    </Container>
  ));
