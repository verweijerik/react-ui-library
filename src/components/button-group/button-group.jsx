import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Button } from '../button/button';
import styles from './button-group.module.less';

export class ButtonGroup extends React.PureComponent {
  onClick = (evt, key, value, label) => {
    evt.preventDefault();
    evt.stopPropagation();
    this.props.onSelected(key, value, label);
  };

  renderIcon = (icon) => {
    if (icon) {
      return <i className={cx('icon', icon)} />;
    }
  };

  renderButtons = () => {
    const buttons = this.props.items;
    return buttons.map((button, i) => {
      const type = Array.isArray(button) ? 'array' : typeof button;
      const groupOrder =
        i === 0 ? 'first' : i === buttons.length - 1 ? 'last' : 'middle';
      const label =
        type === 'string'
          ? button
          : type === 'object'
          ? button.label
          : type === 'array'
          ? button[1]
          : null;
      const value =
        type === 'string'
          ? button
          : type === 'object'
          ? button.value
          : type === 'array'
          ? button[0]
          : null;
      const key = type === 'object' ? button.key : value;
      const hidden = type === 'object' ? button.hidden : false;
      const icon = type === 'object' ? button.icon : false;
      const active =
        type === 'object'
          ? key === this.props.value
          : value === this.props.value;
      if (!hidden) {
        return (
          <Button
            active={active}
            basic={this.props.basic}
            disabled={this.props.disabled || button.disabled}
            groupOrder={groupOrder}
            icon={icon}
            key={i}
            label={label}
            onClick={(evt) => this.onClick(evt, key, value, label)}
            small={this.props.small}
          />
        );
      }
    });
  };

  renderHeader = () => {
    if (this.props.header) {
      return <label className={styles.label}>{this.props.header}</label>;
    }
  };

  render() {
    return (
      <div>
        <div>
          {this.renderHeader()}
          <div className={styles.buttonGroup}>{this.renderButtons()}</div>
        </div>
      </div>
    );
  }
}

ButtonGroup.defaultProps = {
  basic: false,
  items: [],
  header: '',
  onSelected: () => {},
  small: false,
  value: '',
};

ButtonGroup.propTypes = {
  basic: PropTypes.bool, // Deprecated?
  items: PropTypes.array,
  header: PropTypes.string,
  onSelected: PropTypes.func,
  small: PropTypes.bool,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
