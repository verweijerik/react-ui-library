import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './textarea.module.less';
import { Tooltip } from '../tooltip/tooltip';
import { isStringNumberOrNode } from '../../helpers/types';

export const TextArea = ({
  name,
  value,
  placeholder,
  cols,
  rows,
  disabled,
  onChange,
  onKeyPress,
  onFocus,
  onBlur,
  tabIndex,
  error,
  warning,
  tooltip,
  maxTooltipWidth,
  resize,
}) => {
  const textarea = (
    <textarea
      className={cx(
        styles.textarea,
        error ? styles.error : '',
        warning ? styles.warning : '',
      )}
      name={name}
      value={value}
      placeholder={placeholder}
      cols={cols}
      rows={rows}
      onChange={onChange}
      disabled={disabled}
      onKeyPress={onKeyPress}
      onFocus={onFocus}
      onBlur={onBlur}
      tabIndex={tabIndex}
      style={{ resize }}
    />
  );

  return (
    <Tooltip
      error={!!error}
      warning={!!warning}
      text={tooltip || error || warning}
      enabled={
        (tooltip && isStringNumberOrNode(tooltip)) ||
        (error && isStringNumberOrNode(error)) ||
        (warning && isStringNumberOrNode(warning)) ||
        false
      }
      maxWidth={maxTooltipWidth}
    >
      {textarea}
    </Tooltip>
  );
};

TextArea.defaultProps = {
  error: null,
  warning: null,
  tooltip: null,
  name: undefined,
  onChange: () => {},
  onKeyPress: () => {},
  onFocus: () => {},
  onBlur: () => {},
  placeholder: '',
  tabIndex: 0,
  value: '',
  disabled: false,
  cols: undefined,
  rows: undefined,
  resize: undefined,
  maxTooltipWidth: undefined,
};

TextArea.propTypes = {
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  warning: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  tooltip: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.node,
  ]),
  name: PropTypes.string,
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  placeholder: PropTypes.string,
  tabIndex: PropTypes.number,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  disabled: PropTypes.bool,
  cols: PropTypes.number,
  rows: PropTypes.number,
  resize: PropTypes.string,
  maxTooltipWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
