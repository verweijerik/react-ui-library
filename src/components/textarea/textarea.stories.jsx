import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { TextArea } from './textarea';

const style = {
  height: '500px',
  width: '500px',
};

const sectionStyle = {
  marginBottom: 30,
};

storiesOf('Forms/TextArea', module)
  .add('default', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <TextArea name="example" onChange={() => {}} />
      </div>
    </Container>
  ))
  .add('with value', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <TextArea name="example" value="Example" onChange={() => {}} />
      </div>
    </Container>
  ))
  .add('with placeholder', () => (
    <Container style={style}>
      <div style={sectionStyle}>
        <TextArea
          name="example"
          placeholder="Placeholder"
          onChange={() => {}}
        />
      </div>
    </Container>
  ))
  .add('disabled', () => (
    <Container style={style}>
      <TextArea name="example" value="Example" onChange={() => {}} disabled />
    </Container>
  ))
  .add('sized', () => (
    <Container style={style}>
      <TextArea
        name="example"
        value="Example"
        onChange={() => {}}
        cols={40}
        rows={10}
      />
    </Container>
  ))
  .add('not resizeable', () => (
    <Container style={style}>
      <TextArea
        name="example"
        value="Example"
        onChange={() => {}}
        cols={40}
        rows={10}
        resize="none"
      />
    </Container>
  ))
  .add('with tooltip', () => (
    <Container style={style}>
      <TextArea
        name="example"
        value="Example"
        onChange={() => {}}
        tooltip="Some info"
      />
    </Container>
  ))
  .add('with error', () => (
    <Container style={style}>
      <TextArea
        name="example"
        value="Example"
        onChange={() => {}}
        error="Error message goes here"
      />
    </Container>
  ))
  .add('with warning', () => (
    <Container style={style}>
      <TextArea
        name="example"
        value="Example"
        onChange={() => {}}
        warning="Warning message goes here"
      />
    </Container>
  ))
  .add('managed', () => {
    const [value, setValue] = useState('Example');
    return (
      <Container style={style}>
        <TextArea
          name="example"
          value={value}
          onChange={(evt) => setValue(evt.target.value)}
        />
      </Container>
    );
  });
