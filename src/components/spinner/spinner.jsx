import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './spinner.module.less';

export const Spinner = ({ small, colored, dark, tiny }) => {
  return (
    <div
      className={cx(
        styles.spinner,
        small ? styles.small : '',
        tiny ? styles.tiny : '',
        dark ? styles.dark : '',
        colored ? styles.colored : '',
      )}
    >
      <div />
      <div />
      <div />
      <div />
    </div>
  );
};

Spinner.defaultProps = {
  colored: false,
  dark: false,
  small: false,
  tiny: false,
};

Spinner.propTypes = {
  colored: PropTypes.bool,
  dark: PropTypes.bool,
  small: PropTypes.bool,
  tiny: PropTypes.bool,
};
