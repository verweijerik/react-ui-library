import React from 'react';
import { storiesOf } from '@storybook/react';
import { Container } from '../../helpers/container';
import { Spinner } from './spinner.jsx';

const style = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  height: '100px',
  width: '100px',
  backgroundColor: '#262626',
};

storiesOf('Progress/Spinner', module)
  .add('default', () => (
    <Container style={style}>
      <Spinner />
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <Spinner small />
    </Container>
  ))
  .add('tiny', () => (
    <Container style={style}>
      <Spinner tiny />
    </Container>
  ))
  .add('dark', () => (
    <Container>
      <div style={{ ...style, backgroundColor: '#E0E1E2' }}>
        <Spinner dark />
      </div>
      <div style={{ ...style, backgroundColor: '#E0E1E2' }}>
        <Spinner dark small />
      </div>
      <div style={{ ...style, backgroundColor: '#E0E1E2' }}>
        <Spinner dark tiny />
      </div>
    </Container>
  ))
  .add('colored', () => (
    <Container>
      <div style={{ ...style, backgroundColor: 'white' }}>
        <Spinner colored />
      </div>
      <div style={{ ...style, backgroundColor: 'white' }}>
        <Spinner colored small />
      </div>
      <div style={{ ...style, backgroundColor: 'white' }}>
        <Spinner colored tiny />
      </div>
    </Container>
  ));
