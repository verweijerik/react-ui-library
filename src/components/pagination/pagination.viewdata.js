/*
 Borrowed from: https://gist.github.com/kottenator/9d936eb3e4e3c3e02598
 */
export const generatePageRange = (currentPage, lastPage, delta = 1) => {
  const range = Array(lastPage)
    .fill(null)
    .map((_, index) => index + 1);

  return range.reduce((pages, page) => {
    // allow adding of first and last pages
    if (page === 1 || page === lastPage) {
      return [...pages, page];
    }

    // if within delta range add page
    if (page - delta <= currentPage && page + delta >= currentPage) {
      return [...pages, page];
    }

    // otherwise add gap if gap was not the last item added.
    if (pages[pages.length - 1] !== '...') {
      return [...pages, '...'];
    }

    return pages;
  }, []);
};

export const selectedRowsPerPage = (options, value) => {
  return (
    value ||
    (options &&
      options.reduce((acc, row) => {
        return row.selected ? row.value : acc;
      }, 10)) ||
    10
  );
};
