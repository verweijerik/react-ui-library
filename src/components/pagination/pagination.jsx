import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Select } from '../select/select';
import { Input } from '../input/input';
import { ButtonGroup } from '../button-group/button-group';
import { generatePageRange, selectedRowsPerPage } from './pagination.viewdata';
import styles from './pagination.module.less';

export const Pagination = (props) => {
  const {
    rowCount,
    selectedPage,
    small,
    onSelectPage,
    rowsPerPage,
  } = props.pagination;

  const [goTo, setGoTo] = useState('');

  const numberOfPages = rowsPerPage
    ? Math.ceil(
        rowCount / selectedRowsPerPage(rowsPerPage.options, rowsPerPage.value),
      )
    : 10;
  const pages = generatePageRange(selectedPage, numberOfPages).map((p) => ({
    key: p,
    label: p.toString(),
    value: p,
    disabled: p === '...',
  }));

  return (
    <div className={styles.paginationContainer}>
      {rowsPerPage ? (
        <div style={{ flexShrink: 0 }}>
          <Select
            onChange={rowsPerPage.onChange}
            options={rowsPerPage.options}
            native={!!rowsPerPage.options}
            small={props.pagination.small}
            width="auto"
            value={rowsPerPage.value}
          />
        </div>
      ) : null}
      <div className={styles.pagination}>
        <ButtonGroup
          items={pages}
          value={selectedPage}
          small={small}
          basic
          onSelected={onSelectPage}
        />
      </div>
      <Input
        value={goTo}
        onChange={(evt) => setGoTo(evt.target.value)}
        onKeyPress={(evt) => {
          if (evt.key === 'Enter') {
            const page = parseInt(goTo);
            if (!isNaN(page) && page <= numberOfPages) {
              setGoTo('');
              onSelectPage(page);
            }
          }
        }}
        width="65px"
        placeholder="Goto"
        small={props.pagination.small}
        disabled={numberOfPages <= 1}
      />
    </div>
  );
};

export const paginationShape = PropTypes.shape({
  rowCount: PropTypes.number.isRequired,
  selectedPage: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
          .isRequired,
        selected: PropTypes.bool,
      }),
    ).isRequired,
    value: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string, //deprecated
      PropTypes.number, //deprecated
    ]),
  }),
  onSelectPage: PropTypes.func,
  small: PropTypes.bool,
});

Pagination.propTypes = {
  pagination: paginationShape.isRequired,
};
