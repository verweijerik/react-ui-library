import { generatePageRange, selectedRowsPerPage } from './pagination.viewdata';

describe('pagination', () => {
  test('single page', () => {
    expect(generatePageRange(1, 1, 2)).toMatchObject([1]);
  });
  test('few pages', () => {
    expect(generatePageRange(1, 4, 2)).toMatchObject([1, 2, 3, 4]);
  });
  test('many pages, low selection', () => {
    expect(generatePageRange(2, 25, 2)).toMatchObject([1, 2, 3, 4, '...', 25]);
  });
  test('many pages, middle selection', () => {
    expect(generatePageRange(15, 25, 2)).toMatchObject([
      1,
      '...',
      13,
      14,
      15,
      16,
      17,
      '...',
      25,
    ]);
  });
  test('many pages, high selection', () => {
    expect(generatePageRange(24, 25, 2)).toMatchObject([
      1,
      '...',
      22,
      23,
      24,
      25,
    ]);
  });
  test('selected rows per page, by value', () => {
    const options = [{}, {}, { value: 30, selected: true }, {}];
    const value = 66;
    expect(selectedRowsPerPage(options, value)).toBe(66);
  });
  test('selected rows per page, by selected option', () => {
    const options = [{}, {}, { value: 30, selected: true }, {}];
    expect(selectedRowsPerPage(options)).toBe(30);
  });
  test('default selected rows per page', () => {
    expect(selectedRowsPerPage()).toBe(10);
  });
});
