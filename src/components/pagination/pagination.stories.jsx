import React from 'react';
import { storiesOf } from '@storybook/react';

import { Container } from '../../helpers/container';
import { Pagination } from './pagination';

const style = {
  height: '500px',
  width: '500px',
};

const pagination = {
  rowCount: 500,
  selectedPage: 10,
};

const paginationWithNumberPerPage = {
  rowCount: 500,
  selectedPage: 10,
  rowsPerPage: {
    onChange: () => {},
    options: [
      { label: '10 / page', value: 10 },
      { label: '20 / page', value: 20 },
      { label: '50 / page', value: 50 },
    ],
    value: { label: '20 / page', value: 20 },
  },
  onSelectPage: () => {},
};

const paginationSmall = {
  rowCount: 500,
  selectedPage: 10,
  rowsPerPage: {
    onChange: () => {},
    options: [
      { label: '10 / page', value: 10 },
      { label: '20 / page', value: 20 },
      { label: '50 / page', value: 50 },
    ],
    value: { label: '20 / page', value: 20 },
  },
  onSelectPage: () => {},
  small: true,
};

storiesOf('Basic/Pagination', module)
  .add('default', () => (
    <Container style={style}>
      <Pagination pagination={pagination} />
    </Container>
  ))
  .add('with number per page', () => (
    <Container style={style}>
      <Pagination pagination={paginationWithNumberPerPage} />
    </Container>
  ))
  .add('small', () => (
    <Container style={style}>
      <Pagination pagination={paginationSmall} />
    </Container>
  ));
