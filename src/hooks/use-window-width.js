import { debounce } from 'lodash';
import { useState, useEffect } from 'react';

const getWidth = () =>
  window.innerWidth ||
  document.documentElement.clientWidth ||
  document.body.clientWidth;

//https://dev.to/vitaliemaldur/resize-event-listener-using-react-hooks-1k0c
export const useWindowWidth = () => {
  let [width, setWidth] = useState(getWidth());
  const resizeListener = debounce(() => setWidth(getWidth()), 150);
  useEffect(() => {
    window.addEventListener('resize', resizeListener);
    return () => {
      window.removeEventListener('resize', resizeListener);
    };
  }, []);
  return width;
};
