import { useEffect } from 'react';

export const useKeyboardEvent = (key, fn, dependencyList = []) => {
  useEffect(() => {
    const handler = (event) => {
      if (event.key === key) {
        fn();
      }
    };

    window.addEventListener('keydown', handler);
    return () => {
      window.removeEventListener('keydown', handler);
    };
  }, dependencyList);
};
