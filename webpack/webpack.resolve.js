const path = require('path');

module.exports = {
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      '~components': path.resolve(__dirname, '../src/components'),
      '~helpers': path.resolve(__dirname, '../src/helpers'),
      '~style': path.resolve(__dirname, '../src/style'),
      '~vendor': path.resolve(__dirname, '../src/vendor'),
    },
  },
};
