const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = function getRules(env) {
  const styleLoader = MiniCssExtractPlugin.loader;

  return [
    {
      test: /\.html$/,
      use: [
        {
          loader: 'html-loader',
          options: {
            minimize: true,
          },
        },
      ],
    },
    {
      test: /\.jsx$|\.es6$|\.js$/,
      exclude: /(node_modules|oliasoftDefaultLibraries)/,
      use: {
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          presets: ['@babel/react'],
        },
      },
    },
    {
      test: /\.(ttf|eot|woff|woff2)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: './fonts/[name].[contenthash].[ext]',
          },
        },
      ],
    },
    {
      test: /\.(svg|gif|png|jpg)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: './images/[name].[contenthash].[ext]',
          },
        },
      ],
    },
    {
      test: /^(?!.*?(\.module)).*\.css$/, //all *.css except for *.module.css
      use: [
        styleLoader,
        {
          loader: 'css-loader',
          options: {
            sourceMap: env === 'development',
          },
        },
      ],
    },
    {
      test: /^(?!.*?(\.module)).*\.less$/, //all *.less except for *.module.less
      use: [
        styleLoader,
        {
          loader: 'css-loader',
          options: {
            sourceMap: env === 'development',
          },
        },
        'less-loader',
      ],
    },
    {
      test: /\.module\.css$/, //*.module.css
      use: [
        styleLoader,
        {
          loader: 'css-loader',
          options: {
            sourceMap: env === 'development',
            modules: {
              mode: 'local',
              localIdentName: '[name]-[local]-[hash:base64:5]', //friendly module classNames
            },
          },
        },
      ],
    },
    {
      test: /\.module\.less$/, //*.module.less
      use: [
        styleLoader,
        {
          loader: 'css-loader',
          options: {
            sourceMap: env === 'development',
            modules: {
              mode: 'local',
              localIdentName: '[name]-[local]-[hash:base64:5]', //friendly module classNames
            },
          },
        },
        'less-loader',
      ],
    },
    {
      test: /\.stories\.jsx?$/,
      loader: require.resolve('@storybook/source-loader'),
      include: [path.resolve(__dirname, '../src')],
      enforce: 'pre',
    },
  ];
};
