const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const getRules = require('../webpack/webpack.common.rules.js');
const node = require('../webpack/webpack.common.node.js');
const resolve = require('../webpack/webpack.resolve.js').resolve;

module.exports = {
  stories: ['../src/components/**/*.stories.@(js|jsx|mdx)'],
  addons: [
    '@storybook/addon-actions/register',
    '@storybook/addon-links/register',
    '@storybook/addon-storysource',
    '@storybook/addon-docs',
  ],
  webpackFinal: (config) => {
    const rules = getRules('development');

    config.module.rules = []; //delete default storybook rules because they conflict (postcss causes a conflict)

    rules.forEach((r) => {
      config.module.rules.push(r);
    });

    return {
      ...config,
      module: {
        ...config.module,
        rules: [...rules], //attach our own rules from the application webpack config
      },
      plugins: [
        ...config.plugins,
        new MiniCssExtractPlugin({
          filename: '[name].css',
        }),
      ],
      resolve,
      node,
    };
  },
};
